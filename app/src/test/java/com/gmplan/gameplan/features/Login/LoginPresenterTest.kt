package com.gmplan.gameplan.features.Login

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.RxSchedulersImplTest
import com.gmplan.gameplan.data.pojos.userPojo.UserPojo
import com.gmplan.gameplan.data.source.TaskDataSource
import io.reactivex.Single
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

class LoginPresenterTest {


    private lateinit var view: LoginView
    private lateinit var schedulers: RxSchedulers
    private lateinit var presenter: LoginPresenter
    private lateinit var repository: TaskDataSource

    @Before
    fun setup() {
        view = Mockito.mock(LoginView::class.java)
        schedulers = RxSchedulersImplTest()
        repository = Mockito.mock(TaskDataSource::class.java)
        presenter = LoginPresenter(repository, schedulers)
    }

    @Test
    fun loginApiNullReturn() {

        Mockito.`when`(repository.LoginUser(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
                .thenReturn(Single.just(UserPojo()))

        presenter.LoginUser("email", "password", "some_id")

        Mockito.verify(view).onWrongCredentials()

    }

    @Test
    fun test() {
        assertEquals(1, 1)
    }
}