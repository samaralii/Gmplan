package com.gmplan.gameplan.util

import com.prolificinteractive.materialcalendarview.CalendarDay
import org.junit.Assert.*
import org.junit.Test
import java.util.*

class HelperFunsUnitTest {

    @Test
    fun shouldReturnCorrectDate() {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, 1992)
        calendar.set(Calendar.MONTH, 10 - 1)
        calendar.set(Calendar.DAY_OF_MONTH, 23)
        val c = CalendarDay.from(calendar)
        assertEquals("1992-10-23", c.toStrng())
    }

    @Test
    fun shouldReturnNull() {
        assertNull("".splitTags())
    }

    @Test
    fun shouldReturnSingleString() {

        val string = "test".splitTags()
        val singleText = string?.get(0)

        assertNotNull(singleText)
        assertNotNull(string)

        assertEquals(1, string?.size)
        assertEquals("test", singleText)



        val string2 = "test".splitTags()
        val singleText2 = string2?.get(0)

        assertNotNull(singleText2)
        assertNotNull(string2)

        assertEquals(3, string2?.size)
        assertEquals("test", singleText2)

    }


}
