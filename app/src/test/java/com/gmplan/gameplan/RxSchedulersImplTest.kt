package com.gmplan.gameplan

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import io.reactivex.schedulers.Schedulers


class RxSchedulersImplTest : RxSchedulers {
    override fun io() = Schedulers.trampoline()
    override fun computation() = Schedulers.trampoline()
    override fun ui() = Schedulers.trampoline()
}