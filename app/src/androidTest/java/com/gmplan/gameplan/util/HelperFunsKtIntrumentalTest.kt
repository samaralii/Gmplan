package com.gmplan.gameplan.util

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import android.widget.EditText
import com.prolificinteractive.materialcalendarview.CalendarDay
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@RunWith(AndroidJUnit4::class)
class HelperFunsKtIntrumentalTest {

    val DATE = "CalendarDay{1992-10-23}"

    @Test
    fun editTextShouldShowError() {
        val appContext = InstrumentationRegistry.getTargetContext()
        val et = EditText(appContext)
        et.setText("")
        assertFalse(et.validate())
    }

    @Test
    fun editTextShouldShowNotError() {
        val appContext = InstrumentationRegistry.getTargetContext()
        val et = EditText(appContext)
        et.setText("some_text")
        assertTrue(et.validate())
    }

    @Test
    fun shouldPassValidEmail() {
        assertTrue("samarali@live.com".isValidEmail())
    }

    @Test
    fun shouldfailValidEmail() {
        assertFalse("samaralilive.com".isValidEmail())
    }



}