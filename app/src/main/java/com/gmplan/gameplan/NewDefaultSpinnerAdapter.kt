package com.gmplan.gameplan

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

class NewDefaultSpinnerAdapter(val data: List<String>) : BaseAdapter() {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return getCustomView(position, parent as ViewGroup)
    }

    override fun getItem(position: Int): String {
        return data[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return data.size
    }


    private fun getCustomView(pos: Int, parent: ViewGroup): View {

        val inflater: LayoutInflater = parent.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val view: View = inflater.inflate(R.layout.spinner_careers_list, parent, false)

        val name = view.findViewById<TextView>(R.id.tvName) as TextView

        name.text = data[pos]

        return view
    }

}
