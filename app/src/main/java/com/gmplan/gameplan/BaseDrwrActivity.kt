package com.gmplan.gameplan

import android.content.Intent
import android.graphics.drawable.Drawable
import android.support.annotation.StringRes
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.gmplan.gameplan.data.pojos.userPojo.UserData
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.Login.LoginActivity
import com.gmplan.gameplan.features.agenda.AgendaActivity
import com.gmplan.gameplan.features.dashboard.NewStudentDashboardActivity
import com.gmplan.gameplan.features.dashboard.StaffDashboardActivity
import com.gmplan.gameplan.features.editProfile.EditProfileActivity
import com.gmplan.gameplan.features.feed.FeedActivity
import com.gmplan.gameplan.features.interestProfile.onetIntro.OnetIntroActivity
import com.gmplan.gameplan.features.myRecruiters.MyRecruiterActivity
import com.gmplan.gameplan.features.profileVIews.ProfileViewsActivity
import com.gmplan.gameplan.features.searchCareers.SearchCareersActivity
import com.gmplan.gameplan.features.staff.myMessenger.MyMessengerActivity
import com.gmplan.gameplan.features.staff.studentsProfiles.StudentProfileActivity
import com.gmplan.gameplan.features.staff.updateStatus.UpdateStatusActivity
import com.gmplan.gameplan.infrastructure.GmplanApplication
import com.gmplan.gameplan.util.AppConst
import com.gmplan.gameplan.util.Utilz
import com.hannesdorfmann.mosby.mvp.MvpActivity
import com.hannesdorfmann.mosby.mvp.MvpPresenter
import com.hannesdorfmann.mosby.mvp.MvpView
import com.mikepenz.materialdrawer.AccountHeaderBuilder
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.ProfileDrawerItem
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader
import com.mikepenz.materialdrawer.util.DrawerImageLoader

abstract class BaseDrwrActivity<V : MvpView, P : MvpPresenter<V>> : MvpActivity<V, P>(), Drawer.OnDrawerItemClickListener {


    private lateinit var item1: PrimaryDrawerItem

    private lateinit var item2: PrimaryDrawerItem
    private lateinit var item3: PrimaryDrawerItem
    private lateinit var item4: PrimaryDrawerItem
    private lateinit var item5: PrimaryDrawerItem
    private lateinit var item6: PrimaryDrawerItem

    private lateinit var item7: PrimaryDrawerItem
    private lateinit var item8: PrimaryDrawerItem
    private lateinit var item9: PrimaryDrawerItem
    private lateinit var item10: PrimaryDrawerItem
//    private lateinit var item11: PrimaryDrawerItem

    private lateinit var item12: PrimaryDrawerItem
    private lateinit var item13: PrimaryDrawerItem


    override fun createPresenter() = gifPresenter()
    protected abstract fun gifPresenter(): P
    protected abstract fun finishCurrentActivity()
    fun gifApplication() = (application as GmplanApplication)


    var type = 0
    var getUserData: UserData? = null
    private var toolbar: Toolbar? = null
    private var img_url = ""
    private var drwr: DrawerBuilder? = null
    private var selectedId = -1L

    private lateinit var baseRepository: TaskRepository

    fun createDrawer(toolbar: Toolbar? = null, repository: TaskRepository,
                     haveDrawer: Boolean = false, selectedId: Long = -1) {
        this.toolbar = toolbar
        this.baseRepository = repository
        this.getUserData = repository.getUserData(AppConst.USER_DATA).userData
        this.selectedId = selectedId
        img_url = getUserData.let { Utilz.getImgUrl(it) }
        getUserData?.user?.type?.let {
            type = it.toInt()
        }
        DrawerImageLoader.init(object : AbstractDrawerImageLoader() {
            override fun set(imageView: ImageView, uri: android.net.Uri, placeholder: Drawable, tag: String?) {
                super.set(imageView, uri, placeholder, tag)
                try {
                    Glide.with(this@BaseDrwrActivity)
                            .load(img_url)
                            .placeholder(placeholder)
                            .dontAnimate()
                            .into(imageView)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })

        if (haveDrawer) {
            addDrawerItems()
            drawerSetup()
        }


    }

    private fun addDrawerItems() {
        item1 = drawerItem(R.string.drawer_item_dashboard, 1).withSelectable(false)

        when (type) {
            AppConst.TYPE_STAFF -> {
                item7 = drawerItem(R.string.drawer_item_studentProfile, 7)
                item8 = drawerItem(R.string.drawer_item_myMessenger, 8)
                item9 = drawerItem(R.string.drawer_item_myAgenda, 9)
                item10 = drawerItem(R.string.drawer_item_updateStatus, 10)
//                item11 = drawerItem(R.string.drawer_item_offerCenter, 11)
            }

            AppConst.TYPE_STUDENT -> {
                item2 = drawerItem(R.string.drawer_item_editProfile, 2)
                item3 = drawerItem(R.string.drawer_item_interestProfile, 3)
                item4 = drawerItem(R.string.drawer_item_searchCareers, 4)
                item5 = drawerItem(R.string.drawer_item_myRecruiters, 5)
                item6 = drawerItem(R.string.drawer_item_profileViews, 6)
            }
        }


        item12 = drawerItem(R.string.drawer_item_logout, 12)
        item13 = drawerItem(R.string.drawer_news_feed, 13)
    }

    private fun drawerItem(@StringRes name: Int, id: Long) = PrimaryDrawerItem().apply {
        withTextColor(Utilz.getColor(this@BaseDrwrActivity, R.color.md_black_1000))
        withSelectable(false)
        withName(name)
        withIdentifier(id)
    }


    private fun drawerSetup() {

        var name = ""
        var email = ""

        when (type) {
            AppConst.TYPE_STUDENT -> {
                name = "${getUserData?.student?.firstName} ${getUserData?.student?.lastName}"
                email = getUserData?.student?.mySchool.toString()
            }
            AppConst.TYPE_STAFF -> {
                name = getUserData?.admissionStaff?.name.toString()
                email = getUserData?.admissionStaff?.jobTitle.toString()
            }
        }


        val header = AccountHeaderBuilder().apply {
            withActivity(this@BaseDrwrActivity)
            addProfiles(
                    ProfileDrawerItem().apply {
                        withName(name)
                        withEmail(email)
                        withIcon(img_url)
                    }
            )

            withHeaderBackground(R.drawable.drawerbg)
            withOnAccountHeaderListener { _, _, _ ->
                Utilz.tmsg(this@BaseDrwrActivity, "Profile")
                false
            }
        }.build()

        drwr = DrawerBuilder(this).apply {
            withActivity(this@BaseDrwrActivity)
            withToolbar(toolbar!!)
            withAccountHeader(header)
            withSelectedItem(selectedId)
            withSliderBackgroundColorRes(R.color.md_white_1000)
            withDrawerItems(setDrawerItems())
            withOnDrawerItemClickListener(this@BaseDrwrActivity)
            build()
        }


    }

    private fun setDrawerItems(): List<IDrawerItem<*, *>> {
        val item: MutableList<IDrawerItem<*, *>> = ArrayList()

        item.add(item1)
        item.add(item13)

        when (type) {
            4 -> {
                item.add(item2)
                item.add(item3)
                item.add(item4)
                item.add(item5)
                item.add(item6)
            }
            5 -> {
                item.add(item7)
                item.add(item8)
                item.add(item9)
                item.add(item10)
//                item.add(item11)
            }
        }
        item.add(item12)
        return item
    }

    override fun onItemClick(p0: View?, p1: Int, p2: IDrawerItem<*, *>?): Boolean {

        if (p2?.identifier == selectedId) {
            return false
        }


        when (p2?.identifier?.toInt()) {

            1 -> {
                if (type == AppConst.TYPE_STUDENT) {
                    startActivity(Intent(this, NewStudentDashboardActivity::class.java))
                } else if (type == AppConst.TYPE_STAFF) {
                    startActivity(Intent(this, StaffDashboardActivity::class.java))
                }
            }

            2 -> startActivity(Intent(this, EditProfileActivity::class.java))
            3 -> startActivity(Intent(this, OnetIntroActivity::class.java))
            4 -> startActivity(Intent(this, SearchCareersActivity::class.java))
            5 -> startActivity(Intent(this, MyRecruiterActivity::class.java))
            6 -> startActivity(Intent(this, ProfileViewsActivity::class.java))
            7 -> startActivity(Intent(this, StudentProfileActivity::class.java))
            8 -> startActivity(Intent(this, MyMessengerActivity::class.java))
            9 -> startActivity(Intent(this, AgendaActivity::class.java))
            10 -> startActivity(Intent(this, UpdateStatusActivity::class.java))

            12 -> {
                with(baseRepository) {
                    deleteKey(AppConst.USER_DATA)
                    deleteKey(AppConst.IS_FIRST_TIME_LOGIN)
                    deleteKey(AppConst.IS_FINGERPRINT_ENABLED)
                }

                startActivity(Intent(this, LoginActivity::class.java))
            }

            13 -> startActivity(Intent(this, FeedActivity::class.java))

        }

        finishCurrentActivity()

        return false
    }


    private var isBackPress = true
    override fun onBackPressed() {
        if (isBackPress) {
            Utilz.tmsgInfo(this, "Press Back Again To Exit")
            isBackPress = false
        } else {
            super.onBackPressed()
        }
    }

}