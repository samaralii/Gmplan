package com.gmplan.gameplan.features.introView

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment

import com.github.paolorotolo.appintro.AppIntro
import com.gmplan.gameplan.features.Login.LoginActivity

/**
 * Created by Sammie on 3/16/2017.
 */

class Intro : AppIntro() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        addSlide(FragIntroA())
        addSlide(FragIntroB())
        addSlide(FragIntroC())

    }

    override fun onSlideChanged(oldFragment: Fragment?, newFragment: Fragment?) {
        super.onSlideChanged(oldFragment, newFragment)


        if (newFragment is FragIntroA) {
            newFragment.playAnimation()
        }

        if (oldFragment is FragIntroA) {
            oldFragment.hideText()
        }

        if (newFragment is FragIntroB) {
            newFragment.playAnimation()
        }

        if (newFragment is FragIntroC) {
            newFragment.playAnimation()
        }

    }


    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        gotoLoginView()
    }


    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        gotoLoginView()
    }


    private fun gotoLoginView() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }


}
