package com.gmplan.gameplan.features.interestProfile.jobzones

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.Result

/**
 * Created by Sammie on 9/8/2017.
 */
class ResultFragment : Fragment() {


    var _result: ArrayList<Result>? = null

    var unbinder: Unbinder? = null

    @BindView(R.id.frag_result_list)
    lateinit var list: RecyclerView

    companion object {

        private val RESULT_LIST = "resultlist"

        fun newInstance(result: ArrayList<Result>): ResultFragment {
            val fragment = ResultFragment()
            val b = Bundle()
            b.putParcelableArrayList(RESULT_LIST, result)
            fragment.arguments = b
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _result = arguments!!.getParcelableArrayList(RESULT_LIST)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.frag_result, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    fun init() {

        list.layoutManager = LinearLayoutManager(context)

        _result?.let { list.adapter = ResultAdapter(it) }

    }


    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }


}