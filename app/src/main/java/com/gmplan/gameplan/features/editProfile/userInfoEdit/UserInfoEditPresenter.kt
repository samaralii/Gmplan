package com.gmplan.gameplan.features.editProfile.userInfoEdit

import android.util.Log
import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.userPojo.UserData
import com.gmplan.gameplan.data.source.TaskDataSource
import com.gmplan.gameplan.util.splitTags
import com.google.gson.JsonElement
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.observers.DisposableSingleObserver
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class UserInfoEditPresenter(private val taskDataSource: TaskDataSource, private val rxSchedulers: RxSchedulers) :
        MvpBasePresenter<UserInfoEditView>() {


    fun editUserProfile(id: String, likes: String, dislikes: String, music: String,
                        team: String, college: String, file: File?) {


        val student_id = RequestBody.create(
                MediaType.parse("multipart/form-data"), id)

        val myLikes = RequestBody.create(
                MediaType.parse("multipart/form-data"), convertTag(likes))

        val myDislikes = RequestBody.create(
                MediaType.parse("multipart/form-data"), convertTag(dislikes))

        val myMusic = RequestBody.create(
                MediaType.parse("multipart/form-data"), convertTag(music))

        val myTeam = RequestBody.create(
                MediaType.parse("multipart/form-data"), convertTag(team))

        val myCollege = RequestBody.create(
                MediaType.parse("multipart/form-data"), convertTag(college))

        var photoFile: MultipartBody.Part? = null

        if (file != null) {
            Log.d("FILE PATH", file.absolutePath)
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
            photoFile = MultipartBody.Part.createFormData("photo", file.name, requestFile)
        }


        taskDataSource.editProfile(student_id, myLikes, myDislikes, myMusic, myTeam, myCollege, photoFile)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissProgress()

                }
                .subscribeWith(object : DisposableSingleObserver<JsonElement>() {
                    override fun onError(p0: Throwable) {
                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onFailedEditProfile()

                    }

                    override fun onSuccess(p0: JsonElement) {


                        if (isViewAttached)
                            view?.onSuccessEditProfile()

                    }

                })

    }


    private fun convertTag(s: String): String {
        return s.replace(" ", ",")
    }

    fun onCreateView(userData: UserData) {

        if (isViewAttached) view?.setTags(userData.student.myLike.splitTags(),
                userData.student.myDislike.splitTags(),
                userData.student.myMusic.splitTags(),
                userData.student.myTeam.splitTags(),
                userData.student.myCollege.splitTags())

    }


}



