package com.gmplan.gameplan.features.staff.myMessenger.outbox

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.MessagesObject


class OutboxAdapter(var data: MutableList<MessagesObject> = arrayListOf(), private val context: Context) : RecyclerView.Adapter<OutboxVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OutboxVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_outbox, parent, false)
        return OutboxVH(view)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: OutboxVH, position: Int) {
        holder.subject.text = data[position].subject
    }

    fun addData(data: List<MessagesObject>) {
        this.data.clear()
        this.data = data as MutableList<MessagesObject>
        notifyDataSetChanged()

    }

}

class OutboxVH(v: View) : RecyclerView.ViewHolder(v) {
    val subject by lazy { v.findViewById<TextView>(R.id.list_msgs_subject) }
}