package com.gmplan.gameplan.features.staff.studentsProfiles.profileDetails

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.bumptech.glide.Glide
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.DetailsItems
import com.gmplan.gameplan.data.pojos.studentProfileDetails.StudentProfileDetails
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.staff.studentsProfiles.StudentProfileActivity
import com.gmplan.gameplan.features.staff.studentsProfiles.detailFragment.DetailFragment
import com.gmplan.gameplan.util.AppConst
import com.gmplan.gameplan.util.Utilz
import com.hannesdorfmann.mosby.mvp.MvpFragment
import de.hdodenhof.circleimageview.CircleImageView
import javax.inject.Inject

/**
 * Created by Sammie on 6/8/2017.
 */
class ProfileDetailFragment : MvpFragment<ProfileDetailView, ProfileDetailPresenter>(), ProfileDetailView, DetailItemsAdapter.Listener {

    @BindView(R.id.frag_profile_detail_list)
    lateinit var recyclerView: RecyclerView

    @Inject
    lateinit var taskRepository: TaskRepository

    @Inject
    lateinit var schedulers: RxSchedulersImpl

    @BindView(R.id.frag_profile_profilePicture)
    lateinit var imageView: CircleImageView;

    var pd: ProgressDialog? = null

    var unbinder: Unbinder? = null

    var data: StudentProfileDetails? = null

    override fun createPresenter(): ProfileDetailPresenter {
        (activity as StudentProfileActivity).gifApplication().component?.Inject(this)
        return ProfileDetailPresenter(taskRepository, schedulers)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pd = Utilz.dialog(context!!)
        data = arguments!!.getParcelable(AppConst.DATA)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.frag_profile_detail, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)

        presenter.getDetaimsItems("${data?.studentinfo?.student?.firstName} ${data?.studentinfo?.student?.lastName}")

        Glide.with(context)
                .load(Utilz.getImgUrl(data?.studentinfo?.student?.photoDir, data?.studentinfo?.student?.photo))
                .placeholder(R.drawable.placeholder)
                .dontAnimate()
                .into(imageView)

    }

    override fun dismissDialog() {
        pd?.let {
            if (it.isShowing)
                it.dismiss()
        }
    }

    override fun detailsItems(list: List<DetailsItems>) {
        recyclerView.adapter = DetailItemsAdapter(list, this)
    }

    override fun onItemClick(id: Int) {
        openDetailFragment(id)
    }

    private fun openDetailFragment(cat: Int) {
        val b = Bundle()
        b.putParcelable(AppConst.DATA_1, data)
        b.putInt(AppConst.DATA, cat)
//        Utilz.replaceFragmentToActivity(fragmentManager, DetailFragment(),
//                R.id.activity_sp_container, b, addToBackStack = true)


        val fragment = DetailFragment()
        fragment.arguments = b

        val ft = fragmentManager!!.beginTransaction()
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
        ft.replace(R.id.activity_sp_container, fragment)
        ft.addToBackStack(null)
        ft.commit()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }


}