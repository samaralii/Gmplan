package com.gmplan.gameplan.features.myCareers.careersJobs

import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.careersJobs.Job
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.CustomWebViewActivity
import com.gmplan.gameplan.features.myCareers.MyCareersActivity
import com.gmplan.gameplan.util.Utilz
import com.hannesdorfmann.mosby.mvp.MvpFragment
import javax.inject.Inject

/**
 * Created by Sammie on 8/24/2017.
 */
class CareersJobs : MvpFragment<CareersjobsView, CareersJobsPresenter>(), CareersjobsView, CareersJobsAdapter.Listener {


    @Inject
    lateinit var taskRepository: TaskRepository

    @Inject
    lateinit var schedulers: RxSchedulersImpl

    var unbinder: Unbinder? = null

    var Adapter: CareersJobsAdapter? = null

    @BindView(R.id.frag_mycareerjob_careerList)
    lateinit var recyclerView: RecyclerView

    @BindView(R.id.frag_mycareerjobs_sr)
    lateinit var swipeRefresh: SwipeRefreshLayout

    var studentSchoolId: String? = null
    var careerTitle: String? = null

    companion object {

        val STUDENT_SCHOOL_ID = "schoolId"
        val CAREER_TITLE = "careerTitle"

        fun newInstance(studentSchoolId: String, careerTitle: String): CareersJobs {
            val fragment = CareersJobs()
            val b = Bundle()
            b.putString(STUDENT_SCHOOL_ID, studentSchoolId)
            b.putString(CAREER_TITLE, careerTitle)
            fragment.arguments = b
            return fragment
        }
    }


    override fun createPresenter(): CareersJobsPresenter {
        (activity as MyCareersActivity).gifApplication().component?.Inject(this)
        return CareersJobsPresenter(taskRepository, schedulers)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        studentSchoolId = arguments!!.getString(STUDENT_SCHOOL_ID)
        careerTitle = arguments!!.getString(CAREER_TITLE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.frag_mycareersjobs, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        recyclerView.layoutManager = LinearLayoutManager(context)
        refreshList()
        swipeRefresh.setOnRefreshListener { refreshList() }

    }

    fun refreshList() {
        presenter.getCareersJobsList(studentSchoolId.toString(), careerTitle.toString())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

    override fun dismissDialog() {
        if (swipeRefresh.isRefreshing)
            swipeRefresh.isRefreshing = false
    }

    override fun onErrorGettingJobs() {
        Utilz.tmsgInfo(context!!, "Error")
    }

    override fun onNoJobsFound() {
        Utilz.tmsgInfo(context!!, "No Jobs Found")
    }

    override fun jobsList(jobs: List<Job>) {
        recyclerView.adapter = CareersJobsAdapter(jobs, this)
    }

    override fun onClick(url: String, title: String) {
        val i = Intent(context, CustomWebViewActivity::class.java)
        i.putExtra("url", url)
        i.putExtra("job", title)
        startActivity(i)
    }


}