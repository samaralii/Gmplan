package com.gmplan.gameplan.features.staff.studentsProfiles

import android.os.Bundle
import android.support.v7.widget.Toolbar
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.BaseDrwrActivity
import com.gmplan.gameplan.EmptyPresenter
import com.gmplan.gameplan.EmptyView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.staff.studentsProfiles.schoolListFragment.SchoolListFragment
import com.gmplan.gameplan.util.Utilz
import javax.inject.Inject

/**
 * Created by Sammie on 6/6/2017.
 */
class StudentProfileActivity : BaseDrwrActivity<EmptyView, EmptyPresenter>() {
    override fun gifPresenter() = EmptyPresenter()

    @BindView(R.id.app_toolbar)
    lateinit var toolbar: Toolbar

    override fun finishCurrentActivity() {
        finish()
    }

    @Inject
    lateinit var repository: TaskRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sp)
        gifApplication().component?.Inject(this)
        ButterKnife.bind(this)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Students Profile"
        createDrawer(toolbar, repository, true, 7)
        init()
    }

    private fun init() {
        Utilz.addFragmentToActivity(supportFragmentManager,
                SchoolListFragment.newInstance(), R.id.activity_sp_container)
    }


    override fun onBackPressed() {
        val c = supportFragmentManager.backStackEntryCount
        if (c > 0)
            supportFragmentManager.popBackStack()
        else
            super.onBackPressed()
    }

}