package com.gmplan.gameplan.features.staff.myMessenger.inbox

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.MessagesObject


class InboxAdapter(var data: MutableList<MessagesObject> = arrayListOf(),
                   private val context: Context) : RecyclerView.Adapter<MessagesVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessagesVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_messagess, parent, false)
        return MessagesVH(view)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: MessagesVH, position: Int) {
//        Glide.with(context).load(data[position].photo).into(holder.image)
        holder.subject.text = data[position].subject
    }

    fun addData(data: List<MessagesObject>) {
        this.data.clear()
        this.data = data as MutableList<MessagesObject>
        notifyDataSetChanged()

    }

}

class MessagesVH(v: View) : RecyclerView.ViewHolder(v) {
    val image by lazy { v.findViewById<ImageView>(R.id.list_msgs_iv) }
    val subject by lazy { v.findViewById<TextView>(R.id.list_msgs_subject) }
}