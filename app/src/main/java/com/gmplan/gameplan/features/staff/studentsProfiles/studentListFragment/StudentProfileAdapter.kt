package com.gmplan.gameplan.features.staff.studentsProfiles.studentListFragment

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.Student_

class StudentProfileAdapter(private val data: MutableList<Student_>, val listener: Listener) :
        RecyclerView.Adapter<StudentProfileAdapter.StudentProfileVH>() {

    override fun onBindViewHolder(holder: StudentProfileAdapter.StudentProfileVH, position: Int) {
        holder.name.text = data[position].name
    }

    override fun onCreateViewHolder(parent: android.view.ViewGroup, viewType: Int): StudentProfileAdapter.StudentProfileVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_student_profile, parent, false)
        return StudentProfileVH(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }


    inner class StudentProfileVH(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_student_name)
        lateinit var name: android.widget.TextView


        init {
            ButterKnife.bind(this, v)

            v.setOnClickListener {
                listener.onItemClick(data[layoutPosition].id)
            }
        }

    }

    interface Listener {
        fun onItemClick(id: String?)
    }
}