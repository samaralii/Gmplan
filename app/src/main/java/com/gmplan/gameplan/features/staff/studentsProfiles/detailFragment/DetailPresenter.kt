package com.gmplan.gameplan.features.staff.studentsProfiles.detailFragment

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.source.TaskDataSource
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby.mvp.MvpPresenter

/**
 * Created by Sammie on 6/8/2017.
 */
class DetailPresenter(private val repo: TaskDataSource, private val schedulers: RxSchedulers): MvpBasePresenter<DetailView>() {


}