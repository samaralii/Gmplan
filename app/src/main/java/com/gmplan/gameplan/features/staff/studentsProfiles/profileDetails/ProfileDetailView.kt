package com.gmplan.gameplan.features.staff.studentsProfiles.profileDetails

import com.gmplan.gameplan.data.pojos.DetailsItems
import com.gmplan.gameplan.data.pojos.studentProfileDetails.StudentProfileDetails
import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Sammie on 6/8/2017.
 */
interface ProfileDetailView: MvpView {
    fun dismissDialog()
    fun detailsItems(list: List<DetailsItems>)
}