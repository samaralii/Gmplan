package com.gmplan.gameplan.features.agenda.agendaFragment


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.CheckList
import com.gmplan.gameplan.data.pojos.Event
import com.gmplan.gameplan.data.pojos.EventsDes
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.agenda.AgendaActivity
import com.gmplan.gameplan.features.agenda.createAgenda.CreateAgendaActivity
import com.gmplan.gameplan.features.agenda.eventsFragment.EventsListFragment
import com.gmplan.gameplan.util.Utilz
import com.gmplan.gameplan.util.toStrng
import com.hannesdorfmann.mosby.mvp.MvpFragment
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import java.util.*
import javax.inject.Inject


class AgendaFragment : MvpFragment<AgendaView, AgendaPresenter>(), AgendaView {


    var unbinder: Unbinder? = null

    @BindView(R.id.activity_agenda_calendarView)
    lateinit var cv: MaterialCalendarView

    @BindView(R.id.progress)
    lateinit var progress: View

    @Inject
    lateinit var taskRepo: TaskRepository

    @Inject
    lateinit var scheduler: RxSchedulersImpl

    val userData by lazy { (activity as AgendaActivity).getUserData }

    val id by lazy {
        when (userData?.user?.type) {
            "4" -> userData?.student?.id.toString()
            "5" -> userData?.admissionStaff?.id.toString()
            else -> "0"
        }
    }

    override fun createPresenter(): AgendaPresenter {
        (activity as AgendaActivity).gifApplication().component?.Inject(this)
        return AgendaPresenter(taskRepo, scheduler)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_agenda, container, false)
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cv.selectionMode = MaterialCalendarView.SELECTION_MODE_MULTIPLE

        showProgress()

        when (userData?.user?.type) {
            "4" -> presenter.getEvents(userData?.student?.id.toString(), true)
            "5" -> presenter.getEvents(userData?.admissionStaff?.id.toString(), false)
        }

    }


    override fun setEvenListeners(dateList: Array<String?>, events: List<Event>) {

        cv.setOnDateChangedListener { _, calendarDay, _ ->

            (0 until dateList.size)
                    .filter { calendarDay.toStrng() == dateList[it] }
                    .forEach { cv.setDateSelected(calendarDay, true) }

            showProgress()

            when (userData?.user?.type) {
                "4" -> presenter.getEventByDate(calendarDay.toStrng(), userData?.student?.id.toString())
                "5" -> presenter.getStaffEventByDate(calendarDay.toStrng(), userData?.admissionStaff?.id.toString())
            }
        }
    }

    private fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        progress.visibility = View.GONE
    }

    override fun setEventToCalendar(time: Date?) {
        cv.setDateSelected(time, true)
    }

    @OnClick(R.id.activity_agenda_btnAdd)
    fun onAddAgendaClick() {
        startActivityForResult(Intent(context, CreateAgendaActivity::class.java), 1)
    }

    override fun onErrorGettingChecklist() {
    }

    override fun openSingleEventDialog(checkList: CheckList) {

    }


    override fun openMultipleEventDialog(checklist: ArrayList<CheckList>) {
    }

    override fun dismissDialog() {
        hideProgress()
    }

    override fun onErrorGettingEvents() {
    }

    override fun noEventsFound() {
    }

    override fun onErrorGettingEventsByDate() {
    }

    override fun EventsByDate(events: ArrayList<EventsDes>) {
        if (events.isNotEmpty())
            Utilz.replaceFragmentToActivity(fragmentManager!!, EventsListFragment.newInstance(events),
                    R.id.activity_agenda_container, true)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1 && resultCode == Activity.RESULT_OK){
            when (userData?.user?.type) {
                "4" -> presenter.getEvents(userData?.student?.id.toString(), true)
                "5" -> presenter.getEvents(userData?.admissionStaff?.id.toString(), false)
            }
        }

    }

}
