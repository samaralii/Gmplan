package com.gmplan.gameplan.features.feed

import android.os.Bundle
import android.support.v7.widget.Toolbar
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.BaseDrwrActivity
import com.gmplan.gameplan.EmptyPresenter
import com.gmplan.gameplan.EmptyView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.util.Utilz
import javax.inject.Inject

class FeedActivity : BaseDrwrActivity<EmptyView, EmptyPresenter>() {

    override fun gifPresenter() = EmptyPresenter()

    @BindView(R.id.app_toolbar)
    lateinit var toolbar: Toolbar

    override fun finishCurrentActivity() {
        finish()
    }

    @Inject
    lateinit var repository: TaskRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feed)
        gifApplication().component?.Inject(this)
        ButterKnife.bind(this)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "News Feed"
        createDrawer(toolbar, repository, true, 13)
        init()
    }

    private fun init() {
        Utilz.addFragmentToActivity(supportFragmentManager, FeedFragment(), R.id.activity_feed_container)
    }
}
