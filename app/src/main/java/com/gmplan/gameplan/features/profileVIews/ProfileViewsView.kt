package com.gmplan.gameplan.features.profileVIews

import com.gmplan.gameplan.data.pojos.P_View
import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Sammie on 5/3/2017.
 */
interface ProfileViewsView: MvpView {

    fun dismissProgress()
    fun onSuccess(profileViews: List<P_View>)
    fun onNoProfileViews()
    fun onErrorProfileViews()
}