package com.gmplan.gameplan.features.myHighlights.myachievements

import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.gmplan.gameplan.R

/**
 * Created by Sammie on 5/23/2017.
 */
class AddAchievementDialog(val listener: DialogListener) : DialogFragment() {

    var unbinder: Unbinder? = null

    @BindView(R.id.dialog_add_achv_etDetail)
    lateinit var etDetail: EditText

    @BindView(R.id.dialog_add_achv_etTitle)
    lateinit var etTitle: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.dialog_add_achv, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        dialog.setTitle("Add Achievement")
    }

    @OnClick(R.id.dialog_add_achv_btnPost)
    fun onPostClick() {

        if (etTitle.text.isEmpty()) {
            etTitle.error = "Required Field"
            return
        } else {
            etTitle.error = null
        }


        if (etDetail.text.isEmpty()) {
            etDetail.error = "Required Field"
            return
        } else {
            etDetail.error = null
        }

        listener.onPostClick(etTitle.text.toString(), etDetail.text.toString(), dialog)

    }


    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }


    interface DialogListener {
        fun onPostClick(title: String, detail: String, dialog: DialogInterface)
    }


}