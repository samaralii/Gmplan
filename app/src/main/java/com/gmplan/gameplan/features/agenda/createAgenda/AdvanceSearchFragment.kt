package com.gmplan.gameplan.features.agenda.createAgenda

import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.SearchStudents
import kotlinx.android.synthetic.main.dialog_advancesearch.*

/**
 * Created by Sammie on 10/6/2017.
 */
class AdvanceSearchFragment : DialogFragment() {

    var listener: AdvanceSearchListener? = null

    companion object {

        private val STAFF_ID = "staffid"
        private val COLLEGE_ID = "collegeid"

        fun newInstance(staffId: String, collegeId: String): AdvanceSearchFragment {
            val dialog = AdvanceSearchFragment()
            val b = Bundle()
            b.putString(STAFF_ID, staffId)
            b.putString(COLLEGE_ID, collegeId)
            dialog.arguments = b
            return dialog
        }
    }

    var staffId = ""
    var collegeId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        staffId = arguments!!.getString(STAFF_ID)
        collegeId = arguments!!.getString(COLLEGE_ID)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_advancesearch, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        dialog_btnCancel.setOnClickListener {
            dialog.dismiss()
        }


        dialog_btnSearch.setOnClickListener {
            onSearchClick()
            dialog.dismiss()
        }


    }

    fun onSearchClick() {

        val students = SearchStudents(
                staffId, collegeId,
                dialog_etName.text.toString(),
                dialog_etUgpafrom.text.toString(),
                dialog_etUgpato.text.toString(),
                dialog_etWgpafrom.text.toString(),
                dialog_etWgpato.text.toString(),
                dialog_etClassRankfrom.text.toString(),
                dialog_etClassRankto.text.toString(),
                dialog_etSatfrom.text.toString(),
                dialog_etSatto.text.toString(),
                dialog_etActfrom.text.toString(),
                dialog_etActto.text.toString())

        val map = hashMapOf(
                "staffid" to staffId,
                "college_id" to collegeId,
                "name" to dialog_etName.text.toString(),
                "ugpa_from" to dialog_etUgpafrom.text.toString(),
                "ugpa_to" to dialog_etUgpato.text.toString(),
                "wgpa_from" to  dialog_etWgpafrom.text.toString(),
                "wgpa_to" to  dialog_etWgpato.text.toString(),
                "class_rank_from" to  dialog_etClassRankfrom.text.toString(),
                "class_rank_to" to dialog_etClassRankto.text.toString(),
                "sat_score_from" to dialog_etSatfrom.text.toString(),
                "sat_score_to" to  dialog_etSatto.text.toString(),
                "act_score_from" to dialog_etActfrom.text.toString(),
                "act_score_to" to dialog_etActto.text.toString(),
                "school" to "",
                "city" to ""
        )


        listener?.onSearchClick(map)

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        try {

            listener = context as AdvanceSearchListener

        } catch (e: Exception) {
            throw Exception("attach listener")
        }


    }


    interface AdvanceSearchListener {
        fun onSearchClick(students: HashMap<String, String>)
    }


}