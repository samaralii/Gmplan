package com.gmplan.gameplan.features.staff.studentsProfiles.detailFragment

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.studentProfileDetails.SemesterGoal

/**
 * Created by Sammie on 6/12/2017.
 */
class DetailSemesterGoalAdapter(private val data: List<SemesterGoal>) : RecyclerView.Adapter<DetailSemesterGoalAdapter.DetailSemesterGoalVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailSemesterGoalVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_detail_semestergoals, parent, false)
        return DetailSemesterGoalVH(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: DetailSemesterGoalVH, position: Int) {
        holder.tvTitle.text = data[position].title
        holder.tvDetail.text = data[position].detail
    }


    inner class DetailSemesterGoalVH(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_detail_semestergoal_title)
        lateinit var tvTitle: TextView


        @BindView(R.id.list_detail_semestergoal_detail)
        lateinit var tvDetail: TextView

        @BindView(R.id.list_detail_semestergoal_like)
        lateinit var like: ImageView


        init {
            ButterKnife.bind(this, v)
        }

        @OnClick(R.id.list_detail_semestergoal_like)
        fun onLikeClick() {

        }

    }
}
