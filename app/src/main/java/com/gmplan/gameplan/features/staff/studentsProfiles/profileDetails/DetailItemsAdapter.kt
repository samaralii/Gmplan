package com.gmplan.gameplan.features.staff.studentsProfiles.profileDetails

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.DetailsItems

/**
 * Created by Sammie on 6/8/2017.
 */
class DetailItemsAdapter(val data: List<DetailsItems>, val listener: Listener) : RecyclerView.Adapter<DetailItemsAdapter.DetailItemVH>() {

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailItemVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_detail_items, parent, false)
        return DetailItemVH(v)
    }

    override fun onBindViewHolder(holder: DetailItemVH, position: Int) {
        holder.name.text = data[position].name
    }


    inner class DetailItemVH(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_detail_items_name)
        lateinit var name: TextView

        init {
            ButterKnife.bind(this, v)

            v.setOnClickListener {
                listener.onItemClick(data[layoutPosition].id)
            }
        }
    }

    interface Listener {
        fun onItemClick(id: Int)
    }
}