package com.gmplan.gameplan.features.mySemesters.semestersListFragment

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.Semestergoal

/**
 * Created by Sammie on 5/22/2017.
 */
class MySemestersAdapter(val data: MutableList<Semestergoal>, val listeners: Listeners, val context: Context):
        RecyclerView.Adapter<MySemestersAdapter.MySemestersVH>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MySemestersVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_mysemesters, parent, false)
        return MySemestersVH(v)
    }

    override fun onBindViewHolder(holder: MySemestersVH, position: Int) {
        holder.title.text = data[position].title
        holder.text.text = data[position].detail



        holder.let {

            it.tvOption.setOnClickListener { _->

                val popup = PopupMenu(context, it.tvOption)
                popup.inflate(R.menu.options_menu_default)

                popup.setOnMenuItemClickListener {

                    when(it.itemId){
                        R.id.menu1 ->  listeners.onDeleteClick(data[position].id, position)
                    }

                    false
                }

                popup.show()

            }


        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun removeAt(position: Int) {
        data.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, data.size)
    }


    inner class MySemestersVH(val v: View) : RecyclerView.ViewHolder(v) {


        @BindView(R.id.list_mysemesters_Title)
        lateinit var title: TextView

        @BindView(R.id.list_mysemesters_text)
        lateinit var text: TextView

        @BindView(R.id.list_mysemesters_tvMenu)
        lateinit var tvOption: View


        init {
            ButterKnife.bind(this, v)
        }

    }

    interface Listeners {
        fun onDeleteClick(semesterId: String, pos: Int)
    }
}