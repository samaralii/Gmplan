package com.gmplan.gameplan.features.Login

import com.gmplan.gameplan.data.pojos.userPojo.UserPojo
import com.hannesdorfmann.mosby.mvp.MvpView

interface LoginView: MvpView {

    fun onErrorLogin()
    fun onSuccessLogin(userPojo: UserPojo?)
    fun onWrongCredentials()
    fun onFinally()
    fun onTestApiReturn()
    fun onReturnApi(result: Any)
    fun openFeedActivity()
    fun enableFingerprintViews()
}