package com.gmplan.gameplan.features.myHighlights.myachievements

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.Achievement

/**
 * Created by Sammie on 5/23/2017.
 */
class MyAchievAdapter(val data: MutableList<Achievement>, val listener: Listener) :
        RecyclerView.Adapter<MyAchievAdapter.MyAcievVH>() {

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyAcievVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_myachiev, parent, false)
        return MyAcievVH(v)
    }

    override fun onBindViewHolder(holder: MyAcievVH, position: Int) {
        holder.detail.text = data[position].detail
        holder.title.text = data[position].title
    }

    fun removeAt(position: Int) {
        data.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, data.size)
    }


    inner class MyAcievVH(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_myachiev_detail)
        lateinit var detail: TextView

        @BindView(R.id.list_myachiev_title)
        lateinit var title: TextView

        init {
            ButterKnife.bind(this, v)
        }

    }

    interface Listener {

    }
}