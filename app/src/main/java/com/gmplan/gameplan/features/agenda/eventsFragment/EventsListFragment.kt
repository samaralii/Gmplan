package com.gmplan.gameplan.features.agenda.eventsFragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.CheckLists
import com.gmplan.gameplan.data.pojos.EventsDes
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.agenda.AgendaActivity
import com.gmplan.gameplan.features.agenda.CheckListDialog
import com.gmplan.gameplan.util.tmsgError
import com.gmplan.gameplan.util.tmsgInfo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_events_list.*
import javax.inject.Inject


class EventsListFragment : Fragment() {

    var unbinder: Unbinder? = null

    @BindView(R.id.fragment_events_list)
    lateinit var list: RecyclerView

    var eventsList: ArrayList<EventsDes>? = null

    @Inject
    lateinit var repo: TaskRepository

    companion object {
        private val LIST = "list"

        fun newInstance(data: ArrayList<EventsDes>): EventsListFragment {
            val frag = EventsListFragment()
            val b = Bundle()
            frag.arguments = b
            b.putParcelableArrayList(LIST, data)
            return frag
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as AgendaActivity).gifApplication().component?.Inject(this)
        eventsList = arguments!!.getParcelableArrayList(LIST)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_events_list, container, false)
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list.layoutManager = LinearLayoutManager(context)
        eventsList?.let {

            list.adapter = EventsListAdapter(it, onCareerlistClick = { id ->
                progress.visibility = View.VISIBLE
                getUserCheckList(id)
            })

        }
    }

    private fun getUserCheckList(id: String) {

        repo.getCheckList(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { progress.visibility = View.GONE }
                .subscribeWith(object : DisposableSingleObserver<CheckLists>() {
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        context?.tmsgError("Error")
                    }

                    override fun onSuccess(t: CheckLists) {

                        try {

                            if (t.response == "success") {

                                if (t.checklist.isNotEmpty()) {

                                    val d = CheckListDialog.newInstance(t.checklist)
                                    d.show(fragmentManager, "checklists_dialog")

                                } else {
                                    context?.tmsgInfo("No Checklist Found")
                                }

                            } else {
                                context?.tmsgInfo("No Checklist Found")
                            }


                        } catch (e: Exception) {
                            e.printStackTrace()
                            context?.tmsgError("Error")
                        }
                    }
                })

    }


}
