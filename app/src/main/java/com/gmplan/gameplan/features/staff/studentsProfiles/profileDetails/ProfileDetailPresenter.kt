package com.gmplan.gameplan.features.staff.studentsProfiles.profileDetails

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.DetailsItems
import com.gmplan.gameplan.data.pojos.studentProfileDetails.StudentProfileDetails
import com.gmplan.gameplan.data.source.TaskDataSource
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sammie on 6/8/2017.
 */
class ProfileDetailPresenter(private val repo: TaskDataSource, private val schedulers: RxSchedulers) : MvpBasePresenter<ProfileDetailView>() {

    fun getDetaimsItems(name: String) {
        val list: MutableList<DetailsItems> = ArrayList()

        list.add(DetailsItems("About $name", 0))
        list.add(DetailsItems("Videos", 1))
        list.add(DetailsItems("Achievements", 2))
        list.add(DetailsItems("Favorite Colleges", 3))
        list.add(DetailsItems("Semester Goals", 4))
        list.add(DetailsItems("My Careers", 5))
        list.add(DetailsItems("Interest Profile", 6))

        if (isViewAttached)
            view?.detailsItems(list)
    }



}