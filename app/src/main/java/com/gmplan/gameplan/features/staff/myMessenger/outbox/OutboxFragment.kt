package com.gmplan.gameplan.features.staff.myMessenger.outbox


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.MessagesObject
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.staff.myMessenger.inbox.InboxAdapter
import com.gmplan.gameplan.features.staff.myMessenger.MyMessengerActivity
import com.gmplan.gameplan.util.AppConst
import com.gmplan.gameplan.util.Utilz
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_outbox.*
import javax.inject.Inject


class OutboxFragment : Fragment() {

    private val disposal = CompositeDisposable()

    @Inject
    lateinit var repo: TaskRepository

    var adapter: OutboxAdapter? = null

    companion object {
        fun newInstance() = OutboxFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_outbox, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        (activity as MyMessengerActivity).gifApplication().component?.Inject(this)
        adapter = OutboxAdapter(context = (activity as MyMessengerActivity).applicationContext)

        outbox_list.layoutManager = LinearLayoutManager(context)
        outbox_list.adapter = adapter

        getInboxList()

        outbox_sr.setOnRefreshListener { getInboxList() }
    }

    private fun getInboxList() {
        outbox_sr.isRefreshing = true
        val staffId = repo.getUserData(AppConst.USER_DATA).userData.admissionStaff.id
        disposal.add(repo.getOutboxList(staffId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { outbox_sr.isRefreshing = false }
                .subscribeWith(object : DisposableSingleObserver<List<MessagesObject>>() {
                    override fun onSuccess(t: List<MessagesObject>) {

                        try {
                            if (t.isNotEmpty()) {
                                adapter?.addData(t)
                            } else {
                                Utilz.tmsg(context, "No Item In Inbox")
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            Utilz.tmsg(context, "Error")
                        }

                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        Utilz.tmsg(context, "Error")
                    }

                }))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposal.clear()
    }


}
