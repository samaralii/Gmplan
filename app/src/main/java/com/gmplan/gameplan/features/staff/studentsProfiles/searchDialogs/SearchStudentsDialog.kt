package com.gmplan.gameplan.features.staff.studentsProfiles.searchDialogs

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.AppCompatSpinner
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import butterknife.BindView
import butterknife.BindViews
import butterknife.ButterKnife
import com.gmplan.gameplan.NewDefaultSpinnerAdapter
import com.gmplan.gameplan.R
import com.gmplan.gameplan.util.Utilz
import mabbas007.tagsedittext.TagsEditText
import java.util.*

/**
 * Created by Sammie on 6/6/2017.
 */
class SearchStudentsDialog(val schoolList: List<String>, val citiesList: List<String>,
                           val countriesList: List<String>, val statesList: List<String>,
                           val listener: DialogListener) : DialogFragment() {

    var unbinder: butterknife.Unbinder? = null

    @BindViews(
            R.id.frag_search_student_etName,
            R.id.frag_search_student_etUgpafrom,
            R.id.frag_search_student_etUgpato,
            R.id.frag_search_student_etWgpato,
            R.id.frag_search_student_etWgpafrom,
            R.id.frag_search_student_etRankfrom,
            R.id.frag_search_student_etRankto,
            R.id.frag_search_student_etSatfrom,
            R.id.frag_search_student_etSatto,
            R.id.frag_search_student_etActfrom,
            R.id.frag_search_student_etActto)
    lateinit var editTexts: List<@JvmSuppressWildcards EditText>

    @BindView(R.id.frag_search_student_Schools)
    lateinit var spnSchools: AppCompatSpinner

    @BindView(R.id.frag_search_student_Cities)
    lateinit var spnCities: AppCompatSpinner

    @BindView(R.id.frag_search_student_Countries)
    lateinit var spnCountries: AppCompatSpinner

    @BindView(R.id.frag_search_student_States)
    lateinit var spnStates: AppCompatSpinner

    @BindView(R.id.frag_search_student_etName)
    lateinit var etName: EditText

    @BindView(R.id.frag_search_student_etCities)
    lateinit var etCities: TagsEditText


    val clearText: ButterKnife.Action<EditText> = ButterKnife.Action<EditText> { view, _ -> view.setText("") }


    override fun onCreate(savedInstanceState: android.os.Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.frag_search_student, container, false) as android.view.View
        unbinder = butterknife.ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: android.view.View, savedInstanceState: android.os.Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    val tags = arrayOf<String>()

    private fun init() {
        dialog.setTitle("Search Student")

        spnCities.adapter = NewDefaultSpinnerAdapter(citiesList)
        spnStates.adapter = NewDefaultSpinnerAdapter(statesList)
        spnCountries.adapter = NewDefaultSpinnerAdapter(countriesList)
        spnSchools.adapter = NewDefaultSpinnerAdapter(schoolList)


        etCities.visibility = View.GONE

        val list: MutableList<String> = ArrayList()
        list.add("a")
        list.add("b")
        list.add("c")
        list.add("d")
        list.add("e")

        etCities.setOnClickListener {


            val dialog = Utilz.getPickerDialog(activity!!, "Test", list,
                    btnDone = { value ->
                        tags[tags.size - 1] = value
                        etCities.setTags(tags)
                    })

            dialog.show()

        }


    }


    @butterknife.OnClick(R.id.frag_search_student_btnReset)
    fun onBtnResetClick() {
        butterknife.ButterKnife.apply(editTexts, clearText)
    }

    @butterknife.OnClick(R.id.frag_search_student_btnSearch)
    fun onBtnSearchClick() {

        var name = ""

        if (etName.text.isNotEmpty())
            name = etName.text.toString()

        listener.onSearchClick(name, dialog)
    }

    @butterknife.OnClick(R.id.frag_search_student_btnBack)
    fun onBtnBackClick() {
        dialog.dismiss()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

    interface DialogListener {
        fun onSearchClick(name: String, dialog: Dialog)
    }

}