package com.gmplan.gameplan.features.staff.studentsProfiles.detailFragment

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.studentProfileDetails.Video
import com.gmplan.gameplan.util.Utilz

/**
 * Created by Sammie on 6/9/2017.
 */
class DetailVideoAdapter(val data: List<Video>, val context: Context) :
        RecyclerView.Adapter<DetailVideoAdapter.DetailVideoVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailVideoVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_detailvideos, parent, false)
        return DetailVideoVH(v)
    }

    override fun onBindViewHolder(holder: DetailVideoVH, position: Int) {

        Glide.with(context)
                .load(Utilz.extractImgUrl(data[position].url))
                .dontAnimate()
                .into(holder.image)

        holder.title.text = data[position].name


    }

    override fun getItemCount(): Int {
        return data.size
    }




    inner class DetailVideoVH(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_myvideos_thumnail)
        lateinit var image: ImageView

        @BindView(R.id.list_myvieos_tvTitle)
        lateinit var title: TextView

        init {
            ButterKnife.bind(this, v)
        }



    }



}