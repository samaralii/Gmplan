package com.gmplan.gameplan.features.mygmplan.gmplanMessagesListFragment

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.GmpMessages
import com.gmplan.gameplan.data.source.TaskDataSource
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sammie on 5/26/2017.
 */
class GmplanMessagesPresenter(val repo: TaskDataSource, val schedulers: RxSchedulers): MvpBasePresenter<GmplanMessagesView>() {



    fun getGmpMessages(studentId: String) {
        repo.getGmpMessages(studentId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<GmpMessages>() {
                    override fun onError(p0: Throwable) {

                        p0?.printStackTrace()

                        if (isViewAttached)
                            view?.onFailedGettingMessages()

                    }

                    override fun onSuccess(p0: GmpMessages) {

                        if (p0.response == "success") {

                            if (p0.gmessages.size > 0) {

                                if (isViewAttached)
                                    view?.onSuccessfullyMessagesFound(p0.gmessages)

                            } else {

                                if (isViewAttached)
                                    view?.onNoMessagesFound()

                            }

                        } else {

                            if (isViewAttached)
                                view?.onFailedGettingMessages()

                        }

                    }
                })

    }
}