package com.gmplan.gameplan.features.feed

import com.gmplan.gameplan.data.pojos.staffFeed.StaffNewssFeed
import com.gmplan.gameplan.data.pojos.staffStudent.StaffStudent
import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Sammie on 5/15/2017.
 */
interface FeedView : MvpView{
    fun onErrorGettingFeed()
    fun dismissDialog()
    fun onSuccessFeed(p0: List<StaffNewssFeed>)
    fun onNoFeedFound()
}