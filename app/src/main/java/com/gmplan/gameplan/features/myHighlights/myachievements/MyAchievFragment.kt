package com.gmplan.gameplan.features.myHighlights.myachievements

import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.Achievement
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.myHighlights.MyHighlightsActivity
import com.gmplan.gameplan.util.Utilz
import com.hannesdorfmann.mosby.mvp.MvpFragment
import javax.inject.Inject

/**
 * Created by Sammie on 5/22/2017.
 */
class MyAchievFragment : MvpFragment<MyAchievView, MyAchievPresenter>(),
        MyAchievView, AddAchievementDialog.DialogListener, MyAchievAdapter.Listener {


    @Inject
    lateinit var schedulers: RxSchedulersImpl

    @Inject
    lateinit var taskRepository: TaskRepository

    var unbinder: Unbinder? = null

    @BindView(R.id.frag_achiev_list)
    lateinit var recyclerView: RecyclerView

    @BindView(R.id.progressDialog)
    lateinit var progressDialog: View

    @BindView(R.id.frag_achiev_sr)
    lateinit var swipeToRefresh: SwipeRefreshLayout


    companion object {

        fun getInstance(): MyAchievFragment {
            return MyAchievFragment()
        }

    }

    override fun createPresenter(): MyAchievPresenter {
        (activity as MyHighlightsActivity).gifApplication().component?.Inject(this)
        return MyAchievPresenter(taskRepository, schedulers)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.frag_myachiev, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        recyclerView.layoutManager = LinearLayoutManager(context)

        refreshList()



        swipeToRefresh.setOnRefreshListener {

            refreshList()

        }

    }


    fun refreshList() {
        showProgress()
        presenter.getUserAchievements((activity as MyHighlightsActivity).getUserData?.student?.id.toString())
    }

    @OnClick(R.id.frag_achiev_btnAdd)
    fun onAddClick() {
        val dialog = AddAchievementDialog(this)
        dialog.show(fragmentManager, "dialogg")
    }

    override fun onPostClick(title: String, detail: String, dialog: DialogInterface) {
        dialog.dismiss()
        showProgress()
        presenter.postAchievement((activity as MyHighlightsActivity).getUserData?.student?.id.toString(),
                title, detail)
    }


    private fun showProgress() {

//        if (!progressDialog.isShown)
//            progressDialog.visibility = View.VISIBLE

        swipeToRefresh.isRefreshing = true

    }


    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

    override fun dismissDialog() {


        if (progressDialog.isShown)
            progressDialog.visibility = View.GONE

        if (swipeToRefresh.isRefreshing)
            swipeToRefresh.isRefreshing = false

    }

    override fun onFailedGettingList() {
        Utilz.tmsgError(context, "Error")
    }

    override fun onNoAchievementsFound() {
        Utilz.tmsgError(context, "No Achievements Found")
    }

    override fun onSuccessAchievementsFound(achievement: MutableList<Achievement>) {
        recyclerView.adapter = MyAchievAdapter(achievement, this)
    }

    override fun onFailedPosting() {
        Utilz.tmsgError(context, "Error")
    }

    override fun onSuccessPost() {
        Utilz.tmsgSuccess(context, "Done")
        refreshList()

    }


}