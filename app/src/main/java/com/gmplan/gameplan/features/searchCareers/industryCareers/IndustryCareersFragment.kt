package com.gmplan.gameplan.features.searchCareers.industryCareers

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.industryCareersSearch.Career
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.searchCareers.SearchCareersActivity
import com.gmplan.gameplan.util.hide
import com.gmplan.gameplan.util.show
import com.gmplan.gameplan.util.tmsgError
import com.gmplan.gameplan.util.tmsgSuccess
import com.hannesdorfmann.mosby.mvp.MvpFragment
import javax.inject.Inject

/**
 * Created by Sammie on 9/7/2017.
 */
class IndustryCareersFragment : MvpFragment<IndustryCareersView, IndustryCareersPresenter>(), IndustryCareersView, IndustryCareersAdapter.ListListener {


    var unbinder: Unbinder? = null

    @Inject
    lateinit var schedulers: RxSchedulersImpl

    @Inject
    lateinit var taskRepository: TaskRepository

    @BindView(R.id.frag_industry_careers_list)
    lateinit var list: RecyclerView

    @BindView(R.id.sr)
    lateinit var sr: SwipeRefreshLayout

    var careerId = ""
    var keyword = ""
    var isSearch = false

    companion object {

        private val CAREER_ID = "careerid"
        private val KEYWORD = "keyword"
        private val IS_SEARCH = "issearch"

        fun newInstance(careerId: String = "", isSearchCareer: Boolean = false, keyword: String = ""): IndustryCareersFragment {
            val fragment = IndustryCareersFragment()
            val b = Bundle()
            b.putBoolean(IS_SEARCH, isSearchCareer)
            b.putString(CAREER_ID, careerId)

            if (isSearchCareer)
                b.putString(KEYWORD, keyword)

            fragment.arguments = b
            return fragment
        }

    }

    override fun createPresenter(): IndustryCareersPresenter {
        (activity as SearchCareersActivity).gifApplication().component?.Inject(this)
        return IndustryCareersPresenter(taskRepository, schedulers)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        isSearch = arguments!!.getBoolean(IS_SEARCH, false)

        if (isSearch)
            keyword = arguments!!.getString(KEYWORD)
        else
            careerId = arguments!!.getString(CAREER_ID)


    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.frag_industry_careers, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        list.layoutManager = LinearLayoutManager(context)

        refreshList()

        sr.setOnRefreshListener {
            refreshList()
        }

    }

    private fun refreshList() {
        sr.show()

        if (isSearch)
            presenter.getListByKeyword(keyword, (activity as SearchCareersActivity).getUserData?.student?.id as String)
        else
            presenter.getList(careerId, (activity as SearchCareersActivity).getUserData?.student?.id as String)

    }

    override fun dismissProgress() {
        sr.hide()
    }

    override fun onErrorGettingList() {
        context!!.tmsgError("Error")
    }

    override fun onSuccessGettingList(careers: List<Career>) {
        list.adapter = IndustryCareersAdapter(careers, this)
    }

    override fun onAddClick(career: Career) {
        sr.show()
        presenter.addCareer(career.code,
                (activity as SearchCareersActivity).getUserData?.student?.id as String, career.title)
    }


    override fun onErrorAdding() {
        context!!.tmsgError("Error")
    }


    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

    override fun SuccessAddedCareer() {
        context!!.tmsgSuccess("Added")
    }


}