package com.gmplan.gameplan.features.staff.studentsProfiles.detailFragment

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.studentProfileDetails.Achievement

/**
 * Created by Sammie on 6/12/2017.
 */
class DetailAchievAdapter(private val data: List<Achievement>) : RecyclerView.Adapter<DetailAchievAdapter.DetailAchievVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailAchievVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_detail_achiev, parent, false)
        return DetailAchievVH(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: DetailAchievVH, position: Int) {
        holder.tvTitle.text = data[position].title
        holder.tvDetail.text = data[position].detail
    }


    inner class DetailAchievVH(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_detail_achiev_detail)
        lateinit var tvDetail: TextView

        @BindView(R.id.list_detail_achiev_title)
        lateinit var tvTitle: TextView

        @BindView(R.id.list_detail_achiev_like)
        lateinit var like: ImageView


        init {
            ButterKnife.bind(this, v)
        }

        @OnClick(R.id.list_detail_achiev_title)
        fun onLikeClick() {

        }

    }
}