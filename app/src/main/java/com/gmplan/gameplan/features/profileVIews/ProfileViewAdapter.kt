package com.gmplan.gameplan.features.profileVIews

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.P_View

class ProfileViewAdapter(val data: List<P_View>) : RecyclerView.Adapter<ProfileViewAdapter.ProfileViewsViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfileViewsViewHolder {
        val v: View = LayoutInflater.from(parent.context).inflate(R.layout.list_profileviews, parent, false)
        return ProfileViewsViewHolder(v)
    }

    override fun onBindViewHolder(holder: ProfileViewsViewHolder, position: Int) {
        holder.collegeName.text = data[position].collegename
        holder.noOfProfileViews.text = "Views ${data[position].profileViews}"

        val staffName = if (data[position].staffname != null) data[position].staffname else ""
        val jobTitle = if (data[position].jobtitle != null) "(${data[position].jobtitle})" else ""

        holder.staffName.text = "$staffName $jobTitle"
    }


    override fun getItemCount(): Int {
        return data.size
    }


    class ProfileViewsViewHolder(v: View?) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_profileviews_collegeName)
        lateinit var collegeName: TextView

        @BindView(R.id.list_profileviews_Views)
        lateinit var noOfProfileViews: TextView

        @BindView(R.id.list_profileviews_staffName)
        lateinit var staffName: TextView

        init {
            ButterKnife.bind(this, v as View)
        }

    }
}