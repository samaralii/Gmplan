package com.gmplan.gameplan.features.Login

import android.util.Log
import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.userPojo.UserPojo
import com.gmplan.gameplan.data.source.TaskDataSource
import com.gmplan.gameplan.util.AppConst
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import com.onesignal.OneSignal
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver


class LoginPresenter(private val repository: TaskDataSource, private val schedulers: RxSchedulers) : MvpBasePresenter<LoginView>() {


    private val disposable: CompositeDisposable = CompositeDisposable()

    fun LoginUser(email: String, password: String, userId: String) {

        disposable.add(repository.LoginUser(email, password, userId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally({

                    if (isViewAttached)
                        view?.onFinally()

                })
                .subscribeWith(object : DisposableSingleObserver<UserPojo>() {
                    override fun onSuccess(t: UserPojo) {

                        if (t.response.equals("success", true)) {

                            repository.addValueInPref(AppConst.IS_FIRST_TIME_LOGIN, false)
                            repository.addValueInPref(AppConst.USER_DATA, t)

                            if (isViewAttached)
                                view?.onSuccessLogin(t)

                        } else {

                            if (isViewAttached)
                                view?.onWrongCredentials()

                        }


                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()

                        if (isViewAttached)
                            view?.onErrorLogin()
                    }

                }))

    }

    fun getOneSignalId(email: String, password: String) {
        OneSignal.idsAvailable { userId, registrationId ->
            Log.d("debug", "User:" + userId)

            LoginUser(email, password, userId)

            if (registrationId != null)
                Log.d("debug", "registrationId:" + registrationId)
        }
    }

    override fun detachView(retainInstance: Boolean) {
        super.detachView(retainInstance)

        if (!retainInstance)
            disposable.clear()

    }

    fun checkForPreviousLogin() {
        if (!repository.getBooleanByKey(AppConst.IS_FIRST_TIME_LOGIN)) {

            val check = repository.getStringByKey(AppConst.IS_FINGERPRINT_ENABLED)

            if (check != null && check == "true") {
                if (isViewAttached) view?.enableFingerprintViews()
            } else {
                if (isViewAttached) view?.openFeedActivity()
            }

        }
    }

    fun enableFingerprintLogin() {
        repository.addValueInPref(AppConst.IS_FINGERPRINT_ENABLED, "true")
    }

    fun disableFingerprintLogin() {
        repository.deleteKey(AppConst.IS_FINGERPRINT_ENABLED)
    }

}