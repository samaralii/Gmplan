package com.gmplan.gameplan.features.searchCareers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos._Career

/**
 * Created by Sammie on 9/7/2017.
 */

class CareersSpinnerAdapter(val context: Context, val data: MutableList<_Career>) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return getCustomView(position, parent as ViewGroup)
    }

    override fun getItem(position: Int): _Career {
        return data[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return data.size
    }


    private fun getCustomView(pos: Int, parent: ViewGroup): View {

        val inflater: LayoutInflater =
                parent.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val view: View = inflater.inflate(R.layout.spinner_careers_list, parent, false)

        val name = view.findViewById<TextView>(R.id.tvName) as TextView

        name.text = data[pos].title

        return view

    }


}
