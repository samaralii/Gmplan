package com.gmplan.gameplan.features.staff.myMessenger.searchStudent

import android.app.Dialog
import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.widget.AppCompatSpinner
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import butterknife.*
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.NewDefaultSpinnerAdapter
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.staffStudent.StaffStudent
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.staff.myMessenger.MyMessengerActivity
import com.gmplan.gameplan.util.Utilz
import com.hannesdorfmann.mosby.mvp.MvpFragment
import javax.inject.Inject

/**
 * Created by Sammie on 6/20/2017.
 */
class SearchStudentFragment : MvpFragment<SearchStudentView, SearchStudentPresenter>(), SearchStudentView, ComposeMessageDialog.DialogListener {


    @Inject
    lateinit var schedulers: RxSchedulersImpl

    @Inject
    lateinit var taskRepository: TaskRepository

    @BindView(R.id.frag_search_student_Schools)
    lateinit var spnSchools: AppCompatSpinner

    @BindView(R.id.frag_search_student_Cities)
    lateinit var spnCities: AppCompatSpinner

    @BindView(R.id.frag_search_student_Countries)
    lateinit var spnCountries: AppCompatSpinner

    @BindView(R.id.frag_search_student_States)
    lateinit var spnStates: AppCompatSpinner

    @BindView(R.id.frag_search_student_etName)
    lateinit var etName: EditText


    private var unbinder: Unbinder? = null

    private var pd: ProgressDialog? = null

    @BindViews(
            R.id.frag_search_student_etName,
            R.id.frag_search_student_etUgpafrom,
            R.id.frag_search_student_etUgpato,
            R.id.frag_search_student_etWgpato,
            R.id.frag_search_student_etWgpafrom,
            R.id.frag_search_student_etRankfrom,
            R.id.frag_search_student_etRankto,
            R.id.frag_search_student_etSatfrom,
            R.id.frag_search_student_etSatto,
            R.id.frag_search_student_etActfrom,
            R.id.frag_search_student_etActto)
    lateinit var editTexts: List<@JvmSuppressWildcards EditText>

    val clearText: ButterKnife.Action<EditText> = ButterKnife.Action<EditText> { view, _ -> view.setText("") }


    override fun createPresenter(): SearchStudentPresenter {
        (activity as MyMessengerActivity).gifApplication().component?.Inject(this)
        return SearchStudentPresenter(taskRepository, schedulers)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        pd = Utilz.dialog(context!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.frag_messenger_search_student, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {




        pd?.show()
        presenter.getStudentFieldsValues((activity as MyMessengerActivity).getUserData?.admissionStaff?.id.toString())

    }

    @butterknife.OnClick(R.id.frag_search_student_btnReset)
    fun onBtnResetClick() {
        butterknife.ButterKnife.apply(editTexts, clearText)
    }

    @OnClick(R.id.frag_search_student_btnSearch)
    fun onSearchPress() {

        if (etName.text.isEmpty()) {
            etName.error = "Required Field"
            return
        } else
            etName.error = null


        pd?.show()
        presenter.searchStaffStudent(
                (activity as MyMessengerActivity).getUserData?.admissionStaff?.id.toString(),
                (activity as MyMessengerActivity).getUserData?.admissionStaff?.collegeId.toString(),
                etName.text.toString()
        )
    }

    override fun onStudentsFound(p0: List<StaffStudent>) {
        val dialog = ComposeMessageDialog(p0, this)
        dialog.show(fragmentManager, "compose message dialog")
    }

    override fun onErrorSearchStudent() {
        Utilz.tmsgError(context, "Error")
    }


    override fun onSendClick(to: String, subject: String, message: String, dialog: Dialog) {

        pd?.show()
        presenter.sendMessage(
                (activity as MyMessengerActivity).getUserData?.admissionStaff?.collegeId.toString(),
                (activity as MyMessengerActivity).getUserData?.admissionStaff?.id.toString(),
                (activity as MyMessengerActivity).getUserData?.admissionStaff?.name.toString(),
                to,
                (activity as MyMessengerActivity).getUserData?.admissionStaff?.jobTitle.toString(),
                (activity as MyMessengerActivity).getUserData?.admissionStaff?.collegeId.toString(),
                (activity as MyMessengerActivity).getUserData?.user?.id.toString(),
                subject, message, dialog)

    }

    override fun onSuccessMessageSent() {
        Utilz.tmsgSuccess(context, "Message Sent")
    }

    override fun onErrorMessageSent() {
        Utilz.tmsgError(context, "Error")
    }

    override fun dismissDialog() {
        pd?.let {
            if (it.isShowing)
                it.dismiss()
        }
    }

    override fun onErrorGettingFieldsValue() {
        Utilz.tmsgError(context, "Error")
    }

    override fun onSuccessGettingFieldsValue(schoolList: MutableList<String>, citiesList: MutableList<String>,
                                             countriesList: MutableList<String>, statesList: MutableList<String>) {

        spnCities.adapter = NewDefaultSpinnerAdapter(citiesList)
        spnStates.adapter = NewDefaultSpinnerAdapter(statesList)
        spnCountries.adapter = NewDefaultSpinnerAdapter(countriesList)
        spnSchools.adapter = NewDefaultSpinnerAdapter(schoolList)

    }


    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }
}