package com.gmplan.gameplan.features.agenda

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.CheckList


class CheckListAdapter(val data: ArrayList<CheckList>) : RecyclerView.Adapter<CheckListVH>() {

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: CheckListVH, pos: Int) {
        holder.text?.text = data[pos].title
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): CheckListVH {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.list_checklist, p0, false)
        return CheckListVH(v)
    }

}


class CheckListVH(v: View) : RecyclerView.ViewHolder(v) {
    val text by lazy { v.findViewById<TextView>(R.id.lists_checklist_text) }
}