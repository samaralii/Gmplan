package com.gmplan.gameplan.features.dashboard.items

import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.DashboardItems
import com.gmplan.gameplan.util.AppConst
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter

/**
 * Created by Sammie on 5/15/2017.
 */
class DashboardItemsPresenter : MvpBasePresenter<DashboardItemsView>() {

    fun getDashboardItems(type: Int?) {

        val items: MutableList<DashboardItems> = ArrayList()

        if (type == AppConst.TYPE_STUDENT) {

            items.add(DashboardItems(1, R.drawable.ic_new_mycareers, R.string.my_careers, "#e57e22"))
            items.add(DashboardItems(2, R.drawable.ic_new_mycolleges, R.string.my_colleges, "#1ec0b7"))
            items.add(DashboardItems(3, R.drawable.ic_new_gmplan, R.string.my_gmplan, "#7bbc4a"))
            items.add(DashboardItems(4, R.drawable.ic_new_mymessages, R.string.my_message, "#e64c3c"))
            items.add(DashboardItems(5, R.drawable.ic_new_myhighlights, R.string.my_highlights, "#ae64cb"))
//            items.add(DashboardItems(6, R.drawable.ic_new_myoffers, R.string.my_offers, "#1bb9e0"))
            items.add(DashboardItems(7, R.drawable.ic_new_mysemester, R.string.my_semestergoals, "#59c8e4"))
            items.add(DashboardItems(9, R.drawable.ic_calendar, R.string.staff_my_agenda, "#59c8e4"))

        } else if (type == AppConst.TYPE_STAFF) {

            items.add(DashboardItems(8, R.drawable.ic_new_mycareers, R.string.staff_student_profile, "#e57e22"))
            items.add(DashboardItems(9, R.drawable.ic_calendar, R.string.staff_my_agenda, "#59c8e4"))
            items.add(DashboardItems(10, R.drawable.ic_new_mymessages, R.string.staff_my_messages, "#1bb9e0"))
            items.add(DashboardItems(11, R.drawable.ic_new_updatestatus, R.string.staff_update_status, "#e64c3c"))

        }



        if (isViewAttached)
            view?.dashBoardItems(items)

    }

    fun onItemClick(id: Int) {


        when (id) {

            1 -> {

                if (isViewAttached)
                    view?.openMyCareerActivity()

            }

            2 -> {

                if (isViewAttached)
                    view?.openMyCollegeActivity()


            }

            3 -> {


                if (isViewAttached)
                    view?.openMyGmplanActivity()

            }

            4 -> {

                if (isViewAttached)
                    view?.openMessagesActivity()


            }

            5 -> {

                if (isViewAttached)
                    view?.openHighlightActivity()

            }

            6 -> {


                if (isViewAttached)
                    view?.myOffersActivity()

            }

            7 -> {

                if (isViewAttached)
                    view?.openMySemestersActivity()

            }

            8 -> {

                if (isViewAttached)
                    view?.openStudentProfileActivity()

            }

            9 -> {

                if (isViewAttached)
                    view?.openAgendaActivity()

            }

            10 -> {

                if (isViewAttached)
                    view?.openMyMessageActivity()

            }

            11 -> {

                if (isViewAttached)
                    view?.openUpdateStatusActivity()

            }

            12 -> {

                if (isViewAttached)
                    view?.openProfileActivity()

            }

        }

    }

}