package com.gmplan.gameplan.features.agenda.createAgenda

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.CheckListsEvents
import com.gmplan.gameplan.data.pojos.EventsNotification


class CreateNotificationAdapter(var data: MutableList<EventsNotification>,
                                val onEditClick: (EventsNotification, Int) -> Unit)
    : RecyclerView.Adapter<CreateNotificationVH>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): CreateNotificationVH {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.list_createnotification, p0, false)
        return CreateNotificationVH(v)
    }

    override fun onBindViewHolder(holder: CreateNotificationVH, pos: Int) {

        holder.edit.setOnClickListener {
            onEditClick.invoke(data[holder.layoutPosition], holder.layoutPosition)
        }

        holder.delete.setOnClickListener {
            data.removeAt(holder.layoutPosition)
            notifyDataSetChanged()
        }

        holder.durationText?.text = data[pos].text
        holder.duration?.text = data[pos].duration


    }

    override fun getItemCount() = data.size

    fun addItem(event: EventsNotification) {
        data.add(event)
        notifyDataSetChanged()
    }

    fun editItem(event: EventsNotification, pos: Int) {
        data.removeAt(pos)
        data.add(pos, event)
        notifyDataSetChanged()
    }

    fun getList(): List<EventsNotification>{
        return data
    }


}

class CreateNotificationVH(v: View) : RecyclerView.ViewHolder(v) {
    val durationText by lazy { v.findViewById<TextView>(R.id.list_tvdurationText) }
    val duration by lazy { v.findViewById<TextView>(R.id.llist_duration) }
    val edit by lazy { v.findViewById<TextView>(R.id.list_tvEdit) }
    val delete by lazy { v.findViewById<TextView>(R.id.list_tvDelete) }
}