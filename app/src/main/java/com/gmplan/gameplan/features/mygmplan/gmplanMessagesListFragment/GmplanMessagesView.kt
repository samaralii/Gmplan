package com.gmplan.gameplan.features.mygmplan.gmplanMessagesListFragment

import com.gmplan.gameplan.data.pojos.Gmessage
import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Sammie on 5/26/2017.
 */
interface GmplanMessagesView: MvpView {
    fun dismissDialog()
    fun onFailedGettingMessages()
    fun onNoMessagesFound()
    fun onSuccessfullyMessagesFound(gmessages: List<Gmessage>)
}