package com.gmplan.gameplan.features.editProfile.userInfoEdit

import com.hannesdorfmann.mosby.mvp.MvpView

interface UserInfoEditView : MvpView {
    fun dismissProgress()
    fun onFailedEditProfile()
    fun onSuccessEditProfile()
    fun setTags(myLike: Array<String?>?, myDislike: Array<String?>?, myMusic: Array<String?>?, myTeam: Array<String?>?, myCollege: Array<String?>?)
}