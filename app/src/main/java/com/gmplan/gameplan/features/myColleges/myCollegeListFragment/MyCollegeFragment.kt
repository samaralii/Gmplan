package com.gmplan.gameplan.features.myColleges.myCollegeListFragment

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.College
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.CustomWebViewActivity
import com.gmplan.gameplan.features.myCareers.MyCareersActivity
import com.gmplan.gameplan.features.myColleges.MyCollegesActivity
import com.gmplan.gameplan.util.Utilz
import com.hannesdorfmann.mosby.mvp.MvpFragment
import javax.inject.Inject

/**
 * Created by Sammie on 5/18/2017.
 */
class MyCollegeFragment : MvpFragment<MyCollegeView, MyCollegePresenter>(), MyCollegeView, CollegeAdapter.Listener {


    @Inject
    lateinit var schedulers: RxSchedulersImpl

    @Inject
    lateinit var taskRepository: TaskRepository

    var unbinder: Unbinder? = null


    lateinit var pd: ProgressDialog


    var collegeAdapter: CollegeAdapter? = null


    @BindView(R.id.frag_mycarrer_collegeList)
    lateinit var recyclerView: RecyclerView

    @BindView(R.id.frag_mycolleges_sr)
    lateinit var swipeRefresh: SwipeRefreshLayout

    override fun createPresenter(): MyCollegePresenter {
        (activity as MyCollegesActivity).gifApplication().component?.Inject(this)
        return MyCollegePresenter(taskRepository, schedulers)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pd = Utilz.dialog(context!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.frag_mycollege, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        recyclerView.layoutManager = LinearLayoutManager(context)

        refreshList()

        swipeRefresh.setOnRefreshListener {

            refreshList()

        }



    }

    fun refreshList() {
        pd.show()
        presenter.getUserColleges((activity as MyCollegesActivity).getUserData?.student?.id.toString())
    }

    override fun dismissDialog() {
        pd.dismiss()


        if (swipeRefresh.isRefreshing)
            swipeRefresh.isRefreshing = false
    }


    override fun onFailed() {
        Utilz.tmsgError(context, "Error")
    }

    override fun collegeList(colleges: List<College>) {
        recyclerView.adapter = CollegeAdapter(colleges, this, context!!)
    }

    override fun onNoCollegeFound() {
        Utilz.tmsgInfo(context!!, "No College Found")
    }

    override fun onItemClick(code: String) {
        val i = Intent(context, CustomWebViewActivity::class.java)
        i.putExtra("url", "https://gmplan.com/Careers/api_schooldetails/$code")
        i.putExtra("job", "Detail")
        startActivity(i)


    }



}