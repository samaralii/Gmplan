package com.gmplan.gameplan.features.staff.studentsProfiles.studentListFragment

import android.app.Dialog
import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.School_
import com.gmplan.gameplan.data.pojos.StudentsCareers
import com.gmplan.gameplan.data.pojos.staffStudent.StaffStudent
import com.gmplan.gameplan.data.pojos.studentProfileDetails.StudentProfileDetails
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.staff.studentsProfiles.StudentProfileActivity
import com.gmplan.gameplan.features.staff.studentsProfiles.profileDetails.ProfileDetailFragment
import com.gmplan.gameplan.features.staff.studentsProfiles.searchDialogs.SearchStudentsByCareerDialog
import com.gmplan.gameplan.features.staff.studentsProfiles.searchDialogs.SearchStudentsDialog
import com.gmplan.gameplan.util.AppConst
import com.gmplan.gameplan.util.Utilz
import com.hannesdorfmann.mosby.mvp.MvpFragment
import kotlinx.android.synthetic.main.frag_student_profile.*
import javax.inject.Inject

class StudentProfileFragment : MvpFragment<StudentProfileView, StudentProfilePresenter>(),
        StudentProfileView, SearchStudentsDialog.DialogListener, StudentProfileAdapter.Listener, SearchStudentsByCareerDialog.DialogListener {


    @BindView(R.id.frag_sp_list)
    lateinit var recyclerView: RecyclerView

//    @BindView(R.id.frag_student_profile_sr)
//    lateinit var sr: SwipeRefreshLayout

    var unbinder: Unbinder? = null

    @Inject
    lateinit var taskRepository: TaskRepository

    @Inject
    lateinit var schedulers: RxSchedulersImpl

    var pd: ProgressDialog? = null

    var data: School_? = null

    companion object {
        public val SCHOOL_DATA = "students"
        public fun newInstance(data: School_) = StudentProfileFragment().apply {
            arguments = Bundle().apply {
                putParcelable(SCHOOL_DATA, data)
            }
        }
    }

    override fun createPresenter(): StudentProfilePresenter {
        (activity as StudentProfileActivity).gifApplication().component?.Inject(this)
        return StudentProfilePresenter(taskRepository, schedulers)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pd = Utilz.dialog(context!!)
        data = arguments?.getParcelable(SCHOOL_DATA)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.frag_student_profile, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        recyclerView.layoutManager = LinearLayoutManager(context)
        refreshList()
    }

    fun refreshList() {
//        presenter.getStaffStudents(
//                (activity as StudentProfileActivity).getUserData?.admissionStaff?.id.toString(),
//                (activity as StudentProfileActivity).getUserData?.admissionStaff?.collegeId.toString())

        data?.let {
            if (it.Student != null && it.Student.isNotEmpty()) {
                val list = it.Student as MutableList
                recyclerView.adapter = StudentProfileAdapter(list, this)
            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

//    @OnClick(R.id.frag_sp_simpleSearch)
    fun onSimpleSearch() {
        pd?.show()
        presenter.getStudentFieldsValues((activity as StudentProfileActivity).getUserData?.admissionStaff?.id.toString())

    }

    override fun onSuccessGettingFieldsValue(schoolList: MutableList<String>, citiesList: MutableList<String>, countriesList: MutableList<String>, statesList: MutableList<String>) {
        val dialog = SearchStudentsDialog(schoolList, citiesList, countriesList, statesList, this)
        dialog.show(fragmentManager, "dialog")
    }


    override fun onErrorGettingFieldsValue() {
        Utilz.tmsgError(context, "Error")
    }

    override fun onSearchClick(name: String, dialog: Dialog) {
        Utilz.tmsg(context, "Search Student")
//        fab_menu.close(true)
        pd?.show()
        presenter.searchStaffStudent((activity as StudentProfileActivity).getUserData?.admissionStaff?.id.toString()
                , (activity as StudentProfileActivity).getUserData?.admissionStaff?.collegeId.toString(), name, dialog)
    }


//    @OnClick(R.id.frag_sp_searchByCareer)
    fun onSearchByCareer() {
        pd?.show()
        presenter.getStudentCareerList()
//        fab_menu.close(true)
    }

    override fun onErrorGettingCareerList() {
        Utilz.tmsgError(context, "Error")
    }

    override fun onSuccessCareerList(careerList: List<StudentsCareers>) {
        val dialog = SearchStudentsByCareerDialog(careerList, this)
        dialog.show(fragmentManager, "dialog")
    }

    override fun onErrorSearchingByCareer() {
        Utilz.tmsgError(context, "Error")
    }

    override fun onItemClick(career: StudentsCareers?, dialog: Dialog) {

        pd?.show()
        presenter.getStaffStudentsByCareer((activity as StudentProfileActivity).getUserData?.admissionStaff?.id.toString(),
                career?.career_code.toString(), dialog
        )


    }

    override fun dismissDialog() {
        pd?.let {
            if (it.isShowing)
                it.dismiss()
        }


//        if (sr.isRefreshing)
//            sr.isRefreshing = false
    }

    override fun onErrorGettingStudents() {
        Utilz.tmsgError(context, "Error")
    }

    override fun onNoStudentFound() {
        Utilz.tmsgInfo(context, "No Student Found")
    }

    override fun onStudentsFound(p0: List<StaffStudent>) {
//        recyclerView.adapter = StudentProfileAdapter(p0 as MutableList<StaffStudent>, this)
    }

    override fun onErrorSearchStudent() {
        Utilz.tmsgError(context, "No Student Found")
    }


    override fun onItemClick(id: String?) {
        pd?.show()
        presenter.getStudentDetailData(
                (activity as StudentProfileActivity).getUserData?.admissionStaff?.id
                , (activity as StudentProfileActivity).getUserData?.admissionStaff?.collegeId
                , id)
    }

    override fun onErrorGettingStudentDetail() {
        Utilz.tmsgError(context, "Error")
    }

    override fun onSuccessStudentDetail(p0: StudentProfileDetails) {
        val b = Bundle()
        b.putParcelable(AppConst.DATA, p0)
//        Utilz.replaceFragmentToActivity(fragmentManager, ProfileDetailFragment(),
//                R.id.activity_sp_container, b, addToBackStack = true)

        val fragment = ProfileDetailFragment()
        fragment.arguments = b

        val ft = fragmentManager!!.beginTransaction()
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
        ft.replace(R.id.activity_sp_container, fragment)
        ft.addToBackStack(null)
        ft.commit()

    }

}
