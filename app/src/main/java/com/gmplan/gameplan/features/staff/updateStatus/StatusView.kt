package com.gmplan.gameplan.features.staff.updateStatus

import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Sammie on 6/19/2017.
 */
interface StatusView: MvpView {
    fun onErrorUpdateStatus()
    fun dismissDialog()
    fun onSuccessUpdateStatus()
}