package com.gmplan.gameplan.features.interestProfile.onetIntro

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import com.github.paolorotolo.appintro.AppIntro
import com.gmplan.gameplan.features.interestProfile.questionsView.QuestionsActivity

class OnetIntroActivity : AppIntro() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addSlide(FragmentOne())
        addSlide(FragmentTwo())
        addSlide(FragmentThree())

        setBarColor(Color.parseColor("#000000"))
        setSeparatorColor(Color.parseColor("#000000"))
    }

    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        openQuestionActivity()

    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        openQuestionActivity()

    }

    private fun openQuestionActivity() {
//        startActivity(Intent(this, InterestProfileActivity::class.java))
        startActivity(Intent(this, QuestionsActivity::class.java))
        finish()
    }

}
