package com.gmplan.gameplan.features.searchCareers.industryCareers

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.industryCareersSearch.Career

/**
 * Created by Sammie on 9/7/2017.
 */
class IndustryCareersAdapter(private val data: List<Career>, private val listener: ListListener) : RecyclerView.Adapter<IndustryCareersAdapter.IndustryCareersVH>() {

    override fun onBindViewHolder(holder: IndustryCareersVH, p1: Int) {

        holder.title.text = data[p1].title

    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): IndustryCareersVH {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.list_industry_careers, p0, false)
        return IndustryCareersVH(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }


    inner class IndustryCareersVH(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_careersTitle)
        lateinit var title: TextView

        init {
            ButterKnife.bind(this, v)
        }

        @OnClick(R.id.list_delete)
        fun onDeleteClick() {
            listener.onAddClick(data[layoutPosition])
        }
    }

    interface ListListener {
        fun onAddClick(career: Career)
    }
}