package com.gmplan.gamep

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.UsersSemestersGoals
import com.gmplan.gameplan.features.mySemesters.semestersListFragment.MySemestersView
import com.google.gson.JsonElement
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sammie on 5/19/2017.
 */
class MySemestersPresenter(val repo: com.gmplan.gameplan.data.source.TaskDataSource, val schedulers: RxSchedulers) : MvpBasePresenter<MySemestersView>() {


    fun getUserSemesters(studentId: String) {
        repo.getUserSemesters(studentId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<UsersSemestersGoals>() {
                    override fun onSuccess(p0: com.gmplan.gameplan.data.pojos.UsersSemestersGoals) {

                        if (p0.response == "success") {

                            if (p0.semestergoals != null || p0.semestergoals.isEmpty()) {

                                if (isViewAttached)
                                    view?.onSuccessSemestersList(p0.semestergoals)

                            } else {

                                if (isViewAttached)
                                    view?.onNoSemestersGoalFound()

                            }

                        } else {

                            if (isViewAttached)
                                view?.onFailedGettingSemesters()

                        }


                    }

                    override fun onError(p0: Throwable) {
                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onFailedGettingSemesters()

                    }
                })
    }

    fun deleteSemester(semesterId: String, pos: Int) {
        repo.deleteSemester(semesterId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<JsonElement>() {
                    override fun onError(p0: Throwable) {
                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onFailedDelete()

                    }

                    override fun onSuccess(p0: JsonElement) {

                        val obj = p0.asJsonObject
                        val response = obj.get("response").asString

                        if (response == "success") {

                            if (isViewAttached)
                                view?.deleteSuccessfully(pos)

                        } else {

                            if (isViewAttached)
                                view?.onFailedDelete()

                        }

                    }

                })
    }

    fun addSemester(studendId: String, title: String, grade: String, semester: String, detail: String) {
        repo.addSemesters(studendId, title, grade, semester, detail)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<JsonElement>() {
                    override fun onSuccess(p0: JsonElement) {


                        val obj = p0.asJsonObject

                        val response = obj.get("status").asString

                        if (response == "success") {

                            if (isViewAttached)
                                view?.onSuccessfullySemesterAdded()

                        }


                    }

                    override fun onError(p0: Throwable) {

                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onFailedAddSemester()
                    }

                })
    }


}