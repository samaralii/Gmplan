package com.gmplan.gameplan.features.interestProfile.onetIntro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gmplan.gameplan.R

/**
 * Created by Sammie on 5/4/2017.
 */

class FragmentThree: BaseSliderFragment() {

    override fun onCrtView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater?.inflate(R.layout.frag_three, container, false) as View
    }

}

