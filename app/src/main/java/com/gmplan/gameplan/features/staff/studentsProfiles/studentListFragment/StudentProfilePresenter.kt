package com.gmplan.gameplan.features.staff.studentsProfiles.studentListFragment

import android.app.Dialog
import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.StudentsCareers
import com.gmplan.gameplan.data.pojos.fieldsValue.FieldsValue
import com.gmplan.gameplan.data.pojos.staffStudent.StaffStudent
import com.gmplan.gameplan.data.pojos.studentProfileDetails.StudentProfileDetails
import com.gmplan.gameplan.data.source.TaskDataSource
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sammie on 6/6/2017.
 */
class StudentProfilePresenter(private val repo: TaskDataSource, private val schedulers: RxSchedulers) : MvpBasePresenter<StudentProfileView>() {

    val disposable = CompositeDisposable()

    fun getStaffStudents(staffid: String, collegeId: String) {
        disposable.add(repo.getStaffStudent(staffid, collegeId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<List<StaffStudent>>() {

                    override fun onError(p0: Throwable) {

                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onErrorGettingStudents()

                    }

                    override fun onSuccess(p0: List<StaffStudent>) {


                        if (p0.isNotEmpty()) {

                            if (isViewAttached)
                                view?.onStudentsFound(p0)


                        } else {

                            if (isViewAttached)
                                view?.onNoStudentFound()

                        }

                    }

                }))
    }

    fun getStudentDetailData(staffId: String?, collegeId: String?, studentId: String?) {

        if (staffId == null || collegeId == null || studentId == null) {

            if (isViewAttached)
                view?.dismissDialog()

            if (isViewAttached)
                view?.onErrorGettingStudentDetail()

            return
        }

        disposable.add(repo.getStudentDetail(staffId, collegeId, studentId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<StudentProfileDetails>() {
                    override fun onError(p0: Throwable) {

                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onErrorGettingStudentDetail()

                    }

                    override fun onSuccess(p0: StudentProfileDetails) {

                        if (p0.response == "success") {

                            if (isViewAttached)
                                view?.onSuccessStudentDetail(p0)


                        } else {
                            if (isViewAttached)
                                view?.onErrorGettingStudentDetail()

                        }

                    }
                }))
    }

    fun getStudentFieldsValues(staffId: String) {
        disposable.add(repo.getStudentFieldsValues(staffId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<FieldsValue>() {
                    override fun onSuccess(p0: FieldsValue) {

                        if (p0.response == "success") {

                            onSuccessGettingFieldsValue(p0)

                        } else {

                            if (isViewAttached)
                                view?.onErrorGettingFieldsValue()

                        }

                    }

                    override fun onError(p0: Throwable) {
                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onErrorGettingFieldsValue()
                    }

                }))
    }

    private fun onSuccessGettingFieldsValue(fieldsValue: FieldsValue) {

        val schoolList: MutableList<String> = ArrayList()
        val citiesList: MutableList<String> = ArrayList()
        val countriesList: MutableList<String> = ArrayList()
        val statesList: MutableList<String> = ArrayList()

//        for (v in fieldsValue.schools){
//            schoolList.add(v.name)
//        }

        fieldsValue.schools.mapTo(schoolList) { it.name }
        fieldsValue.cities.mapTo(citiesList) { it.name }
        fieldsValue.counties.mapTo(countriesList) { it.name }
        fieldsValue.states.mapTo(statesList) { it.name }


        schoolList.add(0, "Select School")
        citiesList.add(0, "Select Cities")
        countriesList.add(0, "Select Countries")
        statesList.add(0, "Select States")

        if (isViewAttached)
            view?.onSuccessGettingFieldsValue(schoolList, citiesList, countriesList, statesList)


    }

    fun searchStaffStudent(staffId: String, collegeId: String, name: String, dialog: Dialog) {
        disposable.add(repo.searchStaffStudent(staffId, collegeId, name)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<List<StaffStudent>>() {
                    override fun onSuccess(p0: List<StaffStudent>) {

                        if (p0.isNotEmpty()) {

                            dialog.dismiss()

                            if (isViewAttached)
                                view?.onStudentsFound(p0)


                        } else {

                            if (isViewAttached)
                                view?.onErrorSearchStudent()

                        }

                    }

                    override fun onError(p0: Throwable) {
                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onErrorSearchStudent()
                    }

                }))
    }

    fun getStudentCareerList() {
        disposable.add(repo.getStudentsCareersSearchList()
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<List<StudentsCareers>>() {
                    override fun onError(p0: Throwable) {
                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onErrorGettingCareerList()
                    }

                    override fun onSuccess(p0: List<StudentsCareers>) {

                        if (p0.isNotEmpty()){

                            if (isViewAttached)
                                view?.onSuccessCareerList(p0)

                        } else {

                            if (isViewAttached)
                                view?.onErrorGettingCareerList()

                        }

                    }

                }))
    }

    fun getStaffStudentsByCareer(staffId: String, careerCode: String, dialog: Dialog?) {

//        disposable.add(repo.searchStaffStudentByCareer(staffId, careerCode)
        disposable.add(repo.searchStaffStudentByCareer("46", "39-3091.00")
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<List<StaffStudent>>() {
                    override fun onError(p0: Throwable) {
                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onErrorSearchingByCareer()
                    }

                    override fun onSuccess(p0: List<StaffStudent>) {

                        if (p0.isNotEmpty()) {

                            if (isViewAttached)
                                view?.onStudentsFound(p0)

                            dialog?.dismiss()

                        } else {
                            if (isViewAttached)
                                view?.onNoStudentFound()
                        }

                    }

                }))

    }

    override fun detachView(retainInstance: Boolean) {
        super.detachView(retainInstance)
        if (!retainInstance)
            disposable.clear()
    }

}