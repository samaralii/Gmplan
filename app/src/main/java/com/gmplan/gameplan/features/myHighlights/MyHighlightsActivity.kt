package com.gmplan.gameplan.features.myHighlights

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.widget.Toolbar
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.BaseDrwrActivity
import com.gmplan.gameplan.EmptyPresenter
import com.gmplan.gameplan.EmptyView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.source.TaskRepository
import javax.inject.Inject

/**
 * Created by Sammie on 5/22/2017.
 */
class MyHighlightsActivity : BaseDrwrActivity<EmptyView, EmptyPresenter>() {

    override fun gifPresenter() = EmptyPresenter()

    @BindView(R.id.pager)
    lateinit var vPager: ViewPager

    @BindView(R.id.tab_layout)
    lateinit var tab: TabLayout

    @Inject
    lateinit var repository: TaskRepository

    override fun finishCurrentActivity() {
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_myachiev)
        gifApplication().component?.Inject(this)
        ButterKnife.bind(this)
        val toolbar = findViewById<Toolbar>(R.id.app_toolbar) as Toolbar
        setSupportActionBar(toolbar)
        title = "My Achievements"
        createDrawer(toolbar, repository, true)
        addTabs()
    }

    private fun addTabs() {

        tab.addTab(tab.newTab().setText("Achievements"))
        tab.addTab(tab.newTab().setText("Videos"))

        tab.tabGravity = TabLayout.GRAVITY_FILL


        val adapter = FragmentPagerAdapter(supportFragmentManager, tab.tabCount)
        vPager.adapter = adapter
        vPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tab))

        tab.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                vPager.currentItem = tab?.position as Int
            }

        })


    }
}