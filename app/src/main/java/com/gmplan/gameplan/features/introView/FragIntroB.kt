package com.gmplan.gameplan.features.introView

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.gmplan.gameplan.R

/**
 * Created by Sammie on 7/7/2017.
 */
class FragIntroB : Fragment() {


    var unbinder: Unbinder? = null

    @BindView(R.id.test)
    lateinit var test: View


    companion object {
        fun newInstance(): FragIntroB{
            return FragIntroB()
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.frag_b, container, false) as View
        ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }


    fun playAnimation() {

        YoYo.with(Techniques.DropOut)
                .playOn(test)

    }


}