package com.gmplan.gameplan.features.staff.updateStatus

import android.util.Log
import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.source.TaskDataSource
import com.google.gson.JsonElement
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

/**
 * Created by Sammie on 6/19/2017.
 */
class StatusPresenter(private val repo: TaskDataSource, private val rxSchedulers: RxSchedulers) : MvpBasePresenter<StatusView>() {

    private val disposable = CompositeDisposable()

    fun updateStatus(userId: String, collegeId: String, status: String, imageFile: File) {

        val _collegeId = RequestBody.create(
                MediaType.parse("multipart/form-data"), collegeId)

        val _userId = RequestBody.create(
                MediaType.parse("multipart/form-data"), userId)

        val _status = RequestBody.create(
                MediaType.parse("multipart/form-data"), status)


        Log.d("FILE PATH", imageFile.absolutePath)

        var photoFile: MultipartBody.Part? = null

        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile)

        photoFile = MultipartBody.Part.createFormData("image", imageFile.name, requestFile)

        disposable.add(repo.updateStatus(_collegeId, _userId, _status, photoFile)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<JsonElement>() {
                    override fun onError(p0: Throwable) {
                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onErrorUpdateStatus()
                    }

                    override fun onSuccess(p0: JsonElement) {


                        val obj = p0.asJsonObject
                        val response = obj.get("response").asString

                        if (response == "success") {

                            if (isViewAttached)
                                view?.onSuccessUpdateStatus()

                        } else {

                            if (isViewAttached)
                                view?.onErrorUpdateStatus()

                        }


                    }
                }))

    }

    override fun detachView(retainInstance: Boolean) {
        super.detachView(retainInstance)

        if (!retainInstance)
            disposable.clear()
    }


}