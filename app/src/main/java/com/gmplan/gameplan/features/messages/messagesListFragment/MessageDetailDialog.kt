package com.gmplan.gameplan.features.messages.messagesListFragment

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.detailMessagePojos.Messages

/**
 * Created by Sammie on 5/25/2017.
 */
class MessageDetailDialog(val listener: DialogListener, val message: Messages?) : DialogFragment() {


    var unbinder: Unbinder? = null

    @BindView(R.id.dialog_messages_staffName)
    lateinit var staffName: TextView

    @BindView(R.id.dialog_messages_staffJob)
    lateinit var staffJob: TextView

    @BindView(R.id.dialog_messages_collegeName)
    lateinit var collegeName: TextView

//    @BindView(R.id.dialog_messages_detailText)
//    lateinit var detailText: TextView

    @BindView(R.id.dialog_messages_etReply)
    lateinit var etReply: EditText

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.dialog_messages, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        dialog.setTitle(message?.message?.subject)

        staffName.text = message?.admissionStaff?.name
        staffJob.text = message?.admissionStaff?.jobTitle

        collegeName.text = message?.college?.name

//        detailText.text = message?.message?.body

    }


    @OnClick(R.id.dialog_messages_btnCancel)
    fun onCancelClick() {
        dialog.dismiss()
    }


    @OnClick(R.id.dialog_messages_btnReply)
    fun onReplyClick() {

        if (etReply.text.isEmpty()) {
            etReply.error = "Required Filed"
        } else {
            etReply.error = null
        }


        listener.onReplyClick(dialog, etReply.text.toString(), message)
    }




    interface DialogListener {
        fun onReplyClick(dialog: Dialog, reply: String, message: Messages?)
    }
}