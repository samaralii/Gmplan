package com.gmplan.gameplan.features.staff.studentsProfiles.schoolListFragment

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.AllSchool
import com.gmplan.gameplan.data.pojos.School_
import com.gmplan.gameplan.data.pojos.Student_

class SchoolListAdapter(private var data: MutableList<AllSchool> = arrayListOf(),
                        val onItemClick: (data: School_) -> Unit) : RecyclerView.Adapter<SchoolListVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolListVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_school, parent, false)
        return SchoolListVH(view)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: SchoolListVH, position: Int) {

        val schoolName = data[position].School.name
        val students = data[position].School.Student
        val count = if (students != null && students.isNotEmpty()) students.size else 0

        holder.name?.text = "$schoolName ($count)"

        holder.itemView?.setOnClickListener {
            if (count != 0)
                onItemClick.invoke(data[position].School)
        }

    }

    fun addData(data: MutableList<AllSchool>) {
        this.data.clear()
        this.data = data
        notifyDataSetChanged()
    }


}


class SchoolListVH(v: View) : RecyclerView.ViewHolder(v) {
    val name by lazy { v.findViewById<TextView>(R.id.list_school_name) }
}