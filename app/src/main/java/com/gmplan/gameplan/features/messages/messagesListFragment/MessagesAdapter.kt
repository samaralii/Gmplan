package com.gmplan.gameplan.features.messages.messagesListFragment

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.ShortMessageDetail

/**
 * Created by Sammie on 5/25/2017.
 */
class MessagesAdapter(val data: MutableList<ShortMessageDetail>, val listener: Listener) : RecyclerView.Adapter<MessagesAdapter.MessagesVH>() {

    override fun onBindViewHolder(holder: MessagesVH, position: Int) {
        holder.sender.text = "By ${data[position].sender}"
        holder.subject.text = data[position].subject
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessagesVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_messages, parent, false)
        return MessagesVH(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class MessagesVH(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_messages_sender)
        lateinit var sender: TextView

        @BindView(R.id.list_messages_subject)
        lateinit var subject: TextView

        init {
            ButterKnife.bind(this, v)


            v.setOnClickListener {

                listener.onItemClick(data[layoutPosition].id)

            }
        }

    }

    interface Listener {
        fun onItemClick(id: String)
    }
}