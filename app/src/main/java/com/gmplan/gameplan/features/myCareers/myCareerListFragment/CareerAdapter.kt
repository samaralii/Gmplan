package com.gmplan.gameplan.features.myCareers.myCareerListFragment

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.Career

/**
 * Created by Sammie on 5/18/2017.
 */
class CareerAdapter(val data: MutableList<Career>, val listener: Listener, val context: Context) : RecyclerView.Adapter<CareerAdapter.CareerViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CareerViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_mycareers, parent, false)
        return CareerViewHolder(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: CareerViewHolder, position: Int) {

        holder.careerTitle.text = data[position].careerTitle


        holder.let {

            it.menu.setOnClickListener { _->

                val popup = PopupMenu(context, it.menu)
                popup.inflate(R.menu.options_menu_default)

                popup.setOnMenuItemClickListener {

                    when(it.itemId) {
                        R.id.menu1 ->  listener.onDeleteClick(data[position].id, position)
                    }

                    false
                }

                popup.show()

            }


        }

    }

    fun removeAt(position: Int) {
        data.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, data.size)
    }


    inner class CareerViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_mycareers_careersTitle)
        lateinit var careerTitle: TextView

        @BindView(R.id.list_mycareers_delete)
        lateinit var menu: View

        init {
            ButterKnife.bind(this, v)

            v.setOnClickListener {
                listener.onItemClick(data[layoutPosition].careerCode)
            }
        }

        @OnClick(R.id.list_mycareers_jobs)
        fun onJobsClick() {
            listener.onJobClick(data[layoutPosition].careerTitle)
        }


    }

    interface Listener {
        fun onDeleteClick(career_id: String, pos: Int)
        fun onJobClick(title: String)
        fun onItemClick(code: String)
    }

}