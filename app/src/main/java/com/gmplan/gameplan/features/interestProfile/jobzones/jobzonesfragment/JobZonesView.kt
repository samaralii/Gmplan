package com.gmplan.gameplan.features.interestProfile.jobzones.jobzonesfragment

import com.gmplan.gameplan.data.pojos.Zones
import com.gmplan.gameplan.data.pojos.industryCareersSearch.Career
import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Sammie on 9/12/2017.
 */
interface JobZonesView : MvpView{
    fun onErrorGettingJobZones()
    fun dismissProgress()
    fun onGetAllCareers(careers: ArrayList<Career>)
    fun onGetZones(zones: Zones, average_salary: Double)
}