package com.gmplan.gameplan.features.myCareers.careersJobs

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.careersJobs.Job

class CareersJobsAdapter(val data: List<Job>, val listener: Listener) : RecyclerView.Adapter<CareersJobsAdapter.CareerJobsViewHolder>() {

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(p0: CareerJobsViewHolder, pos: Int) {
        p0.title.text = data[pos].jobtitle
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): CareerJobsViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.list_mycareersjobs, p0, false)
        return CareerJobsViewHolder(v)
    }


    inner class CareerJobsViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_mycareersjobs_careersTitle)
        lateinit var title: TextView


        init {
            ButterKnife.bind(this, v)
        }

        @OnClick(R.id.list_mycareersjobs_detail)
        fun onDetailClick() {
            listener.onClick(data[layoutPosition].url, data[layoutPosition].jobtitle)
        }

    }


    interface Listener {
        fun onClick(url: String, title: String)
    }
}