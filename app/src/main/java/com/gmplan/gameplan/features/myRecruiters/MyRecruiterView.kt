package com.gmplan.gameplan.features.myRecruiters

import com.gmplan.gameplan.data.pojos.Recruiter
import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Sammie on 5/10/2017.
 */
interface MyRecruiterView: MvpView{
    fun onNoRecruiterFound()
    fun onSuccess(recruiters: MutableList<Recruiter>)
    fun onError()
    fun dismissProgress()
}