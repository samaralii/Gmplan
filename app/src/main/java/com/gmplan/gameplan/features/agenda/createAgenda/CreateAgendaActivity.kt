package com.gmplan.gameplan.features.agenda.createAgenda


import android.app.Activity
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.EditText
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.CheckListsEvents
import com.gmplan.gameplan.data.pojos.EventsNotification
import com.gmplan.gameplan.data.pojos.staffStudent.StaffStudent
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.agenda.CreateAgendaJson
import com.gmplan.gameplan.infrastructure.GmplanApplication
import com.gmplan.gameplan.util.AppConst
import com.gmplan.gameplan.util.tmsgError
import com.gmplan.gameplan.util.tmsgSuccess
import com.google.gson.Gson
import com.google.gson.JsonElement
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.frag_create_agenda.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class CreateAgendaActivity : AppCompatActivity(), AddChecklistDialog.AddChecklistListener,
        EditChecklistEventDialog.EditChecklistListener, AddNotificationDialog.AddNotificationListener,
        NotificationEditDialog.AddNotificationEditListener, AdvanceSearchFragment.AdvanceSearchListener {


    @BindView(R.id.frag_createagd_checklists)
    lateinit var list: RecyclerView

    @BindView(R.id.list_notification)
    lateinit var listNotification: RecyclerView

    var adapter: CreateChecklistAdapter? = null
    var NotifiAdapter: CreateNotificationAdapter? = null
    var studentAdapter: SelectStudentsAdapter? = null

    private val createAgendajson = CreateAgendaJson()

    @Inject
    lateinit var repo: TaskRepository

    lateinit var progressDialog: ProgressDialog

    val userData by lazy { repo.getUserData(AppConst.USER_DATA) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.frag_create_agenda)
        (application as GmplanApplication).component?.Inject(this)
        ButterKnife.bind(this)

        progressDialog = ProgressDialog(this)
        progressDialog.setTitle("Loading")

        init()
    }

    private fun init() {

        createAgendajson.color = "#ffff"

        when (userData.userData?.user?.type) {
            "4" -> staff_fields.visibility = View.GONE
            "5" -> {
                staff_fields.visibility = View.VISIBLE


                getStaffStudents(
                        userData.userData?.admissionStaff?.id.toString(),
                        userData.userData?.admissionStaff?.collegeId.toString())

            }
        }







        list.layoutManager = LinearLayoutManager(this)
        listNotification.layoutManager = LinearLayoutManager(this)

        enable_checklist.setOnCheckedChangeListener { _, b ->

            if (b) {
                val d = AddChecklistDialog.newInstance(true)
                d.show(supportFragmentManager, "checklistdialog")
                checklist_layout.visibility = View.VISIBLE
            }


        }

        disable_checklist.setOnCheckedChangeListener { _, b ->

            if (b) {
                adapter = null
                createAgendajson.checklist = null
                checklist_layout.visibility = View.GONE
            }


        }

        enable_notification.setOnCheckedChangeListener { _, b ->

            if (b) {
                val d = AddNotificationDialog.newInstance(true)
                d.show(supportFragmentManager, "notifidialg")
                notificationlist_layout.visibility = View.VISIBLE
            }


        }

        disable_notification.setOnCheckedChangeListener { _, b ->

            if (b) {
                NotifiAdapter = null
                createAgendajson.notificationList = null
                notificationlist_layout.visibility = View.GONE
            }


        }

        enable_studentlist.setOnCheckedChangeListener { _, b ->

            if (b) {
                studentlist_layout.visibility = View.VISIBLE
            }


        }

        disable_studentlist.setOnCheckedChangeListener { _, b ->

            if (b) {
//                studentAdapter = null
                createAgendajson.studentsIdList = null
                studentlist_layout.visibility = View.GONE
            }


        }

        frag_createagd_btnSelectAll.setOnCheckedChangeListener { _, b ->
            studentAdapter?.selectAll(b)
        }


    }

    fun getStaffStudents(staffid: String, collegeId: String) {

        progressDialog.show()

        repo.getStaffStudent(staffid, collegeId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally {

                    progressDialog.dismiss()
                }
                .subscribeWith(object : DisposableSingleObserver<List<StaffStudent>>() {

                    override fun onError(p0: Throwable) {

                        p0.printStackTrace()

                    }

                    override fun onSuccess(p0: List<StaffStudent>) {


                        if (p0.isNotEmpty()) {


                            studentAdapter = SelectStudentsAdapter(p0, updateList = { list ->

                                createAgendajson.studentsIdList = list

                            })


                            studentlist.layoutManager = LinearLayoutManager(this@CreateAgendaActivity)
                            studentlist.adapter = studentAdapter


                        } else {


                        }

                    }

                })
    }

    @OnClick(R.id.frag_createagd_btnSearch)
    fun onSearchClick() {

        val d = AdvanceSearchFragment.newInstance(
                userData.userData?.admissionStaff?.id.toString(),
                userData.userData?.admissionStaff?.collegeId.toString())
        d.show(supportFragmentManager, "search dialog")

    }

    override fun onSearchClick(students: HashMap<String, String>) {

        progressDialog.show()

        repo.searchStudents(students)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally {

                    progressDialog.dismiss()
                }
                .subscribeWith(object : DisposableSingleObserver<List<StaffStudent>>() {

                    override fun onError(p0: Throwable) {

                        p0.printStackTrace()

                    }

                    override fun onSuccess(p0: List<StaffStudent>) {


                        if (p0.isNotEmpty()) {

                            studentAdapter = SelectStudentsAdapter(p0, updateList = { list ->

                                createAgendajson.studentsIdList = list

                            })


                            studentlist.layoutManager = LinearLayoutManager(this@CreateAgendaActivity)
                            studentlist.adapter = studentAdapter


                        } else {


                        }

                    }

                })


    }


    @OnClick(R.id.frag_createagd_btnCreate)
    fun onCreateAgenda() {


        createAgendajson.eventStartDateTime = "${frag_createagd_etStartDate.text} ${frag_createagd_etStartTime.text}"
        createAgendajson.eventEndDateTime = "${frag_createagd_etEndDate.text} ${frag_createagd_etEndTime.text}"
        createAgendajson.eventName = frag_createagd_etEvent.text.toString()
        createAgendajson.student_id = userData.userData?.student?.id

        val gson = Gson()
        val json = gson.toJson(createAgendajson)
        Log.d("JSON--->", json)


        when (userData.userData?.user?.type) {
            "4" -> createStudentEvent(json)
            "5" -> createStaffEvent(json, userData.userData?.admissionStaff?.id.toString())
        }


    }

    fun createStaffEvent(json: String, staffid: String) {

        progressDialog.show()
        repo.createStaffAgendaEvent(json, staffid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { progressDialog.dismiss() }
                .subscribeWith(object : DisposableSingleObserver<JsonElement>() {
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        tmsgError("Error")
                    }

                    override fun onSuccess(t: JsonElement) {

                        try {

                            val jsonObj = t.asJsonObject
                            val response = jsonObj.get("response").asString

                            if (response == "success") {
                                tmsgSuccess("New Event Created")
                                setResult(Activity.RESULT_OK)
                                finish()
                            } else {
                                tmsgError("Error")
                            }


                        } catch (e: Exception) {
                            e.printStackTrace()
                            tmsgError("Error")
                        }
                    }
                })

    }

    fun createStudentEvent(json: String) {
        progressDialog.show()
        repo.createAgendaEvent(json)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { progressDialog.dismiss() }
                .subscribeWith(object : DisposableSingleObserver<JsonElement>() {
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        tmsgError("Error")
                    }

                    override fun onSuccess(t: JsonElement) {

                        try {

                            val jsonObj = t.asJsonObject
                            val response = jsonObj.get("response").asString

                            if (response == "success") {
                                tmsgSuccess("New Event Created")
                                setResult(Activity.RESULT_OK)
                                finish()
                            } else {
                                tmsgError("Error")
                            }


                        } catch (e: Exception) {
                            e.printStackTrace()
                            tmsgError("Error")
                        }
                    }
                })
    }

    @OnClick(R.id.frag_createagd_etStartDate)
    fun onStartDateClick(et: EditText) {

        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)

        val startdate = DatePickerDialog(this,
                { datePicker, year, month, day ->
                    val builder = StringBuilder()
                    builder.append(year).append("-")
                    builder.append(day.toString()).append("-")
                    builder.append((month + 1).toString())

                    et.setText(builder.toString())
                },
                mYear,
                mMonth,
                mDay)
        startdate.show()

    }

    @OnClick(R.id.frag_createagd_etStartTime)
    fun onStartTimeClick(et: EditText) {

        val timePicker = TimePickerDialog(this, { _, v1, v2 ->

            val time = "$v1:$v2"

            try {
                val sdf = SimpleDateFormat("H:mm")
                val dateObj = sdf.parse(time)
                et.setText(SimpleDateFormat("K:mm a").format(dateObj))

            } catch (e: ParseException) {
                e.printStackTrace()
            }


        }, 0, 0, false)

        timePicker.show()

    }

    @OnClick(R.id.frag_createagd_etEndDate)
    fun onEndDateClick(et: EditText) {

        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)

        val startdate = DatePickerDialog(this,
                { datePicker, year, month, day ->
                    val builder = StringBuilder()
                    builder.append(year).append("-")
                    builder.append(day.toString()).append("-")
                    builder.append((month + 1).toString())

                    et.setText(builder.toString())
                },
                mYear,
                mMonth,
                mDay)
        startdate.show()

    }

    @OnClick(R.id.frag_createagd_etEndTime)
    fun onEndTimeClick(et: EditText) {

        val timePicker = TimePickerDialog(this, { _, v1, v2 ->

            val time = "$v1:$v2"

            try {
                val sdf = SimpleDateFormat("H:mm")
                val dateObj = sdf.parse(time)
                et.setText(SimpleDateFormat("K:mm a").format(dateObj))

            } catch (e: ParseException) {
                e.printStackTrace()
            }


        }, 0, 0, false)

        timePicker.show()

    }


    @OnClick(R.id.activity_crtagenda_tvAddAnotherStep)
    fun onAddAnotherStepClick() {
        val d = AddChecklistDialog.newInstance()
        d.show(supportFragmentManager, "checklistdialog")
    }


    @OnClick(R.id.activity_crtagenda_tvAddAnotherNotification)
    fun onAddNotificationClick() {
        val d = AddNotificationDialog.newInstance()
        d.show(supportFragmentManager, "a")
    }


    @OnClick(R.id.color1, R.id.color2, R.id.color3, R.id.color4, R.id.color5, R.id.color6)
    fun onColorClick(v: View) {

        val id = v.id

        when (id) {
            R.id.color1 -> selectColor("#D50000", v)
            R.id.color2 -> selectColor("#304FFE", v)
            R.id.color3 -> selectColor("#00B8D4", v)
            R.id.color4 -> selectColor("#00B8D4", v)
            R.id.color5 -> selectColor("#00B8D4", v)
            R.id.color6 -> selectColor("#c9c9c9", v)
        }

    }

    private fun selectColor(colorCode: String, v: View) {

        YoYo.with(Techniques.Bounce)
                .playOn(v)

        createAgendajson.color = colorCode


    }


    override fun addEvent(event: CheckListsEvents) {
        Log.d("DATA", event.toString())
        adapter?.addItem(event)
    }

    override fun addFirstEvent(event: CheckListsEvents) {


        adapter = CreateChecklistAdapter(mutableListOf(event), onEditClick = { event, pos ->

            val d = EditChecklistEventDialog.newInstance(event, pos)
            d.show(supportFragmentManager, "editlistdialog")

        })

        list.adapter = adapter
        createAgendajson.checklist = adapter?.getList()
    }

    override fun editEvent(event: CheckListsEvents, pos: Int) {
        adapter?.editItem(event, pos)
        createAgendajson.checklist = adapter?.getList()
    }


    override fun onCancelClick() {
        enable_checklist.isChecked = false
        disable_checklist.isChecked = true
    }

    override fun addNotification(event: EventsNotification) {

        NotifiAdapter?.addItem(event)
        createAgendajson.notificationList = NotifiAdapter?.getList()

    }

    override fun addFirstNotification(event: EventsNotification) {

        NotifiAdapter = CreateNotificationAdapter(mutableListOf(event), onEditClick = {

            event, pos ->

            val d = NotificationEditDialog.newInstance(event, pos)
            d.show(supportFragmentManager, "d")

        })

        listNotification.adapter = NotifiAdapter
        createAgendajson.notificationList = NotifiAdapter?.getList()


    }

    override fun onCancelNClick() {
        enable_notification.isChecked = false
        disable_notification.isChecked = true
    }

    override fun onEditNotification(event: EventsNotification, pos: Int) {
        NotifiAdapter?.editItem(event, pos)
    }


}
