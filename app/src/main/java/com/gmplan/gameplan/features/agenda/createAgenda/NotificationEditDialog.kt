package com.gmplan.gameplan.features.agenda.createAgenda

import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Spinner
import butterknife.*
import com.gmplan.gameplan.NewDefaultSpinnerAdapter
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.EventsNotification
import com.gmplan.gameplan.util.tmsgError
import kotlinx.android.synthetic.main.dialog_addnotification.*

/**
 * Created by Sammie on 9/29/2017.
 */
class NotificationEditDialog : DialogFragment() {

    var listener: AddNotificationEditListener? = null
    var unbinder: Unbinder? = null

    @BindView(R.id.duration)
    lateinit var durationSpn: Spinner


    var event: EventsNotification? = null
    var pos: Int? = null


    companion object {


        private val DATA = "data"
        private val POS = "pos"

        fun newInstance(event: EventsNotification, pos: Int): NotificationEditDialog {
            val d = NotificationEditDialog()
            val b = Bundle()
            b.putParcelable(DATA, event)
            b.putInt(POS, pos)
            d.arguments = b
            return d
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.dialog_editnotification, container, false)
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        event = arguments!!.getParcelable(DATA)
        pos = arguments!!.getInt(POS)
    }

    val duratioList = listOf("Minutes", "Hours", "Day", "Week")

    var duration = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dialog_etNotification.setText(event?.text)

        durationSpn.adapter = NewDefaultSpinnerAdapter(duratioList)
    }


    @OnItemSelected(R.id.duration)
    fun onDurationsSelected(pos: Int) {
        duration = duratioList[pos]
    }


    @OnClick(R.id.dialog_btnAdd)
    fun onAddClick() {

        var validate = true

        if (dialog_etNotification.text.isNullOrEmpty())
            validate = false



        if (!validate)
            context?.tmsgError("Fill All Fields")


        pos?.let { listener?.onEditNotification(EventsNotification(dialog_etNotification.text.toString(), duration), it) }

        dialog.dismiss()

    }


    @OnClick(R.id.dialog_btnCancel)
    fun onCancelClick() {
        dialog.dismiss()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            listener = context as AddNotificationEditListener
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    public interface AddNotificationEditListener {
        fun onEditNotification(event: EventsNotification, pos: Int)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

}