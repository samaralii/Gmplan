package com.gmplan.gameplan.features.profileVIews

import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.data.pojos.ProfileViewsPojo
import com.gmplan.gameplan.data.source.TaskDataSource
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sammie on 5/3/2017.
 */
class ProfileViewPresenter(val repo: TaskDataSource, val rxSchedulers: RxSchedulersImpl) : MvpBasePresenter<ProfileViewsView>() {


    val disposable: CompositeDisposable = CompositeDisposable()

    fun getProfileViews(studentId: String) {

        disposable.add(repo.userProfileViews(studentId)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissProgress()


                }
                .subscribeWith(object : DisposableSingleObserver<ProfileViewsPojo>() {

                    override fun onSuccess(data: ProfileViewsPojo) {

                        if (data.response == "success" && data.profileViews != null && data.profileViews.size > 0) {
                            if (isViewAttached)
                                view?.onSuccess(data.profileViews)
                        } else {
                            if (isViewAttached)
                                view?.onNoProfileViews()
                        }

                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        if (isViewAttached)
                            view?.onErrorProfileViews()

                    }

                }))


    }

    override fun detachView(retainInstance: Boolean) {
        super.detachView(retainInstance)
        if (!retainInstance)
            disposable.clear()

    }

}

