package com.gmplan.gameplan.features.agenda.createAgenda


import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.CheckListsEvents
import com.gmplan.gameplan.util.tmsgError
import kotlinx.android.synthetic.main.dialog_addchecklist.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class AddChecklistDialog: DialogFragment() {


    var listener: AddChecklistListener? = null
    var unbinder: Unbinder? = null
    var isFirstItem = false

    companion object {

        private val BOOL = "bool"

        fun newInstance(isFirstItem: Boolean = false): AddChecklistDialog {
            val d = AddChecklistDialog()
            val b = Bundle()
            b.putBoolean(BOOL, isFirstItem)
            d.arguments = b
            return d
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.dialog_addchecklist, container, false)
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isFirstItem = arguments!!.getBoolean(BOOL, false)
    }


    @OnClick(R.id.dialog_btnAdd)
    fun onAddClick() {

        var validate = true

        if (dialog_etEvent.text.isNullOrEmpty())
            validate = false

        if (dialog_etStartDate.text.isNullOrEmpty())
            validate = false

        if (dialog_etStartTime.text.isNullOrEmpty())
            validate = false

        if (dialog_etEndDate.text.isNullOrEmpty())
            validate = false

        if (dialog_etEndTime.text.isNullOrEmpty())
            validate = false


        if (!validate)
            context?.tmsgError("Fill All Fields")


        if (isFirstItem) {

            listener?.addFirstEvent(CheckListsEvents(
                    dialog_etEvent.text.toString(),
                    "${dialog_etStartDate.text} ${dialog_etStartTime.text}",
                    "${dialog_etEndDate.text} ${dialog_etEndTime.text}"
            ))


        } else {

            listener?.addEvent(CheckListsEvents(
                    dialog_etEvent.text.toString(),
                    "${dialog_etStartDate.text} ${dialog_etStartTime.text}",
                    "${dialog_etEndDate.text} ${dialog_etEndTime.text}"
            ))


        }

        dialog.dismiss()

    }


    @OnClick(R.id.dialog_etStartDate)
    fun onStartDateClick(et: EditText) {

        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)

        val startdate = DatePickerDialog(context,
                { _, year, month, day ->
                    val builder = StringBuilder()
                    builder.append(year).append("-")
                    builder.append(day.toString()).append("-")
                    builder.append((month + 1).toString())

                    et.setText(builder.toString())
                },
                mYear,
                mMonth,
                mDay)
        startdate.show()

    }

    @OnClick(R.id.dialog_etStartTime)
    fun onStartTimeClick(et: EditText) {

        val timePicker = TimePickerDialog(context, { _, v1, v2 ->

            val time = "$v1:$v2"

            try {
                val sdf = SimpleDateFormat("H:mm")
                val dateObj = sdf.parse(time)
                et.setText(SimpleDateFormat("K:mm a").format(dateObj))

            } catch (e: ParseException) {
                e.printStackTrace()
            }


        }, 0, 0, false)

        timePicker.show()

    }

    @OnClick(R.id.dialog_etEndDate)
    fun onEndDateClick(et: EditText) {

        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)

        val startdate = DatePickerDialog(context,
                { _, year, month, day ->
                    val builder = StringBuilder()
                    builder.append(year).append("-")
                    builder.append(day.toString()).append("-")
                    builder.append((month + 1).toString())

                    et.setText(builder.toString())
                },
                mYear,
                mMonth,
                mDay)
        startdate.show()

    }

    @OnClick(R.id.dialog_etEndTime)
    fun onEndTimeClick(et: EditText) {

        val timePicker = TimePickerDialog(context, { _, v1, v2 ->

            val time = "$v1:$v2"

            try {
                val sdf = SimpleDateFormat("H:mm")
                val dateObj = sdf.parse(time)
                et.setText(SimpleDateFormat("K:mm a").format(dateObj))

            } catch (e: ParseException) {
                e.printStackTrace()
            }


        }, 0, 0, false)

        timePicker.show()

    }


    @OnClick(R.id.dialog_btnCancel)
    fun onCancelClick() {
        dialog.dismiss()
        listener?.onCancelClick()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            listener = context as AddChecklistListener
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    public interface AddChecklistListener {
        fun addEvent(event: CheckListsEvents)
        fun addFirstEvent(event: CheckListsEvents)
        fun onCancelClick()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

}
