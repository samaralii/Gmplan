package com.gmplan.gameplan.features.interestProfile.questionsView

import com.gmplan.gameplan.data.pojos.Result
import com.gmplan.gameplan.data.pojos.interestProfileQuestions.Question
import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Sammie on 8/28/2017.
 */
interface QuestionsView: MvpView {
    fun dismissProgress()
    fun onErrorGettingQuestion()
    fun showNextQuestions(arrayList: ArrayList<Question>, index: Int)
    fun showPreviousQuestions(arrayList: ArrayList<Question>, index: Int)
    fun showInitialQuestions(arrayList: ArrayList<Question>, index: Int)
    fun enableProceedBtn()
    fun disableProceedBtn()
    fun onErrorGettingResult()
    fun onSuccessResults(result: ArrayList<Result>)
}