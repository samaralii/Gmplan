package com.gmplan.gameplan.features.interestProfile.questionsView

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.interestProfileQuestions.Question
import com.gmplan.gameplan.features.interestProfile.QuestionsAdapter

/**
 * Created by Sammie on 8/28/2017.
 */
class QuestionFragment : Fragment(), QuestionsAdapter.Listener {


    @BindView(R.id.list)
    lateinit var list: RecyclerView


    var questionList: ArrayList<Question>? = null

    var listener: QuestionFragmentListener? = null

    var index: Int? = null


    override fun onAttach(context: Context?) {
        super.onAttach(context)

        try {
            listener = context as QuestionFragmentListener
        } catch (ex: Exception) {
            throw Exception("Must Implement the fucking listener")
        }
    }

    companion object {

        val QUESTIONS_LIST = "list"
        val INDEX = "index"

        fun getInstance(questions: ArrayList<Question>, i: Int): QuestionFragment {
            val fragment = QuestionFragment()
            val b = Bundle()
            b.putParcelableArrayList(QUESTIONS_LIST, questions)
            b.putInt(INDEX, i)
            fragment.arguments = b
            return fragment
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_questions, container, false) as View
        ButterKnife.bind(this, v)
        return v
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        questionList = arguments!!.getParcelableArrayList<Question>(QUESTIONS_LIST)
        index = arguments!!.getInt(INDEX)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        list.layoutManager = LinearLayoutManager(context)
        list.adapter = questionList?.let { QuestionsAdapter(it, this) }

    }

    override fun onDestroy() {
        super.onDestroy()
//        listener?.onDestroyFragment(questionList, index)
    }

    interface QuestionFragmentListener {
        fun onDestroyFragment(list: ArrayList<Question>?, index: Int?)
        fun onUpdateList(list: ArrayList<Question>?, index: Int?)
    }

    override fun onClick(data: ArrayList<Question>) {
        listener?.onUpdateList(data, index)
    }

}