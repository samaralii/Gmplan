package com.gmplan.gameplan.features.myHighlights.myVideos

import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.gmplan.gameplan.R

/**
 * Created by Sammie on 5/23/2017.
 */
class AddVideoDialog(val listener: DialogListener,
                     val isEdit: Boolean = false,
                     val url: String = "url",
                     val name: String = "name",
                     val id: String = "id"
                     ) : DialogFragment() {

    var unbinder: Unbinder? = null

    @BindView(R.id.dialog_add_video_etName)
    lateinit var etName: EditText

    @BindView(R.id.dialog_add_video_etUrl)
    lateinit var etUrl: EditText

    @BindView(R.id.dialog_add_video_btnEdit)
    lateinit var btnEdit: Button

    @BindView(R.id.dialog_add_video_btnAdd)
    lateinit var btnAdd: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.dialog_add_video, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        if (isEdit) {
            dialog.setTitle("Edit Video")

            btnAdd.visibility = View.GONE
            btnEdit.visibility = View.VISIBLE

            etUrl.setText(url)
            etName.setText(name)

        } else {
            dialog.setTitle("Add Video")

            btnAdd.visibility = View.VISIBLE
            btnEdit.visibility = View.GONE

            etUrl.setText("")
            etName.setText("")
        }

    }

    @OnClick(R.id.dialog_add_video_btnAdd)
    fun onClick() {

        if (etName.text.isEmpty()) {
            etName.error = "Required Field"
            return
        } else {
            etName.error = null
        }


        if (etUrl.text.isEmpty()) {
            etUrl.error = "Required Field"
            return
        } else {
            etUrl.error = null
        }


        listener.onAddVideo(dialog, etName.text.toString(), etUrl.text.toString())
    }


    @OnClick(R.id.dialog_add_video_btnEdit)
    fun onEditClick() {

        if (etName.text.isEmpty()) {
            etName.error = "Required Field"
            return
        } else {
            etName.error = null
        }


        if (etUrl.text.isEmpty()) {
            etUrl.error = "Required Field"
            return
        } else {
            etUrl.error = null
        }


        listener.onEditVideo(dialog, etName.text.toString(), etUrl.text.toString(), id)
    }




    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()

    }


    interface DialogListener {
        fun onAddVideo(dialog: DialogInterface, name: String, url: String)
        fun onEditVideo(dialog: DialogInterface, name: String, url: String, id: String)
    }

}