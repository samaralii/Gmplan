package com.gmplan.gameplan.features.myCareers.myCareerListFragment

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.UserCareers
import com.gmplan.gameplan.data.source.TaskDataSource
import com.google.gson.JsonElement
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sammie on 5/18/2017.
 */
class MyCareerPresenter(val repo: TaskDataSource, val schedulers: RxSchedulers) : MvpBasePresenter<MyCareerView>() {


    fun getCareersList(studentId: String) {

        repo.getUserCareers(studentId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }

                .subscribeWith(object : DisposableSingleObserver<UserCareers>() {
                    override fun onSuccess(userCareers: UserCareers) {

                        if (userCareers.response == "success") {

                            if (userCareers.careers == null || userCareers.careers.isEmpty()) {


                                if (isViewAttached)
                                    view?.onNoCareerFound()

                            } else {

                                if (isViewAttached)
                                    view?.careerList(userCareers.careers)

                            }

                        } else {

                            if (isViewAttached)
                                view?.onFailed()

                        }

                    }

                    override fun onError(p0: Throwable) {

                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onFailed()
                    }

                })
    }

    fun deleteCareer(career_id: String, pos: Int) {
        repo.deleteCareer(career_id)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<JsonElement>() {
                    override fun onSuccess(json: JsonElement) {

                        val j = json.asJsonObject
                        val response = j?.get("response")?.asString

                        if (response == "success") {

                            val s = json.asJsonObject
                            val status = s.get("status").asString

                            if (status == "deleted") {

                                if (isViewAttached)
                                    view?.careerDeleted(pos)

                            } else {

                                if (isViewAttached)
                                    view?.onFailedDeleteCareer()

                            }


                        } else {

                            if (isViewAttached)
                                view?.onFailedDeleteCareer()

                        }



                    }

                    override fun onError(p0: Throwable) {

                        if (isViewAttached)
                            view?.onFailedDeleteCareer()

                    }

                })
    }

}