package com.gmplan.gameplan.features.myCareers.careersJobs

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.careersJobs.CareersJobsPojo
import com.gmplan.gameplan.data.source.TaskDataSource
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sammie on 8/24/2017.
 */
class CareersJobsPresenter(val repo: TaskDataSource, val schedulers: RxSchedulers) : MvpBasePresenter<CareersjobsView>() {


    fun getCareersJobsList(studentSchoolId: String, careerTitle: String) {
        repo.getCareersJobs(studentSchoolId, careerTitle)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {
                    if (isViewAttached) view?.dismissDialog()
                }
                .subscribeWith(object : DisposableSingleObserver<CareersJobsPojo>() {
                    override fun onError(p0: Throwable) {
                        p0.printStackTrace()

                        if (isViewAttached) view?.onErrorGettingJobs()
                    }

                    override fun onSuccess(c: CareersJobsPojo) {

                        if (c.jobs.isNotEmpty()) {

                            if (isViewAttached) view?.jobsList(c.jobs)

                        } else {

                            if (isViewAttached) view?.onNoJobsFound()

                        }

                    }
                })
    }
}