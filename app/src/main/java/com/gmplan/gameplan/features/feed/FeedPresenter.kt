package com.gmplan.gameplan.features.feed

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.staffFeed.StaffNewssFeed
import com.gmplan.gameplan.data.source.TaskDataSource
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sammie on 5/15/2017.
 */
class FeedPresenter(private val repo: TaskDataSource, private val schedulers: RxSchedulers) : MvpBasePresenter<FeedView>() {


    fun getStaffFeed() {

        repo.getStaffNewsFeed()
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<List<StaffNewssFeed>>() {
                    override fun onSuccess(p0: List<StaffNewssFeed>) {


                        if (p0.isNotEmpty()) {

                            if (isViewAttached)
                                view?.onSuccessFeed(p0)

                        } else {

                            if (isViewAttached)
                                view?.onNoFeedFound()

                        }
                    }

                    override fun onError(p0: Throwable) {
                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onErrorGettingFeed()

                    }

                })


    }


}