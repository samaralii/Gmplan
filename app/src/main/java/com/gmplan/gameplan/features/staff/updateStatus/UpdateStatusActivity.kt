package com.gmplan.gameplan.features.staff.updateStatus

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.gmplan.gameplan.BaseDrwrActivity
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.infrastructure.GmplanApplication
import com.gmplan.gameplan.service.UpdateStatus
import com.gmplan.gameplan.util.AppConst
import com.gmplan.gameplan.util.Utilz
import com.tbruyelle.rxpermissions2.RxPermissions
import pl.aprilapps.easyphotopicker.EasyImage
import java.io.File
import javax.inject.Inject

class UpdateStatusActivity : BaseDrwrActivity<StatusView, StatusPresenter>(), StatusView {


    var pd: ProgressDialog? = null

    @Inject
    lateinit var rxSchedulers: RxSchedulersImpl
    @Inject
    lateinit var taskRepository: TaskRepository


    @BindView(R.id.activity_update_status_chooserStatus)
    lateinit var chooserStatus: TextView

    @BindView(R.id.activity_update_status_etStatus)
    lateinit var etStatus: EditText

    override fun gifPresenter(): StatusPresenter {
        (application as GmplanApplication).component?.Inject(this)
        return StatusPresenter(taskRepository, rxSchedulers)

    }

    override fun finishCurrentActivity() {
        finish()
    }

    @BindView(R.id.app_toolbar)
    lateinit var toolbar: Toolbar


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_status)
        ButterKnife.bind(this)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Update Status"
        createDrawer(toolbar, taskRepository, true, 10)
        init()
    }

    private fun init() {
        pd = Utilz.dialog(this)
    }

    @OnClick(R.id.activity_update_status_chooseFileBtn)
    fun onChooseFileBtnPress() {


        val rxPermission = RxPermissions(this)

        rxPermission
                .request(android.Manifest.permission.CAMERA)
                .subscribe({ granted ->
                    if (granted) EasyImage.openChooserWithGallery(this, "Choose", 1)
                })


    }


    @OnClick(R.id.activity_update_status_btnSend)
    fun onBtnSendPress() {

        if (etStatus.text.isEmpty()) {
            etStatus.error = "Required Field"
            return
        } else
            etStatus.error = null


//        imageFile?.let {
//            pd?.show()
//            presenter.updateStatus(getUserData?.user?.id.toString(), getUserData?.admissionStaff?.collegeId.toString(),
//                    etStatus.text.toString(), it)
//        }

        val serviceIntent = Intent(this, UpdateStatus::class.java)
        serviceIntent.putExtra(AppConst.COLLEGE_ID, getUserData?.admissionStaff?.collegeId.toString())
        serviceIntent.putExtra(AppConst.USER_ID, getUserData?.user?.id.toString())
        serviceIntent.putExtra(AppConst.STATUS, etStatus.text.toString())
        serviceIntent.putExtra(AppConst.IMAGE_PATH, imageFilePath)

        startService(serviceIntent)

        etStatus.setText("")
        imageFilePath = ""
        chooserStatus.text = ""


    }

    override fun onErrorUpdateStatus() {
        Utilz.tmsgError(this, "Error, try again")
    }

    override fun dismissDialog() {
        pd?.dismiss()
    }

    override fun onSuccessUpdateStatus() {
        Utilz.tmsgSuccess(this, "Done")

        finish()
        startActivity(intent)
    }


    private var imageFile: File? = null
    private var imageFilePath = ""

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        EasyImage.handleActivityResult(requestCode, resultCode, data, this, object : EasyImage.Callbacks {
            override fun onImagePicked(p0: File?, p1: EasyImage.ImageSource?, p2: Int) {

                imageFile = p0
                Log.d("FILEPATH", p0?.absoluteFile.toString())
                imageFilePath = p0?.absolutePath.toString()
                chooserStatus.text = imageFile?.name


            }

            override fun onImagePickerError(p0: java.lang.Exception?, p1: EasyImage.ImageSource?, p2: Int) {

                p0?.printStackTrace()

            }

            override fun onCanceled(p0: EasyImage.ImageSource?, p1: Int) {


            }

        })

    }


}