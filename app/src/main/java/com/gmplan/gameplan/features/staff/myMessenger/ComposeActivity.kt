package com.gmplan.gameplan.features.staff.myMessenger

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.gmplan.gameplan.R

class ComposeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_compose)
    }
}
