package com.gmplan.gameplan.features.myCareers.careersJobs

import com.gmplan.gameplan.data.pojos.careersJobs.Job
import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Sammie on 8/24/2017.
 */
interface CareersjobsView: MvpView {
    fun dismissDialog()
    fun onErrorGettingJobs()
    fun onNoJobsFound()
    fun jobsList(jobs: List<Job>)
}