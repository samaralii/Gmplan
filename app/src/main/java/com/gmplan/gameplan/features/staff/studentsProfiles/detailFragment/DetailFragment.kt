package com.gmplan.gameplan.features.staff.studentsProfiles.detailFragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.studentProfileDetails.StudentProfileDetails
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.staff.studentsProfiles.StudentProfileActivity
import com.gmplan.gameplan.util.AppConst
import com.hannesdorfmann.mosby.mvp.MvpFragment
import javax.inject.Inject

class DetailFragment : MvpFragment<DetailView, DetailPresenter>(), DetailView {

    @JvmField
    @BindView(R.id.frag_details_aboutUser_title)
    var title: TextView? = null

    @JvmField
    @BindView(R.id.frag_details_aboutUser_grade)
    var grade: TextView? = null

    @JvmField
    @BindView(R.id.frag_details_aboutUser_ugpa)
    var ugpa: TextView? = null

    @JvmField
    @BindView(R.id.frag_details_aboutUser_wgpa)
    var wgpa: TextView? = null

    @JvmField
    @BindView(R.id.frag_details_aboutUser_classRank)
    var classRank: TextView? = null

    @JvmField
    @BindView(R.id.frag_details_aboutUser_satScore)
    var satScore: TextView? = null

    @JvmField
    @BindView(R.id.frag_details_aboutUser_actScore)
    var actScore: TextView? = null

    @JvmField
    @BindView(R.id.frag_details_aboutUser_school)
    var school: TextView? = null

    @JvmField
    @BindView(R.id.frag_details_aboutUser_likes)
    var likes: TextView? = null

    @JvmField
    @BindView(R.id.frag_details_aboutUser_dislikes)
    var dislikes: TextView? = null

    @JvmField
    @BindView(R.id.frag_details_aboutUser_music)
    var music: TextView? = null

    @JvmField
    @BindView(R.id.frag_details_aboutUser_teams)
    var teams: TextView? = null

    @JvmField
    @BindView(R.id.frag_details_aboutUser_colleges)
    var colleges: TextView? = null

    @JvmField
    @BindView(R.id.frag_detail_videos_videosList)
    var videLists: RecyclerView? = null

    @JvmField
    @BindView(R.id.frag_details_achievements_listAchievements)
    var achievementsList: RecyclerView? = null

    @JvmField
    @BindView(R.id.frag_details_favcollege_list)
    var favCollegeList: RecyclerView? = null

    @JvmField
    @BindView(R.id.frag_details_semestergoal_list)
    var semestersGoalList: RecyclerView? = null

    @JvmField
    @BindView(R.id.frag_details_career_list)
    var careerList: RecyclerView? = null

    @JvmField
    @BindView(R.id.frag_details_interestprofile_list)
    var interestProfileList: RecyclerView? = null

    @Inject
    lateinit var taskRepository: TaskRepository

    @Inject
    lateinit var schedulers: RxSchedulersImpl

    var cat: Int? = null

    var unbinder: Unbinder? = null

    var data: StudentProfileDetails? = null

    override fun createPresenter(): DetailPresenter {
        (activity as StudentProfileActivity).gifApplication().component?.Inject(this)
        return DetailPresenter(taskRepository, schedulers)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        cat = arguments!!.getInt(AppConst.DATA)
        data = arguments!!.getParcelable(AppConst.DATA_1)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return when (cat) {
            0 -> inflater.inflate(R.layout.frag_detail_aboutuser, container, false)
            1 -> inflater.inflate(R.layout.frag_detail_videos, container, false)
            2 -> inflater.inflate(R.layout.frag_detail_achievements, container, false)
            3 -> inflater.inflate(R.layout.frag_detail_favcollege, container, false)
            4 -> inflater.inflate(R.layout.frag_detail_semestergoal, container, false)
            5 -> inflater.inflate(R.layout.frag_detail_career, container, false)
            6 -> inflater.inflate(R.layout.frag_detail_interestprofile, container, false)
            else -> inflater.inflate(R.layout.frag_detail_default, container, false)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        unbinder = ButterKnife.bind(this, view as View)

        when (cat) {
            0 -> initAboutUser()
            1 -> initVideos()
            2 -> initAchievements()
            3 -> initFavCollege()
            4 -> initSemesterGoal()
            5 -> initCareer()
            6 -> initInterestProfile()
            else -> initDefault()
        }
    }

    private fun initDefault() {

    }

    private fun initInterestProfile() {

        interestProfileList?.layoutManager = LinearLayoutManager(context)
        interestProfileList?.setHasFixedSize(true)
        interestProfileList?.adapter = data?.rating?.let { DetailInterestAdapter(it) }


    }

    private fun initCareer() {

        careerList?.layoutManager = LinearLayoutManager(context)
        careerList?.setHasFixedSize(true)
        careerList?.adapter = data?.studentinfo?.career?.let { DetailCareerAdapter(it) }

    }

    private fun initSemesterGoal() {

        semestersGoalList?.layoutManager = LinearLayoutManager(context)
        semestersGoalList?.setHasFixedSize(true)
        semestersGoalList?.adapter = data?.studentinfo?.semesterGoal?.let { DetailSemesterGoalAdapter(it) }


    }

    private fun initFavCollege() {

        favCollegeList?.layoutManager = LinearLayoutManager(context)
        favCollegeList?.setHasFixedSize(true)
        favCollegeList?.adapter = data?.studentcolleges?.let { DetailFavoriteCollegeAdapter(it) }


    }

    private fun initAchievements() {

        achievementsList?.layoutManager = LinearLayoutManager(context)
        achievementsList?.setHasFixedSize(true)
        achievementsList?.adapter = data?.studentinfo?.achievement?.let { DetailAchievAdapter(it) }

    }

    private fun initVideos() {

        videLists?.layoutManager = LinearLayoutManager(context)
        videLists?.setHasFixedSize(true)
        videLists?.adapter = data?.studentinfo?.video?.let { DetailVideoAdapter(it, context!!) }

    }

    private fun initAboutUser() {
        title?.text = "About ${data?.studentinfo?.student?.firstName} ${data?.studentinfo?.student?.lastName}"
        grade?.text = data?.studentinfo?.student?.grade
        ugpa?.text = data?.studentinfo?.student?.ugpa
        wgpa?.text = data?.studentinfo?.student?.wgpa
        classRank?.text = data?.studentinfo?.student?.classRank
        satScore?.text = data?.studentinfo?.student?.satScore
        actScore?.text = data?.studentinfo?.student?.actScore
        school?.text = data?.studentinfo?.student?.mySchool
        likes?.text = data?.studentinfo?.student?.myLike
        dislikes?.text = data?.studentinfo?.student?.myDislike
        music?.text = data?.studentinfo?.student?.myMusic
        teams?.text = data?.studentinfo?.student?.myTeam
        colleges?.text = data?.studentinfo?.student?.myCollege
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

}