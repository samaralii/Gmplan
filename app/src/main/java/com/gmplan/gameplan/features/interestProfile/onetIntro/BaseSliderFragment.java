package com.gmplan.gameplan.features.interestProfile.onetIntro;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Sammie on 5/4/2017.
 */

public abstract class BaseSliderFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return onCrtView(inflater, container, savedInstanceState);
    }

    abstract View onCrtView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

}
