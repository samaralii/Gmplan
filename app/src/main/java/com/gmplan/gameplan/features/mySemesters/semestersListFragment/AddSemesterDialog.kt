package com.gmplan.gameplan.features.mySemesters.semestersListFragment

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Spinner
import butterknife.*
import com.gmplan.gameplan.R
import com.gmplan.gameplan.features.searchCareers.DefaultSpinnerAdapter
import com.gmplan.gameplan.util.Utilz

/**
 * Created by Sammie on 5/22/2017.
 */
class AddSemesterDialog : DialogFragment() {


    var unbinder: Unbinder? = null

    var listener: DialogListener? = null


    @BindView(R.id.dialog_add_semester_etDetail)
    lateinit var etDetail: EditText

    @BindView(R.id.dialog_add_semester_etTitle)
    lateinit var etTitle: EditText

    @BindView(R.id.dialog_add_semester_spnGrade)
    lateinit var spnGrade: Spinner

    @BindView(R.id.dialog_add_semester_spnSemester)
    lateinit var spnSemester: Spinner


    lateinit var gradeList: Array<String>
    lateinit var semsterList: Array<String>

    var grade: String? = null
    var semester: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            listener = activity!!.supportFragmentManager.findFragmentByTag("semesterFragment") as DialogListener
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.dialog_add_dialog, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        dialog.setTitle("Add Semester")

        gradeList = resources.getStringArray(R.array.grade)
        semsterList = resources.getStringArray(R.array.semester)

        spnGrade.adapter = DefaultSpinnerAdapter(context!!, gradeList)
        spnSemester.adapter = DefaultSpinnerAdapter(context!!, semsterList)


    }


    @OnItemSelected(R.id.dialog_add_semester_spnGrade)
    fun onGradeSelected(pos: Int) {
        grade = gradeList[pos]
    }

    @OnItemSelected(R.id.dialog_add_semester_spnSemester)
    fun onSemesterSelected(pos: Int) {
        semester = semsterList[pos]
    }


    @OnClick(R.id.dialog_add_semester_btnAdd)
    fun onAddClick() {

        if (etTitle.text.isEmpty()) {
            etTitle.error = "Required Field"
            return
        } else {
            etTitle.error = null
        }

        if (grade == gradeList[0]) {
            Utilz.tmsgError(context, "Select Grade")
            return
        }

        if (semester == semsterList[0]) {
            Utilz.tmsgError(context, "Select Semester")
            return
        }

        if (etDetail.text.isEmpty()) {
            etDetail.error = "Required Field"
            return
        } else {
            etDetail.error = null
        }

        listener?.onAddClick(etTitle.text.toString(), etDetail.text.toString(), grade.toString(), semester.toString(), dialog)


    }


    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }


    interface DialogListener {
        fun onAddClick(title: String, detail: String, grade: String, semester: String, dialog: Dialog)
    }


}