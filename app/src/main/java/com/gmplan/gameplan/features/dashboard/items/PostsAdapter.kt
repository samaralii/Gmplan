package com.gmplan.gameplan.features.dashboard.items

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.DashboardItems
import me.grantland.widget.AutofitTextView


class PostsAdapter(val list: List<DashboardItems>, val context: Context, val listener: Listener) : RecyclerView.Adapter<PostsAdapter.PostsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_dashitems, parent, false)
        return PostsViewHolder(v)
    }

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {

        Glide.with(context)
                .load(list[position].imageId)
                .dontAnimate()
                .into(holder.iv)

        holder.tv.text = context.resources.getText(list[position].nameId)
        holder.bg.setBackgroundColor(Color.parseColor(list[position].color))

    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class PostsViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.imageView)
        lateinit var iv: ImageView

        @BindView(R.id.text)
        lateinit var tv: AutofitTextView

        @BindView(R.id.list_dashitems_bg)
        lateinit var bg: RelativeLayout

        init {

            ButterKnife.bind(this, v)

            v.setOnClickListener {

                listener.onClick(list[layoutPosition].id)

            }
        }

//        @OnClick(R.id.imageView)
//        fun onClick() {
//            listener.onClick(list[layoutPosition].id)
//        }


    }

    interface Listener {
        fun onClick(id: Int)
    }

}
