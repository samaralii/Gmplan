package com.gmplan.gameplan.features.interestProfile.questionsView

import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.gmplan.gameplan.BaseDrwrActivity
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.Result
import com.gmplan.gameplan.data.pojos.interestProfileQuestions.Question
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.interestProfile.jobzones.JobZonesActivity
import com.gmplan.gameplan.util.Utilz
import com.gmplan.gameplan.util.hideView
import com.gmplan.gameplan.util.showView
import com.gmplan.gameplan.util.tmsgError
import javax.inject.Inject

/**
 * Created by Sammie on 8/28/2017.
 */
class QuestionsActivity : BaseDrwrActivity<QuestionsView, QuestionsPresenter>(), QuestionsView, QuestionFragment.QuestionFragmentListener {


    var fragment = QuestionFragment()

    var isProceed = false

    private fun showFragment(questions: ArrayList<Question>, index: Int) {
        fragment = QuestionFragment.getInstance(questions, index)
        Utilz.replaceFragmentToActivity(supportFragmentManager, fragment, R.id.activity_questions_container)
    }


    @Inject
    lateinit var schedulers: RxSchedulersImpl

    @Inject
    lateinit var taskRepository: TaskRepository

    override fun gifPresenter(): QuestionsPresenter {
        gifApplication().component?.Inject(this)
        return QuestionsPresenter(taskRepository, schedulers)
    }

    override fun finishCurrentActivity() {
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_questions)
        ButterKnife.bind(this)
        createDrawer(repository = taskRepository)
        init()
    }

    private fun init() {
        (findViewById<View>(R.id.activity_questions_proceed)).isEnabled = false

        showView(R.id.activity_questions_progressBar)

        getUserData?.student?.id?.let {
            presenter.getQuestions(it)
        }

    }

    override fun dismissProgress() {

        hideView(R.id.activity_questions_progressBar)

    }

    override fun onErrorGettingQuestion() {
        Utilz.tmsgError(this, "Error")
    }


    @OnClick(R.id.activity_questions_next)
    fun onNextClick() {

        if (isProceed) {

            showView(R.id.activity_questions_progressBar)
            presenter.onSaveAnswer(getUserData?.student?.id as String)

        } else {
            presenter.onNextClick()
        }

    }

    @OnClick(R.id.activity_questions_back)
    fun onPreviousClick() {
        presenter.onPreviousClick()
    }

    override fun showInitialQuestions(arrayList: ArrayList<Question>, index: Int) {
        showFragment(arrayList, index)
    }

    override fun showNextQuestions(arrayList: ArrayList<Question>, index: Int) {
        showFragment(arrayList, index)
    }

    override fun showPreviousQuestions(arrayList: ArrayList<Question>, index: Int) {
        showFragment(arrayList, index)
    }


    @OnClick(R.id.activity_questions_close)
    fun onCloseClick() {
        finish()
    }

    override fun disableProceedBtn() {
//        (findViewById<View>(R.id.activity_questions_proceed)).isEnabled = false

        isProceed = false
    }

    override fun enableProceedBtn() {
//        (findViewById<View>(R.id.activity_questions_proceed)).isEnabled = true

        isProceed = true

    }

    override fun onDestroyFragment(list: ArrayList<Question>?, index: Int?) {
    }

    override fun onUpdateList(list: ArrayList<Question>?, index: Int?) {
        index?.let { list?.let { it1 -> presenter.onUpdateList(it, it1) } }
    }

    @OnClick(R.id.activity_questions_proceed)
    fun onProceedClick() {
//        (findViewById<View>(R.id.activity_questions_progressBar)).visibility = View.VISIBLE
        showView(R.id.activity_questions_progressBar)
        presenter.onSaveAnswer(getUserData?.student?.id as String)
    }


    override fun onErrorGettingResult() {
        this.tmsgError("Error")
    }

    override fun onSuccessResults(result: ArrayList<Result>) {

        val i = Intent(this, JobZonesActivity::class.java)
        i.putExtra("data", result)
        startActivity(i)
        finish()

    }

}