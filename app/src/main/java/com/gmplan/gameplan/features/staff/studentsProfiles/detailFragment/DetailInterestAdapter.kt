package com.gmplan.gameplan.features.staff.studentsProfiles.detailFragment

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.studentProfileDetails.Rating

class DetailInterestAdapter(private val data: List<Rating>) : RecyclerView.Adapter<DetailInterestAdapter.DetailInterestVH>() {

    override fun onBindViewHolder(holder: DetailInterestVH, position: Int) {
        holder.tvArea.text = data[position].area
        holder.tvDesc.text = data[position].description
        holder.tvScore.text = data[position].score
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailInterestVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_interest, parent, false)
        return DetailInterestVH(v)
    }


    inner class DetailInterestVH(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_interest_area)
        lateinit var tvArea: TextView

        @BindView(R.id.list_interest_score)
        lateinit var tvScore: TextView

        @BindView(R.id.list_interest_desc)
        lateinit var tvDesc: TextView

        init {
            ButterKnife.bind(this, v)
        }
    }
}