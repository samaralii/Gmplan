package com.gmplan.gameplan.features.interestProfile.jobzones.jobzonesfragment

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.adapters.JobZonesAdapter
import com.gmplan.gameplan.data.pojos.Zones
import com.gmplan.gameplan.data.pojos.industryCareersSearch.Career
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.interestProfile.jobzones.JobZonesActivity
import com.gmplan.gameplan.util.hideView
import com.gmplan.gameplan.util.showView
import com.gmplan.gameplan.util.tmsg
import com.gmplan.gameplan.util.tmsgError
import com.hannesdorfmann.mosby.mvp.MvpFragment
import javax.inject.Inject

/**
 * Created by Sammie on 9/12/2017.
 */
class JobZonesFragment : MvpFragment<JobZonesView, JobZonesPresenter>(), JobZonesView {


    @Inject
    lateinit var schedulers: RxSchedulersImpl

    @Inject
    lateinit var taskRepository: TaskRepository

    override fun createPresenter(): JobZonesPresenter {
        (activity as JobZonesActivity).gifApplication().component?.Inject(this)
        return JobZonesPresenter(taskRepository, schedulers)
    }

    var unbinder: Unbinder? = null

    @BindView(R.id.frag_jobzones_list)
    lateinit var list: RecyclerView

    @BindView(R.id.progressDialog)
    lateinit var progress: View


    @BindView(R.id.frag_jobzones_edu)
    lateinit var tvEdu: TextView

    @BindView(R.id.frag_jobzones_exp)
    lateinit var tvExp: TextView

    @BindView(R.id.frag_jobzones_jobTraining)
    lateinit var tvJobTraining: TextView

    @BindView(R.id.frag_jobzones_examples)
    lateinit var tvExamples: TextView

    @BindView(R.id.frag_jobzones_avgSalary)
    lateinit var tvAvgSalary: TextView

    private var listener: JobZonesFragmentListener? = null

    companion object {

        val ZONE = "zone"

        fun newInstance(zone: Int): JobZonesFragment {
            val fragment = JobZonesFragment()
            val b = Bundle()
            b.putInt(ZONE, zone)
            fragment.arguments = b
            return fragment
        }
    }

    var zone = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        zone = arguments!!.getInt(ZONE, 1)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.frag_jobzones, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        progress.showView()
        presenter.getJobZone((activity as JobZonesActivity).getUserData?.student?.id.toString(),
                zone.toString())

        list.layoutManager = LinearLayoutManager(context)
        list.hasFixedSize()

    }

    override fun onErrorGettingJobZones() {
        context!!.tmsgError("Error")
    }

    override fun dismissProgress() {
        progress.hideView()
    }


    override fun onGetAllCareers(careers: ArrayList<Career>) {

        listener?.updateCareerList(careers)

        list.adapter = JobZonesAdapter(careers,
                onJobsClick = { code ->
                    context!!.tmsg(code)
                },
                onUpdateList = { newList ->
                    listener?.updateCareerList(newList)
                }
        )
    }

    override fun onGetZones(zones: Zones, average_salary: Double) {
        tvAvgSalary.text = "$$average_salary"
        tvEdu.text = zones.education
        tvExamples.text = zones.examples
        tvExp.text = zones.experience
        tvJobTraining.text = zones.job_training
    }


    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
        listener?.clearList()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        try {
            listener = activity as JobZonesFragmentListener
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    interface JobZonesFragmentListener {
        fun updateCareerList(list: ArrayList<Career>)
        fun clearList()
    }

}