package com.gmplan.gameplan.features.interestProfile.jobzones.jobzonesfragment

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.JobZones
import com.gmplan.gameplan.data.source.TaskDataSource
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sammie on 9/12/2017.
 */
class JobZonesPresenter(private val repo: TaskDataSource, private val schedulers: RxSchedulers) : MvpBasePresenter<JobZonesView>() {


    private val disposable = CompositeDisposable()

    fun getJobZone(studentId: String, jobZone: String) {
        disposable.add(repo.getJobZones(studentId, jobZone)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally { if (isViewAttached) view?.dismissProgress() }
                .subscribeWith(object : DisposableSingleObserver<JobZones>() {
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        if (isViewAttached) view?.onErrorGettingJobZones()
                    }

                    override fun onSuccess(t: JobZones) {
                        try {

                            val avrgSalary = if (t.average_salary is Double) t.average_salary.toDouble() else 0.0

                            if (isViewAttached) view?.onGetZones(t.zones, avrgSalary)
                            if (isViewAttached) view?.onGetAllCareers(t.careers)
                        } catch (ex: Exception) {
                            if (isViewAttached) view?.onErrorGettingJobZones()
                        }
                    }
                }))
    }

    override fun detachView(retainInstance: Boolean) {
        super.detachView(retainInstance)
        if (!retainInstance) disposable.clear()
    }

}