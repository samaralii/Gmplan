package com.gmplan.gameplan.features.agenda.eventsFragment

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.EventsDes

/**
 * Created by Sammie on 9/25/2017.
 */


class EventsListAdapter(val data: ArrayList<EventsDes>, val onCareerlistClick: (eventId: String) -> Unit) : RecyclerView.Adapter<EventsListVH>() {

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: EventsListVH, pos: Int) {

        holder.title.text = data[pos].event_name
        holder.startDate?.text = data[pos].event_start_date
        holder.endDate?.text = data[pos].event_end_date

        holder.btnCheckList?.setOnClickListener {
            onCareerlistClick.invoke(data[holder.layoutPosition].id)
        }

    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): EventsListVH {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.list_events, p0, false)
        return EventsListVH(v)
    }
}

class EventsListVH(v: View) : RecyclerView.ViewHolder(v) {
    val title by lazy { v.findViewById<TextView>(R.id.list_events_title) }
    val startDate by lazy { v.findViewById<TextView>(R.id.list_events_startdate) }
    val endDate by lazy { v.findViewById<TextView>(R.id.list_events_enddate) }
    val btnCheckList by lazy { v.findViewById<Button>(R.id.list_events_btnChecklist) }
}