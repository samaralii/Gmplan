package com.gmplan.gameplan.features.mySemesters.semestersListFragment

/**
 * Created by Sammie on 5/19/2017.
 */
interface MySemestersView: com.hannesdorfmann.mosby.mvp.MvpView {
    fun onFailedGettingSemesters()
    fun onNoSemestersGoalFound()
    fun onSuccessSemestersList(semestergoals: List<com.gmplan.gameplan.data.pojos.Semestergoal>)
    fun dismissDialog()
    fun deleteSuccessfully(pos: Int)
    fun onFailedDelete()
    fun onSuccessfullySemesterAdded()
    fun onFailedAddSemester()
}