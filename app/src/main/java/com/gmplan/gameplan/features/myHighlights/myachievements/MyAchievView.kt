package com.gmplan.gameplan.features.myHighlights.myachievements

import com.gmplan.gameplan.data.pojos.Achievement
import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Sammie on 5/22/2017.
 */
interface MyAchievView : MvpView {
    fun dismissDialog()
    fun onFailedGettingList()
    fun onNoAchievementsFound()
    fun onSuccessAchievementsFound(achievement: MutableList<Achievement>)
    fun onFailedPosting()
    fun onSuccessPost()
}