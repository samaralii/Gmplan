package com.gmplan.gameplan.features.staff.studentsProfiles.detailFragment

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.studentProfileDetails.Studentcollege

/**
 * Created by Sammie on 6/12/2017.
 */
class DetailFavoriteCollegeAdapter(private val data: List<Studentcollege>) : RecyclerView.Adapter<DetailFavoriteCollegeAdapter.DetailFavCollegeVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailFavCollegeVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_detail_favcollege, parent, false)
        return DetailFavCollegeVH(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: DetailFavCollegeVH, position: Int) {
        holder.tvTitle.text = data[position].collegeName
    }


    inner class DetailFavCollegeVH(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_detail_favcollege_title)
        lateinit var tvTitle: TextView

        @BindView(R.id.list_detail_favcollege_like)
        lateinit var like: ImageView


        init {
            ButterKnife.bind(this, v)
        }

        @OnClick(R.id.list_detail_favcollege_like)
        fun onLikeClick() {

        }

    }
}
