package com.gmplan.gameplan.features.searchCareers

import com.gmplan.gameplan.data.pojos._Career
import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Sammie on 5/3/2017.
 */
interface SearchCareersView: MvpView {
    fun dismissProgress()
    fun onErrorGettingCareers()
    fun industryCareerList(careers: MutableList<_Career>)
    fun stemCareerList(stemCareers: MutableList<_Career>)
}