package com.gmplan.gameplan.features.interestProfile.jobzones

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.gmplan.gameplan.BaseDrwrActivity
import com.gmplan.gameplan.EmptyPresenter
import com.gmplan.gameplan.EmptyView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.Result
import com.gmplan.gameplan.data.pojos.SavingCareer
import com.gmplan.gameplan.data.pojos._Careerslist
import com.gmplan.gameplan.data.pojos.industryCareersSearch.Career
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.interestProfile.jobzones.jobzonesfragment.JobZonesFragment
import com.gmplan.gameplan.features.interestProfile.questionsView.QuestionsActivity
import com.gmplan.gameplan.infrastructure.GmplanApplication
import com.gmplan.gameplan.util.Utilz
import com.google.gson.Gson
import com.google.gson.JsonElement
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class JobZonesActivity : BaseDrwrActivity<EmptyView, EmptyPresenter>(), JobZonesFragment.JobZonesFragmentListener {

    override fun gifPresenter() = EmptyPresenter()


    override fun finishCurrentActivity() {
    }

    var count = 1

    @Inject
    lateinit var repoo: TaskRepository

    private var careerList: ArrayList<Career>? = null

    private var JobZone = "1"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jobzones)

        (application as GmplanApplication).component?.Inject(this)

        openResultFragment()

        (findViewById<View>(R.id.activity_jobzones_next)).setOnClickListener {

            count++

            if (count <= 7) {

                if (count > 3)
                    saveCareer()
                else
                    openDesiredFragment()


            } else {
                count = 7
            }

        }

        (findViewById<View>(R.id.activity_jobzones_back)).setOnClickListener {

            count--

            if (count < 1) {
                startActivity(Intent(this, QuestionsActivity::class.java))
                finish()
            }

            if (count >= 1) {
                openDesiredFragment()
            } else {
                count = 1
            }

        }

        (findViewById<View>(R.id.activity_jobzones_close)).setOnClickListener {
            finish()
        }

    }

    private fun openDesiredFragment() {

        when (count) {
            1 -> openResultFragment()
            2 -> openJobZonesTextFragment()
            3 -> {
                openJobZonesFragment(1)
                JobZone = "1"
            }
            4 -> {
                openJobZonesFragment(2)
                JobZone = "2"
            }
            5 -> {
                openJobZonesFragment(3)
                JobZone = "3"
            }
            6 -> {
                openJobZonesFragment(4)
                JobZone = "4"
            }
            7 -> {
                openJobZonesFragment(5)
                JobZone = "5"
            }
        }

    }

    private fun openResultFragment() {
        val result = intent.getParcelableArrayListExtra<Result>("data")
        Utilz.replaceFragmentToActivity(supportFragmentManager,
                ResultFragment.newInstance(result), R.id.activity_jobzones_container)
    }

    private fun openJobZonesTextFragment() {
        Utilz.replaceFragmentToActivity(supportFragmentManager,
                JobZonesTextFragment.newInstance(), R.id.activity_jobzones_container)
    }

    private fun openJobZonesFragment(zone: Int) {
        Utilz.replaceFragmentToActivity(supportFragmentManager,
                JobZonesFragment.newInstance(zone), R.id.activity_jobzones_container)
    }


    override fun updateCareerList(list: ArrayList<Career>) {
        careerList = list
    }

    override fun clearList() {
        careerList = null
    }

    fun saveCareer() {

        if (careerList == null) {
            openDesiredFragment()
            return
        }

        val cList = careerList as ArrayList<Career>

        if (cList.isEmpty()) {
            openDesiredFragment()
            return
        }

        val list = mutableListOf<_Careerslist>()

        val studentId = getUserData?.student?.id.toString()

        (0 until cList.size)
                .filter { cList[it].isChecked }
                .forEach {
                    list.add(_Careerslist(
                            cList[it].title,
                            cList[it].code,
                            JobZone,
                            studentId,
                            "1"
                    ))
                }

        val careers = SavingCareer(list)

        val gson = Gson()
        val careersJson = gson.toJson(careers)

        Log.e("JSON", careersJson)

        repoo.addJobZonesCareer(careersJson, studentId, JobZone)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { openDesiredFragment() }
                .subscribeWith(object : DisposableSingleObserver<JsonElement>() {
                    override fun onSuccess(t: JsonElement) {

                        try {

                            val obj = t.asJsonObject
                            val res = obj.get("response").toString()
//
//                            if (res.equals("success", true))


                        } catch (e: Exception) {
                            e.printStackTrace()
//                            this@JobZonesActivity.tmsgError("Error Saving Careers")

                        }

                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
//                        this@JobZonesActivity.tmsgError("Error Saving Careers")

                    }

                })

    }


}