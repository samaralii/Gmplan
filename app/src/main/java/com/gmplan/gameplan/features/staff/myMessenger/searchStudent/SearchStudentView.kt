package com.gmplan.gameplan.features.staff.myMessenger.searchStudent

import com.gmplan.gameplan.data.pojos.staffStudent.StaffStudent
import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Sammie on 6/20/2017.
 */
interface SearchStudentView: MvpView {
    fun dismissDialog()
    fun onErrorGettingFieldsValue()
    fun onSuccessGettingFieldsValue(schoolList: MutableList<String>, citiesList: MutableList<String>, countriesList: MutableList<String>, statesList: MutableList<String>)
    fun onStudentsFound(p0: List<StaffStudent>)
    fun onErrorSearchStudent()
    fun onSuccessMessageSent()
    fun onErrorMessageSent()
}