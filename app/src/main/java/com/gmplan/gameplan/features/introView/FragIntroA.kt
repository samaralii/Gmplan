package com.gmplan.gameplan.features.introView

import android.animation.Animator
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.gmplan.gameplan.R

/**
 * Created by Sammie on 7/7/2017.
 */
class FragIntroA : Fragment() {

    var unbinder: Unbinder? = null

    @BindView(R.id.intro_gLogo)
    lateinit var logo: ImageView

    @BindView(R.id.welcometext)
    lateinit var welcomeTxt: TextView

    companion object {
        fun newInstance(): FragIntroA {
            return FragIntroA()
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.frag_a, container, false) as View
        ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }


    fun playAnimation() {

        YoYo.with(Techniques.RollIn)
                .duration(1500)
                .withListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(animation: Animator?) {
                    }

                    override fun onAnimationEnd(animation: Animator?) {

                        YoYo.with(Techniques.BounceIn)
                                .withListener(object : Animator.AnimatorListener {
                                    override fun onAnimationRepeat(a: Animator?) {
                                    }

                                    override fun onAnimationEnd(a: Animator?) {
                                    }

                                    override fun onAnimationCancel(a: Animator?) {
                                    }

                                    override fun onAnimationStart(a: Animator?) {
                                        welcomeTxt.visibility = View.VISIBLE
                                    }

                                })
                                .playOn(welcomeTxt)

                    }

                    override fun onAnimationCancel(animation: Animator?) {
                    }

                    override fun onAnimationStart(animation: Animator?) {
                    }

                })
                .playOn(logo)

    }

    fun hideText() {
        if (welcomeTxt.visibility == View.VISIBLE)
            welcomeTxt.visibility = View.INVISIBLE
    }

}