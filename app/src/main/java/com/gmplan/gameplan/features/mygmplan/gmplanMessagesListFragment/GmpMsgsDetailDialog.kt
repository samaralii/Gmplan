package com.gmplan.gameplan.features.mygmplan.gmplanMessagesListFragment

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.gmplan.gameplan.R

/**
 * Created by Sammie on 5/29/2017.
 */
class GmpMsgsDetailDialog(val txtBody: String) : DialogFragment() {


    var unbinder: Unbinder? = null

    @BindView(R.id.dialog_msg_detail_body)
    lateinit var tvBody: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.dialog_msg_detail, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        dialog.setTitle("Detail")

        tvBody.text = txtBody

    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }
}