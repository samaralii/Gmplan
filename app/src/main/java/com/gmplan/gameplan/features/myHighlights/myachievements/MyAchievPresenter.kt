package com.gmplan.gameplan.features.myHighlights.myachievements

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.UserAchievements
import com.gmplan.gameplan.data.source.TaskDataSource
import com.google.gson.JsonElement
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sammie on 5/22/2017.
 */
class MyAchievPresenter(val repo: TaskDataSource, val schedulers: RxSchedulers) : MvpBasePresenter<MyAchievView>() {


    fun getUserAchievements(studentId: String) {
        repo.getUserAchievements(studentId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<UserAchievements>() {
                    override fun onSuccess(p0: UserAchievements) {

                        if (p0.response == "success") {

                            if (p0.achievement.size > 0) {

                                if (isViewAttached)
                                    view?.onSuccessAchievementsFound(p0.achievement)

                            } else {

                                if (isViewAttached)
                                    view?.onNoAchievementsFound()

                            }

                        } else {

                            if (isViewAttached)
                                view?.onFailedGettingList()

                        }


                    }

                    override fun onError(p0: Throwable) {
                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onFailedGettingList()

                    }
                })
    }

    fun postAchievement(studentId: String, title: String, detail: String) {
        repo.postAchievement(studentId, title, detail)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<JsonElement>() {

                    override fun onSuccess(p0: JsonElement) {

                        val obj = p0.asJsonObject
                        val status = obj.get("status").asString

                        if (status == "success") {

                            if (isViewAttached)
                                view?.onSuccessPost()

                        }

                    }

                    override fun onError(p0: Throwable) {
                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onFailedPosting()
                    }

                })
    }
}