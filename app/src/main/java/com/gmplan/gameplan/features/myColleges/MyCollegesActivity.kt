package com.gmplan.gameplan.features.myColleges

import android.os.Bundle
import android.support.v7.widget.Toolbar
import com.gmplan.gameplan.BaseDrwrActivity
import com.gmplan.gameplan.EmptyPresenter
import com.gmplan.gameplan.EmptyView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.myColleges.myCollegeListFragment.MyCollegeFragment
import com.gmplan.gameplan.util.Utilz
import javax.inject.Inject

class MyCollegesActivity : BaseDrwrActivity<EmptyView, EmptyPresenter>() {

    override fun gifPresenter() = EmptyPresenter()

    override fun finishCurrentActivity() {
        finish()
    }

    @Inject
    lateinit var repository: TaskRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mycareers)
        gifApplication().component?.Inject(this)
        val toolbar = findViewById<Toolbar>(R.id.app_toolbar) as Toolbar
        setSupportActionBar(toolbar)
        title = "My Colleges"
        createDrawer(toolbar, repository, true)
        Utilz.addFragmentToActivity(supportFragmentManager, MyCollegeFragment(), R.id.activity_mycareers_container)
    }

}