package com.gmplan.gameplan.features.searchCareers

import android.os.Bundle
import android.support.v7.widget.Toolbar
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.BaseDrwrActivity
import com.gmplan.gameplan.EmptyPresenter
import com.gmplan.gameplan.EmptyView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.util.Utilz
import javax.inject.Inject

/**
 * Created by Sammie on 5/3/2017.
 */

class SearchCareersActivity : BaseDrwrActivity<EmptyView, EmptyPresenter>() {
    override fun gifPresenter() = EmptyPresenter()


    override fun finishCurrentActivity() {
        finish()
    }

    @BindView(R.id.app_toolbar)
    lateinit var toolbar: Toolbar


    @Inject
    lateinit var repository: TaskRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_careers)
        gifApplication().component?.Inject(this)
        ButterKnife.bind(this)
        init()
    }

    private fun init() {

        setSupportActionBar(toolbar)
        supportActionBar?.title = "Search Careers"
        createDrawer(toolbar, repository, true, 4)

        Utilz.addFragmentToActivity(supportFragmentManager, SearchCareersFragment.newInstance(),
                R.id.activity_search_careers_container)


    }

    override fun onBackPressed() {
        val c = supportFragmentManager.backStackEntryCount
        if (c > 0)
            supportFragmentManager.popBackStack()
        else
            super.onBackPressed()
    }


}
