package com.gmplan.gameplan.features.myRecruiters

import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.data.pojos.MyRecruitersPojo
import com.gmplan.gameplan.data.source.TaskDataSource
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sammie on 5/10/2017.
 */
class MyRecruiterPresenter(val repo: TaskDataSource, val schedulers: RxSchedulersImpl): MvpBasePresenter<MyRecruiterView>() {


    fun getRecruiters(studentId: String) {
        repo.userRecruiters(studentId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissProgress()

                }
                .subscribeWith(object : DisposableSingleObserver<MyRecruitersPojo>() {

                    override fun onError(p0: Throwable) {

                        if (isViewAttached)
                            view?.onError()

                    }

                    override fun onSuccess(data: MyRecruitersPojo) {

                        if (data.response == "success" && data.recruiters != null && data.recruiters.size > 0) {

                            if (isViewAttached)
                                view?.onSuccess(data.recruiters)


                        } else {

                            if (isViewAttached)
                                view?.onNoRecruiterFound()

                        }


                    }

                })
    }

}