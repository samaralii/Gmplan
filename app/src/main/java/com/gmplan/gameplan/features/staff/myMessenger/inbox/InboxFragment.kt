package com.gmplan.gameplan.features.staff.myMessenger.inbox

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.MessagesObject
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.staff.myMessenger.MyMessengerActivity
import com.gmplan.gameplan.util.AppConst
import com.gmplan.gameplan.util.Utilz
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.frag_inout.*
import javax.inject.Inject

class InboxFragment : Fragment() {

    private val disposal = CompositeDisposable()

    @Inject
    lateinit var repo: TaskRepository

    var adapter: InboxAdapter? = null

    companion object {
        fun newInstance() = InboxFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.frag_inout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        (activity as MyMessengerActivity).gifApplication().component?.Inject(this)
        adapter = InboxAdapter(context = (activity as MyMessengerActivity).applicationContext)

        inbox_list.layoutManager = LinearLayoutManager(context)
        inbox_list.adapter = adapter

        getInboxList()

        inbox_sr.setOnRefreshListener { getInboxList() }
    }

    private fun getInboxList() {
        inbox_sr.isRefreshing = true
        val collegeId = repo.getUserData(AppConst.USER_DATA).userData.admissionStaff.collegeId
        disposal.add(repo.getInboxList(collegeId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { inbox_sr.isRefreshing = false }
                .subscribeWith(object : DisposableSingleObserver<List<MessagesObject>>() {
                    override fun onSuccess(t: List<MessagesObject>) {

                        try {
                            if (t.isNotEmpty()) {
                                adapter?.addData(t)
                            } else {
                                Utilz.tmsg(context, "No Item In Inbox")
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            Utilz.tmsg(context, "Error")
                        }

                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        Utilz.tmsg(context, "Error")
                    }

                }))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposal.clear()
    }

}