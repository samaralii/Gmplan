package com.gmplan.gameplan.features.messages.messagesListFragment

import com.gmplan.gameplan.data.pojos.ShortMessageDetail
import com.gmplan.gameplan.data.pojos.detailMessagePojos.Messages
import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Sammie on 5/25/2017.
 */
interface MessagesView: MvpView {
    fun dismissDialog()
    fun onFailedGettingList()
    fun onNoMessagesFound()
    fun onSuccessMessages(messages: MutableList<ShortMessageDetail>)
    fun onFailedGettingDetail()
    fun onSuccessMessageDetail(messages: Messages)
    fun onFailedReply()
    fun onSuccessReply()
}