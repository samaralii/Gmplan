package com.gmplan.gameplan.features.myColleges.myCollegeListFragment

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.College

/**
 * Created by Sammie on 5/18/2017.
 */
class CollegeAdapter(val data: List<College>, val listener: Listener, val context: Context) : RecyclerView.Adapter<CollegeAdapter.CollegeViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CollegeViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_mycollege, parent, false)
        return CollegeViewHolder(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: CollegeViewHolder, position: Int) {

        holder.title.text = data[position].collegeName


        holder.let {

            it.menu.setOnClickListener { _->

                val popup = PopupMenu(context, it.menu)
                popup.inflate(R.menu.options_menu_default)

                popup.setOnMenuItemClickListener {

                    when(it.itemId) {
//                        R.id.menu1 ->  listeners.onDeleteClick(data[position].id, position)
                    }

                    false
                }

                popup.show()

            }


        }


    }


    inner class CollegeViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_mycollege_Title)
        lateinit var title: TextView

        @BindView(R.id.list_mycollege_menu)
        lateinit var menu: View

        init {
            ButterKnife.bind(this, v)

            v.setOnClickListener {
                listener.onItemClick(data[layoutPosition].collegeId)
            }
        }


    }

    interface Listener {
        fun onItemClick(code: String)
    }

}