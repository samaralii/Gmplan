package com.gmplan.gameplan.features.agenda.createAgenda

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.staffStudent.StaffStudent

/**
 * Created by Sammie on 10/5/2017.
 */

class SelectStudentsAdapter(var data: List<StaffStudent>,
                            val updateList: (List<String>) -> Unit)
    : RecyclerView.Adapter<SelectStudetnVH>() {

    var isSelectAll = false

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SelectStudetnVH {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.list_selectstudent, p0, false)
        return SelectStudetnVH(v)
    }

    override fun onBindViewHolder(holder: SelectStudetnVH, pos: Int) {

        (0 until data.size).forEach { data[it].isSelected = isSelectAll }
        holder.cbStudent.isChecked = isSelectAll


        holder.cbStudent?.text = "${data[pos].student.firstName} ${data[pos].student.lastName}"

        holder.cbStudent?.setOnCheckedChangeListener { _, b ->

            data[pos].isSelected = b

            val newList = mutableListOf<String>()

            (0 until data.size)
                    .filter { data[it].isSelected }
                    .forEach { newList.add(data[it].student.id) }

            updateList.invoke(newList)

        }


    }

    override fun getItemCount() = data.size


    fun selectAll(selectAll: Boolean) {
        isSelectAll = selectAll
        notifyDataSetChanged()
    }


}

class SelectStudetnVH(v: View) : RecyclerView.ViewHolder(v) {
    val cbStudent by lazy { v.findViewById<CheckBox>(R.id.list_selectstudent_cbStudent) }
}