package com.gmplan.gameplan.features.searchCareers.stemCareers

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.StemCareers
import com.gmplan.gameplan.data.pojos._Career
import com.gmplan.gameplan.data.pojos.industryCareersSearch.IndustryCareers
import com.gmplan.gameplan.data.source.TaskDataSource
import com.google.gson.JsonElement
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sammie on 9/7/2017.
 */
interface StemCareerView: MvpView {
    fun dismissProgress()
    fun onErrorGettingList()
    fun onSuccessGettingList(stemCareers: MutableList<_Career>)
    fun onErrorAdding()
    fun SuccessAddedCareer()

}


class StemCareersPresenter(private val repo: TaskDataSource, private val schedulers: RxSchedulers):
        MvpBasePresenter<StemCareerView>(){


    private val disposable = CompositeDisposable()

    fun getList(careerId: String) {
        repo.findStemCareers(careerId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally { if (isViewAttached) view?.dismissProgress() }
                .subscribeWith(object : DisposableSingleObserver<StemCareers>() {
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        if (isViewAttached) view?.onErrorGettingList()
                    }

                    override fun onSuccess(t: StemCareers) {

                        if (t.stemCareer != null){
                            if (t.stemCareer.isNotEmpty()) {

                                if (isViewAttached) view?.onSuccessGettingList(t.stemCareer)

                            } else {
                                if (isViewAttached) view?.onErrorGettingList()
                            }
                        } else {
                            if (isViewAttached) view?.onErrorGettingList()
                        }
                    }
                })
    }


    fun addCareer(careerCode: String, studentId: String, careerTitle: String) {
        repo.addCareer(careerCode, studentId, careerTitle)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally { if (isViewAttached) view?.dismissProgress() }
                .subscribeWith(object : DisposableSingleObserver<JsonElement>() {
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        if (isViewAttached) view?.onErrorAdding()
                    }

                    override fun onSuccess(t: JsonElement) {

                        try {

                            val obj = t.asJsonObject

                            val response = obj.get("response").asString

                            if (response == "success") {
                                if (isViewAttached) view?.SuccessAddedCareer()
                            } else {
                                if (isViewAttached) view?.onErrorAdding()
                            }



                        } catch (e: Exception) {
                            if (isViewAttached) view?.onErrorAdding()
                        }
                    }
                })
    }


    override fun detachView(retainInstance: Boolean) {
        super.detachView(retainInstance)
        if (!retainInstance) disposable.clear()
    }


}