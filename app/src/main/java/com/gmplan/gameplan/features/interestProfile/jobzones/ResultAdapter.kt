package com.gmplan.gameplan.features.interestProfile.jobzones

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.Result

/**
 * Created by Sammie on 9/8/2017.
 */
class ResultAdapter(val data: ArrayList<Result>) : RecyclerView.Adapter<ResultAdapter.ResultVH>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ResultVH {
        val v = LayoutInflater.from(p0.context)?.inflate(R.layout.list_result, p0, false) as View
        return ResultVH(v)
    }

    override fun onBindViewHolder(h: ResultVH, p: Int) {
        h.area.text = data[p].area
        h.score.text = data[p].score
        h.des.text = data[p].description
    }

    override fun getItemCount(): Int {
        return data.size
    }


    inner class ResultVH(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_result_area)
        lateinit var area: TextView

        @BindView(R.id.list_result_score)
        lateinit var score: TextView

        @BindView(R.id.list_result_des)
        lateinit var des: TextView


        init {
            ButterKnife.bind(this, v)
        }

    }
}