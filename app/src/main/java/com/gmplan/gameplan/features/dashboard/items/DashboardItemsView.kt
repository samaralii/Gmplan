package com.gmplan.gameplan.features.dashboard.items

import com.gmplan.gameplan.data.pojos.DashboardItems
import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Sammie on 5/15/2017.
 */
interface DashboardItemsView: MvpView {
    fun dashBoardItems(items: List<DashboardItems>)
    fun openMyCareerActivity()
    fun openMyCollegeActivity()
    fun openMySemestersActivity()
    fun openHighlightActivity()
    fun openMessagesActivity()
    fun openMyGmplanActivity()
    fun myOffersActivity()
    fun openStudentProfileActivity()
    fun openUpdateStatusActivity()
    fun openMyStudentsActivity()
    fun openProfileActivity()
    fun openAgendaActivity()
    fun openMyMessageActivity()
}