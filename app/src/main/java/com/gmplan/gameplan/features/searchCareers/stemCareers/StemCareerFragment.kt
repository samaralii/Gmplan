package com.gmplan.gameplan.features.searchCareers.stemCareers

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos._Career
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.searchCareers.SearchCareersActivity
import com.gmplan.gameplan.util.hide
import com.gmplan.gameplan.util.show
import com.gmplan.gameplan.util.tmsgError
import com.gmplan.gameplan.util.tmsgSuccess
import com.hannesdorfmann.mosby.mvp.MvpFragment
import javax.inject.Inject

/**
 * Created by Sammie on 9/7/2017.
 */
class StemCareerFragment : MvpFragment<StemCareerView, StemCareersPresenter>(), StemCareerView, StemCareersAdapter.ListListener {


    var unbinder: Unbinder? = null

    @Inject
    lateinit var schedulers: RxSchedulersImpl

    @Inject
    lateinit var taskRepository: TaskRepository

    @BindView(R.id.frag_stem_careers_list)
    lateinit var list: RecyclerView

    @BindView(R.id.sr)
    lateinit var sr: SwipeRefreshLayout

    var careerId = ""


    companion object {

        private val CAREER_ID = "careerid"

        fun newInstance(careerId: String): StemCareerFragment {
            val fragment = StemCareerFragment()
            val b = Bundle()
            b.putString(CAREER_ID, careerId)
            fragment.arguments = b
            return fragment
        }

    }

    override fun createPresenter(): StemCareersPresenter {
        (activity as SearchCareersActivity).gifApplication().component?.Inject(this)
        return StemCareersPresenter(taskRepository, schedulers)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        careerId = arguments!!.getString(CAREER_ID)

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.frag_stem_careers, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        list.layoutManager = LinearLayoutManager(context)


        refreshList()

        sr.setOnRefreshListener {
            refreshList()
        }

    }

    private fun refreshList() {
        sr.show()
        presenter.getList(careerId)
    }


    override fun dismissProgress() {
        sr.hide()
    }

    override fun onErrorGettingList() {
        context!!.tmsgError("Error")
    }

    override fun onSuccessGettingList(stemCareers: MutableList<_Career>) {
        list.adapter = StemCareersAdapter(stemCareers, this)
    }

    override fun onAddClick(career: _Career) {
        sr.show()
        presenter.addCareer(career.code,
                (activity as SearchCareersActivity).getUserData?.student?.id as String, career.title)
    }

    override fun onErrorAdding() {
        context!!.tmsgError("Error")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

    override fun SuccessAddedCareer() {
        context!!.tmsgSuccess("Added")
    }


}
