package com.gmplan.gameplan.features.agenda


import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.CheckList
import kotlinx.android.synthetic.main.fragment_check_list_dialog.*
import kotlinx.android.synthetic.main.list_gmplist.*


class CheckListDialog : DialogFragment() {


    companion object {
        private val LIST = "list"

        fun newInstance(data: ArrayList<CheckList>): CheckListDialog {
            val d = CheckListDialog()
            val b = Bundle()
            d.arguments = b
            b.putParcelableArrayList(LIST, data)
            return d
        }
    }

    private val checkList: ArrayList<CheckList> by lazy { arguments!!.getParcelableArrayList<CheckList>(LIST) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_check_list_dialog, container, false)
    }


    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dialog.setTitle("Checklist")

        frag_check_list_list.layoutManager = LinearLayoutManager(context)
        frag_check_list_list.hasFixedSize()
        frag_check_list_list.adapter = CheckListAdapter(checkList)
    }

}
