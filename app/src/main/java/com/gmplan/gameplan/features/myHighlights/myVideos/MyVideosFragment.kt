package com.gmplan.gameplan.features.myHighlights.myVideos

import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.Video
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.myHighlights.MyHighlightsActivity
import com.gmplan.gameplan.util.Utilz
import com.hannesdorfmann.mosby.mvp.MvpFragment
import javax.inject.Inject

/**
 * Created by Sammie on 5/22/2017.
 */
class MyVideosFragment : MvpFragment<MyVideosView, MyVideosPresenter>(), MyVideosView, MyVideosAdapter.Listener, AddVideoDialog.DialogListener {


    @Inject
    lateinit var schedulers: RxSchedulersImpl

    @Inject
    lateinit var taskRepository: TaskRepository

    var unbinder: Unbinder? = null

    @BindView(R.id.frag_myvideos_list)
    lateinit var recyclerView: RecyclerView

    @BindView(R.id.progressDialog)
    lateinit var progressDialog: View

    var adapter: MyVideosAdapter? = null

    @BindView(R.id.frag_myvideos_sr)
    lateinit var swipeToRefresh: SwipeRefreshLayout

    companion object {
        fun getInstance(): MyVideosFragment {
            return MyVideosFragment()
        }
    }

    override fun createPresenter(): MyVideosPresenter {
        (activity as MyHighlightsActivity).gifApplication().component?.Inject(this)
        return MyVideosPresenter(taskRepository, schedulers)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.frag_myvideos, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        recyclerView.layoutManager = LinearLayoutManager(context)

        refreshList()

        swipeToRefresh.setOnRefreshListener { refreshList() }

    }

    fun refreshList() {
        showProgress()
        presenter.getUserVideos((activity as MyHighlightsActivity).getUserData?.student?.id.toString())
    }


    @OnClick(R.id.frag_myVideos_btnAdd)
    fun onAddClick() {
        val dialog = AddVideoDialog(this)
        dialog.show(fragmentManager, "dialog")
    }

    override fun onAddVideo(dialog: DialogInterface, name: String, url: String) {
        dialog.dismiss()
        showProgress()
        presenter.postVideo((activity as MyHighlightsActivity).getUserData?.student?.id.toString(),
                url, name)
    }


    private fun showProgress() {

//        if (!progressDialog.isShown)
//            progressDialog.visibility = View.VISIBLE

        swipeToRefresh.isRefreshing = true

    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

    override fun dismissDialog() {

        if (progressDialog.isShown)
            progressDialog.visibility = View.GONE


        if (swipeToRefresh.isRefreshing)
            swipeToRefresh.isRefreshing = false

    }


    override fun onFailedGettingUserVideos() {
        Utilz.tmsgError(context, "Error Getting Videos")
    }

    override fun onNoVideoFound() {
        Utilz.tmsgError(context, "No Videos Found")
    }

    override fun onSuccessVideoFound(videos: MutableList<Video>) {
        adapter = MyVideosAdapter(videos, this, context!!)
        recyclerView.adapter = adapter
    }

    override fun onDeleteItem(id: String, pos: Int) {
        showProgress()
        presenter.deleteVideo(id, pos)
    }

    override fun onEditItem(id: String, url: String, name: String) {
        val dialog = AddVideoDialog(this, true, url, name, id)
        dialog.show(fragmentManager, "dialog")
    }

    override fun onEditVideo(dialog: DialogInterface, name: String, url: String, id: String) {
        dialog.dismiss()
        showProgress()
        presenter.editVideo(id, url, name)
    }


    override fun onFailedPostingVideo() {
        Utilz.tmsgError(context, "Error Posting Video")
    }

    override fun onSuccessVideoPosted() {
        Utilz.tmsgSuccess(context, "Done")
        refreshList()
    }

    override fun onFailedEditingVideo() {
        Utilz.tmsgError(context, "Error Editing Video")
    }

    override fun onSuccessVideoEdited() {
        Utilz.tmsgSuccess(context, "Done")
        refreshList()
    }

    override fun onSuccessVideoDeleted(pos: Int) {
        Utilz.tmsgSuccess(context, "Done")
        adapter?.removeAt(pos)
    }

    override fun onFailedDeletingVideo() {
        Utilz.tmsgError(context, "Error Deleting Video")
    }

}