package com.gmplan.gameplan.features.staff.myMessenger.searchStudent

import android.app.Dialog
import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.fieldsValue.FieldsValue
import com.gmplan.gameplan.data.pojos.staffStudent.StaffStudent
import com.gmplan.gameplan.data.source.TaskDataSource
import com.google.gson.JsonElement
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sammie on 6/20/2017.
 */
class SearchStudentPresenter(val repo: TaskDataSource, val schedulers: RxSchedulers) :
        MvpBasePresenter<SearchStudentView>() {

    val disposable = CompositeDisposable()


    fun getStudentFieldsValues(staffId: String) {
        disposable.add(repo.getStudentFieldsValues(staffId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<FieldsValue>() {
                    override fun onSuccess(p0: FieldsValue) {

                        if (p0.response == "success") {

                            onSuccessGettingFieldsValue(p0)

                        } else {

                            if (isViewAttached)
                                view?.onErrorGettingFieldsValue()

                        }

                    }

                    override fun onError(p0: Throwable) {
                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onErrorGettingFieldsValue()
                    }

                }))
    }

    fun searchStaffStudent(staffId: String, collegeId: String, name: String) {
        disposable.add(repo.searchStaffStudent(staffId, collegeId, name)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<List<StaffStudent>>() {
                    override fun onSuccess(p0: List<StaffStudent>) {

                        if (p0.isNotEmpty()) {

                            if (isViewAttached)
                                view?.onStudentsFound(p0)


                        } else {

                            if (isViewAttached)
                                view?.onErrorSearchStudent()

                        }

                    }

                    override fun onError(p0: Throwable) {
                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onErrorSearchStudent()
                    }

                }))
    }

    private fun onSuccessGettingFieldsValue(fieldsValue: FieldsValue) {

        val schoolList: MutableList<String> = ArrayList()
        val citiesList: MutableList<String> = ArrayList()
        val countriesList: MutableList<String> = ArrayList()
        val statesList: MutableList<String> = ArrayList()

//        for (v in fieldsValue.schools){
//            schoolList.add(v.name)
//        }

        fieldsValue.schools.mapTo(schoolList) { it.name }
        fieldsValue.cities.mapTo(citiesList) { it.name }
        fieldsValue.counties.mapTo(countriesList) { it.name }
        fieldsValue.states.mapTo(statesList) { it.name }


        schoolList.add(0, "Select School")
        citiesList.add(0, "Select Cities")
        countriesList.add(0, "Select Countries")
        statesList.add(0, "Select States")

        if (isViewAttached)
            view?.onSuccessGettingFieldsValue(schoolList, citiesList, countriesList, statesList)


    }


    fun sendMessage(collegeId: String, staffId: String, staffName: String, studentId: String, staffTitle: String,
                    staffCollege: String, userId: String, subject: String, message: String, dialog: Dialog){
        repo.staffSendMessage(collegeId, staffId, studentId, staffName, staffTitle, staffCollege, userId,
                subject, message)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<JsonElement>() {

                    override fun onSuccess(p0: JsonElement) {

                        if (isViewAttached)
                            view?.onSuccessMessageSent()

                        dialog.dismiss()

                    }

                    override fun onError(p0: Throwable) {
                        p0.printStackTrace()


                        if (isViewAttached)
                            view?.onErrorMessageSent()
                    }

                })
    }

    override fun detachView(retainInstance: Boolean) {
        super.detachView(retainInstance)
        if (!retainInstance)
            disposable.clear()
    }


}