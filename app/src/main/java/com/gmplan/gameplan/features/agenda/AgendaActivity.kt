package com.gmplan.gameplan.features.agenda

import android.os.Bundle
import android.support.v7.widget.Toolbar
import butterknife.ButterKnife
import com.gmplan.gameplan.BaseDrwrActivity
import com.gmplan.gameplan.EmptyPresenter
import com.gmplan.gameplan.EmptyView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.agenda.agendaFragment.AgendaFragment
import com.gmplan.gameplan.util.Utilz
import javax.inject.Inject

class AgendaActivity : BaseDrwrActivity<EmptyView, EmptyPresenter>() {
    override fun gifPresenter() = EmptyPresenter()

    override fun finishCurrentActivity() {
        finish()
    }

    @Inject
    lateinit var repository: TaskRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activiy_agenda)
        gifApplication().component?.Inject(this)
        ButterKnife.bind(this)
        val toolbar = findViewById<Toolbar>(R.id.app_toolbar)
        setSupportActionBar(toolbar)
        title = "My Agenda"
        createDrawer(toolbar, repository, true, 9)
        Utilz.addFragmentToActivity(supportFragmentManager, AgendaFragment(), R.id.activity_agenda_container)
    }

    override fun onBackPressed() {
        val c = supportFragmentManager.backStackEntryCount
        if (c > 0)
            supportFragmentManager.popBackStack()
        else
            super.onBackPressed()
    }


}