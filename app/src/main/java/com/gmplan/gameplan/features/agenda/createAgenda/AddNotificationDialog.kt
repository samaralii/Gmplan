package com.gmplan.gameplan.features.agenda.createAgenda

import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Spinner
import butterknife.*
import com.gmplan.gameplan.NewDefaultSpinnerAdapter
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.EventsNotification
import com.gmplan.gameplan.util.tmsgError
import kotlinx.android.synthetic.main.dialog_addnotification.*

/**
 * Created by Sammie on 9/29/2017.
 */
class AddNotificationDialog: DialogFragment() {


    var listener: AddNotificationListener? = null
    var unbinder: Unbinder? = null
    var isFirstItem = false

    @BindView(R.id.duration)
    lateinit var durationSpn: Spinner


    companion object {

        private val BOOL = "bool"

        fun newInstance(isFirstItem: Boolean = false): AddNotificationDialog {
            val d = AddNotificationDialog()
            val b = Bundle()
            b.putBoolean(BOOL, isFirstItem)
            d.arguments = b
            return d
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.dialog_addnotification, container, false)
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isFirstItem = arguments!!.getBoolean(BOOL, false)
    }

    val duratioList = listOf("Minutes", "Hours", "Day", "Week")

    var duration = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        durationSpn.adapter = NewDefaultSpinnerAdapter(duratioList)
    }

    @OnItemSelected(R.id.duration)
    fun onDurationsSelected(pos: Int) {
        duration = duratioList[pos]
    }


    @OnClick(R.id.dialog_btnAdd)
    fun onAddClick() {

        var validate = true

        if (dialog_etNotification.text.isNullOrEmpty())
            validate = false



        if (!validate)
            context?.tmsgError("Fill All Fields")


        if (isFirstItem) {

            listener?.addFirstNotification(EventsNotification(dialog_etNotification.text.toString(), duration))


        } else {

            listener?.addNotification(EventsNotification(dialog_etNotification.text.toString(), duration))

        }

        dialog.dismiss()

    }


    @OnClick(R.id.dialog_btnCancel)
    fun onCancelClick() {
        dialog.dismiss()
        listener?.onCancelNClick()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            listener = context as AddNotificationListener
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    public interface AddNotificationListener {
        fun addNotification(event: EventsNotification)
        fun addFirstNotification(event: EventsNotification)
        fun onCancelNClick()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

}