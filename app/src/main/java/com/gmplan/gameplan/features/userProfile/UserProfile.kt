package com.gmplan.gameplan.features.userProfile

import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.BaseDrwrActivity
import com.gmplan.gameplan.EmptyPresenter
import com.gmplan.gameplan.EmptyView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.source.TaskRepository
import javax.inject.Inject

class UserProfile : BaseDrwrActivity<EmptyView, EmptyPresenter>() {
    override fun gifPresenter() = EmptyPresenter()


    @BindView(R.id.activity_profile_tvGrade)
    lateinit var tvGrade: TextView

    @BindView(R.id.activity_profile_tvUgpa)
    lateinit var tvUgpa: TextView

    @BindView(R.id.activity_profile_tvWgpa)
    lateinit var tvWgpa: TextView

    @BindView(R.id.activity_profile_tvClassRank)
    lateinit var tvClassRank: TextView

    @BindView(R.id.activity_profile_tvSatScore)
    lateinit var tvSatScore: TextView

    @BindView(R.id.activity_profile_tvActScore)
    lateinit var tvActScore: TextView

    @BindView(R.id.activity_profile_tvHighSchool)
    lateinit var tvHighScool: TextView

    @BindView(R.id.activity_profile_tvLikes)
    lateinit var tvLikes: TextView

    @BindView(R.id.activity_profile_tvDislikes)
    lateinit var tvDislikes: TextView

    @BindView(R.id.activity_profile_tvMusic)
    lateinit var tvMusic: TextView

    @BindView(R.id.activity_profile_tvTeams)
    lateinit var tvTeams: TextView

    @BindView(R.id.activity_profile_tvColleges)
    lateinit var tvColleges: TextView

    @BindView(R.id.activity_profile_tvUserName)
    lateinit var tvUserName: TextView


    override fun finishCurrentActivity() {
        finish()
    }

    @Inject
    lateinit var repository: TaskRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        gifApplication().component?.Inject(this)
        ButterKnife.bind(this)
        val toolbar = findViewById<Toolbar>(R.id.app_toolbar) as Toolbar
        setSupportActionBar(toolbar)
        title = "My Profile"
        createDrawer(toolbar, repository, true)
        init()
    }

    private fun init() {

        tvUserName.text = "About ${getUserData?.student?.firstName} ${getUserData?.student?.lastName}"


        tvGrade.text = getUserData?.student?.grade
        tvUgpa.text = getUserData?.student?.ugpa
        tvWgpa.text = getUserData?.student?.wgpa
        tvSatScore.text = getUserData?.student?.satScore
        tvActScore.text = getUserData?.student?.actScore
        tvClassRank.text = getUserData?.student?.classRank

        tvHighScool.text = getUserData?.student?.mySchool
        tvLikes.text = getUserData?.student?.myLike
        tvDislikes.text = getUserData?.student?.myDislike
        tvMusic.text = getUserData?.student?.myMusic
        tvTeams.text = getUserData?.student?.myTeam
        tvColleges.text = getUserData?.student?.myCollege


    }

}

