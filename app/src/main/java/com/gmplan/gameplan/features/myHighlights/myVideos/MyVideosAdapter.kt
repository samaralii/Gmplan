package com.gmplan.gameplan.features.myHighlights.myVideos

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.Video
import com.gmplan.gameplan.util.Utilz

/**
 * Created by Sammie on 5/23/2017.
 */
class MyVideosAdapter(val data: MutableList<Video>, val listener: Listener, val context: Context) :
        RecyclerView.Adapter<MyVideosAdapter.MyVideoVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyVideoVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_myvideos, parent, false)
        return MyVideoVH(v)
    }

    override fun onBindViewHolder(holder: MyVideoVH, position: Int) {

        Glide.with(context)
                .load(Utilz.extractImgUrl(data[position].url))
                .dontAnimate()
                .into(holder.image)

        holder.title.text = data[position].name

        holder.let {

            it.tvOption.setOnClickListener { _->

                val popup = PopupMenu(context, it.tvOption)
                popup.inflate(R.menu.options_menu)

                popup.setOnMenuItemClickListener {

                    when(it.itemId){
                        R.id.menu1 -> listener.onEditItem(data[position].id, data[position].url, data[position].name)
                        R.id.menu2 -> listener.onDeleteItem(data[position].id, position)
                    }

                    false
                }

                popup.show()

            }


        }


    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun removeAt(position: Int) {
        data.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, data.size)
    }


    inner class MyVideoVH(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_myvideos_thumnail)
        lateinit var image: ImageView

        @BindView(R.id.list_myvieos_tvTitle)
        lateinit var title: TextView

        @BindView(R.id.textViewOptions)
        lateinit var tvOption: View

        init {
            ButterKnife.bind(this, v)
        }

//        @OnClick(R.id.list_myvideos_delete)
//        fun onDeleteItem() {
//            listener.onDeleteItem(data[layoutPosition].id, layoutPosition)
//        }
//
//        @OnClick(R.id.list_myvideos_edit)
//        fun onEditItem() {
//            listener.onEditItem(data[layoutPosition].id, data[layoutPosition].url, data[layoutPosition].name)
//        }

    }

    interface Listener {
        fun onDeleteItem(id: String, pos: Int)
        fun onEditItem(id: String, url: String, name: String)
    }

}