package com.gmplan.gameplan.features.dashboard

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.staffFeed.StaffNewssFeed
import com.gmplan.gameplan.util.AppConst
import com.pixplicity.htmlcompat.HtmlCompat


class FeedAdapter(val data: MutableList<StaffNewssFeed>, val context: Context) : RecyclerView.Adapter<FeedAdapter.PostsViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_posts, parent, false)
        return PostsViewHolder(v)
    }

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        val fromHtml = HtmlCompat.fromHtml(context, data[position].detail.toString(), 0)
        holder.tvDetail.movementMethod = LinkMovementMethod.getInstance()
        holder.tvDetail.text = fromHtml

        holder.tvCollegeName.text = data[position].collegename
        holder.tvUserName.text = data[position].user



        if (data[position].image == null) {

            holder.IvImage.visibility = View.GONE

        } else {

            holder.IvImage.visibility = View.VISIBLE

            Glide.with(context)
                    .load("${AppConst.BASE_URL_FEED_IMG}${data[position].image}")
                    .dontAnimate()
                    .into(holder.IvImage)


        }


    }

    override fun getItemCount(): Int {
        return data.size
    }


    inner class PostsViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_posts_like)
        lateinit var like: ImageView

        @BindView(R.id.list_posts_detail)
        lateinit var tvDetail: TextView

        @BindView(R.id.list_posts_collegeName)
        lateinit var tvCollegeName: TextView

        @BindView(R.id.list_posts_userName)
        lateinit var tvUserName: TextView

        @BindView(R.id.list_posts_image)
        lateinit var IvImage: ImageView

        init {
            ButterKnife.bind(this, v)
        }

    }
}
