package com.gmplan.gameplan.features.myHighlights

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.gmplan.gameplan.features.myHighlights.myVideos.MyVideosFragment
import com.gmplan.gameplan.features.myHighlights.myachievements.MyAchievFragment

/**
 * Created by Sammie on 5/22/2017.
 */
class FragmentPagerAdapter(fmanager: FragmentManager, val itemsCount: Int): FragmentPagerAdapter(fmanager) {

    override fun getItem(position: Int): Fragment? {
        return when(position) {
            0 -> MyAchievFragment.getInstance( )
            1 -> MyVideosFragment.getInstance()
            else -> {
                null
            }
        }
    }

    override fun getCount(): Int {
        return itemsCount
    }


}