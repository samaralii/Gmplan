package com.gmplan.gameplan.features.agenda.agendaFragment

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.*
import com.gmplan.gameplan.data.source.TaskDataSource
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby.mvp.MvpView
import io.reactivex.observers.DisposableSingleObserver
import java.util.*

class AgendaPresenter(private val repo: TaskDataSource, private val rxSchedulers: RxSchedulers) : MvpBasePresenter<AgendaView>() {


    fun getEvents(id: String, isStudent: Boolean) {

        if (isStudent)
            getStudentEvents(id)
        else
            getStaffEvents(id)

    }

    private fun getStaffEvents(id: String) {
        repo.getStaffEvents(id)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.ui())
                .doFinally { if (isViewAttached) view?.dismissDialog() }
                .subscribeWith(object : DisposableSingleObserver<EventPojo>() {
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        if (isViewAttached) view?.onErrorGettingEvents()
                    }

                    override fun onSuccess(t: EventPojo) {

                        try {

                            if (t.response == "success") {

                                if (t.events.isNotEmpty()) {

                                    setEvents(t.events)

                                } else {
                                    if (isViewAttached) view?.noEventsFound()

                                }

                            } else {
                                if (isViewAttached) view?.onErrorGettingEvents()
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (isViewAttached) view?.onErrorGettingEvents()
                        }

                    }
                })
    }

    private fun getStudentEvents(id: String) {
        repo.getStudentEvents(id)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.ui())
                .doFinally { if (isViewAttached) view?.dismissDialog() }
                .subscribeWith(object : DisposableSingleObserver<EventPojo>() {
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        if (isViewAttached) view?.onErrorGettingEvents()
                    }

                    override fun onSuccess(t: EventPojo) {

                        try {

                            if (t.response == "success") {

                                if (t.events.isNotEmpty()) {

                                    setEvents(t.events)

                                } else {
                                    if (isViewAttached) view?.noEventsFound()

                                }

                            } else {
                                if (isViewAttached) view?.onErrorGettingEvents()
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (isViewAttached) view?.onErrorGettingEvents()
                        }

                    }
                })
    }

    private fun setEvents(events: List<Event>) {

        val dateList = arrayOfNulls<String>(events.size)

        for (i in 0 until events.size) {
            val dateTime = events[i].event.eventStartDate.split(" ")
            val date = dateTime[0].split("-")
            dateList[i] = "${date[0]}-${(date[1]).toInt() - 1}-${date[2]}"
        }

        if (isViewAttached) view?.setEvenListeners(dateList, events)

        for (i in 0 until events.size) {

            val dateTime = events[i].event.eventStartDate.split(" ")
            val date = dateTime[0].split("-")

            val calendar = Calendar.getInstance()
            calendar.set(Calendar.YEAR, date[0].toInt())
            calendar.set(Calendar.MONTH, date[1].toInt() - 1)
            calendar.set(Calendar.DAY_OF_MONTH, date[2].toInt())

            if (isViewAttached) view?.setEventToCalendar(calendar.time)


        }

    }

    fun getChecklist(eventId: String) {
        repo.getCheckList(eventId)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.ui())
                .doFinally { if (isViewAttached) view?.dismissDialog() }
                .subscribeWith(object : DisposableSingleObserver<CheckLists>() {
                    override fun onSuccess(t: CheckLists) {
                        try {

                            if (t.response == "success") {

                                if (t.checklist.isNotEmpty()) {

                                    if (t.checklist.size == 1) {
                                        if (isViewAttached) view?.openSingleEventDialog(t.checklist[0])
                                    } else if (t.checklist.size > 1) {
                                        if (isViewAttached) view?.openMultipleEventDialog(t.checklist)
                                    }

                                }

                            } else {
                                if (isViewAttached) view?.onErrorGettingChecklist()
                            }


                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (isViewAttached) view?.onErrorGettingChecklist()
                        }
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        if (isViewAttached) view?.onErrorGettingChecklist()
                    }

                })
    }

    fun getEventByDate(date: String, studentId: String) {
        repo.getEventByDate(studentId, date)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.ui())
                .doFinally { if (isViewAttached) view?.dismissDialog() }
                .subscribeWith(object : DisposableSingleObserver<EventByDate>() {
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        if (isViewAttached) view?.onErrorGettingEventsByDate()

                    }

                    override fun onSuccess(t: EventByDate) {
                        try {

                            if (t.response == "success") {

                                if (isViewAttached) view?.EventsByDate(t.events)

                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (isViewAttached) view?.onErrorGettingEventsByDate()
                        }
                    }

                })

    }


    fun getStaffEventByDate(date: String, staffId: String) {
        repo.getStaffEventByDate(staffId, date)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.ui())
                .doFinally { if (isViewAttached) view?.dismissDialog() }
                .subscribeWith(object : DisposableSingleObserver<EventByDate>() {
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        if (isViewAttached) view?.onErrorGettingEventsByDate()

                    }

                    override fun onSuccess(t: EventByDate) {
                        try {

                            if (t.response == "success") {

                                if (isViewAttached) view?.EventsByDate(t.events)

                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (isViewAttached) view?.onErrorGettingEventsByDate()
                        }
                    }

                })

    }


}


interface AgendaView : MvpView {
    fun dismissDialog()
    fun onErrorGettingEvents()
    fun noEventsFound()
    fun setEventToCalendar(time: Date?)
    fun setEvenListeners(dateList: Array<String?>, events: List<Event>)
    fun onErrorGettingChecklist()
    fun openSingleEventDialog(checkList: CheckList)
    fun openMultipleEventDialog(checklist: ArrayList<CheckList>)
    fun onErrorGettingEventsByDate()
    fun EventsByDate(events: ArrayList<EventsDes>)
}