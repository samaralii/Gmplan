package com.gmplan.gameplan.features.staff.studentsProfiles.schoolListFragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.AllSchool
import com.gmplan.gameplan.data.pojos.School_
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.staff.studentsProfiles.StudentProfileActivity
import com.gmplan.gameplan.features.staff.studentsProfiles.studentListFragment.StudentProfileFragment
import com.gmplan.gameplan.util.Utilz
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.frag_school_list.*
import javax.inject.Inject

class SchoolListFragment : Fragment() {

    @Inject
    lateinit var taskRepository: TaskRepository

    var adapter = SchoolListAdapter(onItemClick = {
        openStudentFragment(it)
    })

    private fun openStudentFragment(data: School_) {
        val fragment = StudentProfileFragment.newInstance(data)
        val ft = fragmentManager!!.beginTransaction()
        ft.setCustomAnimations(
                R.anim.enter_from_right, R.anim.exit_to_left,
                R.anim.enter_from_left, R.anim.exit_to_right)
        ft.replace(R.id.activity_sp_container, fragment)
        ft.addToBackStack(null)
        ft.commit()
    }

    companion object {
        fun newInstance() = SchoolListFragment().apply {
            arguments = Bundle().apply {

            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as StudentProfileActivity).gifApplication().component?.Inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.frag_school_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        school_list_rv.layoutManager = LinearLayoutManager(context)
        school_list_rv.adapter = adapter

        school_list_sr.setOnRefreshListener {
            refreshList()
        }

        refreshList()
    }

    fun refreshList() {
        getSchools()
    }

    private fun getSchools() {

        school_list_sr.isRefreshing = true

        val userData = (activity as StudentProfileActivity).getUserData

        taskRepository
                .getSchools(userData?.admissionStaff?.id.toString(),
                        userData?.admissionStaff?.collegeId.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { school_list_sr.isRefreshing = false }
                .subscribeWith(object : DisposableSingleObserver<List<AllSchool>>() {
                    override fun onSuccess(t: List<AllSchool>) {

                        try {
                            if (t.isNotEmpty()) {
                                adapter.addData(t as MutableList<AllSchool>)
                            } else {
                                Utilz.tmsgError(context, "No Data Found")
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            Utilz.tmsgError(context, "Error")
                        }

                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        Utilz.tmsgError(context, "Error")
                    }

                })


    }


}