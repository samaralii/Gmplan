package com.gmplan.gameplan.features.staff.myMessenger.searchStudent

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.staffStudent.StaffStudent

/**
 * Created by Sammie on 6/20/2017.
 */
class ComposeMessageDialog(val data: List<StaffStudent>, private val listener: DialogListener) : DialogFragment() {

    private var unbinder: Unbinder? = null

    @BindView(R.id.dialog_compose_message_etMessage)
    lateinit var etMessage: EditText

    @BindView(R.id.dialog_compose_message_etSubject)
    lateinit var etSubject: EditText

    @BindView(R.id.dialog_compose_message_etTO)
    lateinit var etTo: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.dialog_compose_message, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        etTo.isEnabled = false

        dialog.setTitle("Compose Message")

        val names = StringBuilder()

        for (s in data) {
            names.append("${s.student.firstName} ${s.student.lastName}").append(" ")
        }

        etTo.setText(names.toString())
    }

    @OnClick(R.id.dialog_compose_message_btnSend)
    fun onSendClick() {


        if (etMessage.text.isEmpty()) {
            etMessage.error = "Required Field"
            return
        } else
            etMessage.error = null

        if (etSubject.text.isEmpty()) {
            etSubject.error = "Required Field"
            return
        } else
            etSubject.error = null


        val studentIds = StringBuilder()

        val length = data.size - 1

        for (i in data.indices) {
            studentIds.append(data[i].student.id)
            if (i < length) {
                studentIds.append(",")
            }
        }

        listener.onSendClick(studentIds.toString(), etSubject.text.toString(), etMessage.text.toString(), dialog)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

    interface DialogListener {
        fun onSendClick(to: String, subject: String, message: String, dialog: Dialog)
    }
}