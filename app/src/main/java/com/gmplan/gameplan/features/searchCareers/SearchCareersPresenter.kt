package com.gmplan.gameplan.features.searchCareers

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.GetCareersAndStems
import com.gmplan.gameplan.data.source.TaskDataSource
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sammie on 5/3/2017.
 */
class SearchCareersPresenter(private val repo: TaskDataSource, private val schedulers: RxSchedulers) : MvpBasePresenter<SearchCareersView>() {


    private val disposable = CompositeDisposable()


    fun getCareers() {
        disposable.add(repo.getCareersAndStems()
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally { if (isViewAttached) view?.dismissProgress() }
                .subscribeWith(object : DisposableSingleObserver<GetCareersAndStems>() {
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        if (isViewAttached) view?.onErrorGettingCareers()
                    }

                    override fun onSuccess(t: GetCareersAndStems) {


                        if (t.careers != null) {
                            if (t.careers.isNotEmpty()) {
                                if (isViewAttached) view?.industryCareerList(t.careers)
                            }
                        }


                        if (t.stemCareers != null) {
                            if (t.stemCareers.isNotEmpty()) {
                                if (isViewAttached) view?.stemCareerList(t.stemCareers)
                            }
                        }


                    }
                }))
    }


    override fun detachView(retainInstance: Boolean) {
        super.detachView(retainInstance)
        if (!retainInstance) disposable.clear()
    }

}