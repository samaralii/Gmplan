package com.gmplan.gameplan.features.staff.studentsProfiles.searchDialogs

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Spinner
import butterknife.*
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.StudentsCareers
import com.gmplan.gameplan.features.staff.studentsProfiles.studentListFragment.CareerSpinnerAdapter

/**
 * Created by Sammie on 6/6/2017.
 */
class SearchStudentsByCareerDialog(private val careerList: List<StudentsCareers>, private val listener: DialogListener) : DialogFragment() {

    var unbinder: Unbinder? = null

    @BindView(R.id.frag_search_student_career_spinner)
    lateinit var spnCareer: Spinner

    var career: StudentsCareers? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.frag_search_student_career, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        dialog.setTitle("Search By Career")

        spnCareer.adapter = CareerSpinnerAdapter(careerList)
    }

    @OnItemSelected(R.id.frag_search_student_career_spinner)
    fun onitemSelected(pos: Int) {
        career = careerList[pos]
    }

    @OnClick(R.id.frag_search_student_career_btnSearch)
    fun onSearchClick() {
        listener.onItemClick(career, dialog)
    }

    @OnClick(R.id.frag_search_student_career_btnBack)
    fun onBackClick() {
        dialog.dismiss()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

    interface DialogListener {
        fun onItemClick(career: StudentsCareers?, dialog: Dialog)
    }

}