package com.gmplan.gameplan.features.dashboard

import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.BaseDrwrActivity
import com.gmplan.gameplan.EmptyPresenter
import com.gmplan.gameplan.EmptyView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.dashboard.items.DashboardFragment
import com.gmplan.gameplan.infrastructure.GmplanApplication
import com.gmplan.gameplan.util.Utilz
import javax.inject.Inject

/**
 * Created by Sammie on 6/5/2017.
 */
class StaffDashboardActivity : BaseDrwrActivity<EmptyView, EmptyPresenter>() {
    override fun gifPresenter() = EmptyPresenter()

    @BindView(R.id.app_toolbar)
    lateinit var mToolbar: Toolbar

    @BindView(R.id.staff_activity_dashboard_staff_name)
    lateinit var staffName: TextView

    @Inject
    lateinit var repoo: TaskRepository

//    private val userPojo by lazy { repoo.getUserData(AppConst.USER_DATA) }

    override fun finishCurrentActivity() {
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.staff_activity_dashboard)
        (application as GmplanApplication).component?.Inject(this)
        ButterKnife.bind(this)

        setSupportActionBar(mToolbar)
        val itemTitle = "Dashboard"
        title = itemTitle

        createDrawer(mToolbar, repoo, true, 1)
        init()
    }

    private fun init() {

        staffName.text = getUserData?.admissionStaff?.name


        Utilz.addFragmentToActivity(supportFragmentManager,
                DashboardFragment(), R.id.staff_activity_dashboard_container)

    }


}