package com.gmplan.gameplan.features.staff.myMessenger

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import com.gmplan.gameplan.BaseDrwrActivity
import com.gmplan.gameplan.EmptyPresenter
import com.gmplan.gameplan.EmptyView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.staff.myMessenger.inbox.InboxFragment
import com.gmplan.gameplan.features.staff.myMessenger.outbox.OutboxFragment
import com.gmplan.gameplan.util.Utilz
import kotlinx.android.synthetic.main.activity_messegner.*
import javax.inject.Inject

class MyMessengerActivity : BaseDrwrActivity<EmptyView, EmptyPresenter>() {

    override fun gifPresenter() = EmptyPresenter()

    override fun finishCurrentActivity() {
        finish()
    }

    @Inject
    lateinit var repository: TaskRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_messegner)
        gifApplication().component?.Inject(this)
        val toolbar = findViewById<Toolbar>(R.id.app_toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "My Messenger"
        createDrawer(toolbar, repository, true, 8)


//        Utilz.addFragmentToActivity(supportFragmentManager, SearchStudentFragment(), R.id.activity_messenger_container)

        messenger_tabLlayout.apply {
            addTab(newTab().setText("Incoming"))
            addTab(newTab().setText("Out going"))
            addTab(newTab().setText("Drafts"))
            addTab(newTab().setText("Trash"))
            tabGravity = TabLayout.GRAVITY_FILL
        }

        val adapter = FragmentPagesAdapter(supportFragmentManager, messenger_tabLlayout.tabCount)
        messenger_pager.adapter = adapter

        messenger_tabLlayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                messenger_pager.currentItem = tab?.position as Int
            }

        })

        messenger_add.setOnClickListener { startActivity(Intent(this, ComposeActivity::class.java)) }


    }



    class FragmentPagesAdapter(fragmentManager: FragmentManager, val itemCount: Int) : FragmentPagerAdapter(fragmentManager) {

        override fun getItem(position: Int): Fragment? {
            return when (position) {
                0 -> InboxFragment.newInstance()
                1 -> OutboxFragment.newInstance()
                2 -> InboxFragment.newInstance()
                3 -> InboxFragment.newInstance()
                else -> null
            }
        }

        override fun getCount(): Int {
            return itemCount
        }

    }

}