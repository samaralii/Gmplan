package com.gmplan.gameplan.features.myColleges.myCollegeListFragment

import com.gmplan.gameplan.data.pojos.College
import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Sammie on 5/18/2017.
 */
interface MyCollegeView: MvpView {
    fun dismissDialog()
    fun onFailed()
    fun collegeList(colleges: List<College>)
    fun onNoCollegeFound()
}