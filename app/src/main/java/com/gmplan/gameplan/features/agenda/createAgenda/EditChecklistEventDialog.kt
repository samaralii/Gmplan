package com.gmplan.gameplan.features.agenda.createAgenda

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.CheckListsEvents
import com.gmplan.gameplan.util.tmsgError
import kotlinx.android.synthetic.main.dialog_addchecklist.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Sammie on 9/29/2017.
 */
class EditChecklistEventDialog : DialogFragment() {

    var listener: EditChecklistListener? = null
    var unbinder: Unbinder? = null
    var event: CheckListsEvents? = null
    var pos: Int? = null

    companion object {

        private val DATA = "data"
        private val POS = "pos"

        fun newInstance(event: CheckListsEvents, pos: Int): EditChecklistEventDialog {
            val d = EditChecklistEventDialog()
            val b = Bundle()
            b.putParcelable(DATA, event)
            b.putInt(POS, pos)
            d.arguments = b
            return d
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater!!.inflate(R.layout.dialog_editchecklist, container, false)
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        event = arguments!!.getParcelable(DATA)
        pos = arguments!!.getInt(POS)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val startDateTime = event?.startDateTime?.split(" ")
        val endDateTime = event?.endDateTime?.split(" ")

        dialog_etEvent.setText(event?.event)
        dialog_etStartDate.setText(startDateTime?.get(0))
        dialog_etStartTime.setText(startDateTime?.get(1))
        dialog_etEndDate.setText(endDateTime?.get(0))
        dialog_etEndTime.setText(endDateTime?.get(1))

    }


    @OnClick(R.id.dialog_btnEdit)
    fun onAddClick() {

        var validate = true

        if (dialog_etEvent.text.isNullOrEmpty())
            validate = false

        if (dialog_etStartDate.text.isNullOrEmpty())
            validate = false

        if (dialog_etStartTime.text.isNullOrEmpty())
            validate = false

        if (dialog_etEndDate.text.isNullOrEmpty())
            validate = false

        if (dialog_etEndTime.text.isNullOrEmpty())
            validate = false


        if (!validate)
            context?.tmsgError("Fill All Fields")


        pos?.let {
            listener?.editEvent(CheckListsEvents(
                    dialog_etEvent.text.toString(),
                    "${dialog_etStartDate.text} ${dialog_etStartTime.text}",
                    "${dialog_etEndDate.text} ${dialog_etEndTime.text}"
            ), it)
        }


        dialog.dismiss()

    }


    @OnClick(R.id.dialog_etStartDate)
    fun onStartDateClick(et: EditText) {

        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)

        val startdate = DatePickerDialog(context,
                { _, year, month, day ->
                    val builder = StringBuilder()
                    builder.append(day.toString() + "-")
                    builder.append((month + 1).toString() + "-")
                    builder.append(year)

                    et.setText(builder.toString())
                },
                mYear,
                mMonth,
                mDay)
        startdate.show()

    }

    @OnClick(R.id.dialog_etStartTime)
    fun onStartTimeClick(et: EditText) {

        val timePicker = TimePickerDialog(context, { _, v1, v2 ->

            val time = "$v1:$v2"

            try {
                val sdf = SimpleDateFormat("H:mm")
                val dateObj = sdf.parse(time)
                et.setText(SimpleDateFormat("K:mm a").format(dateObj))

            } catch (e: ParseException) {
                e.printStackTrace()
            }


        }, 0, 0, false)

        timePicker.show()

    }

    @OnClick(R.id.dialog_etEndDate)
    fun onEndDateClick(et: EditText) {

        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)

        val startdate = DatePickerDialog(context,
                { _, year, month, day ->
                    val builder = StringBuilder()
                    builder.append(day.toString() + "-")
                    builder.append((month + 1).toString() + "-")
                    builder.append(year)

                    et.setText(builder.toString())
                },
                mYear,
                mMonth,
                mDay)
        startdate.show()

    }

    @OnClick(R.id.dialog_etEndTime)
    fun onEndTimeClick(et: EditText) {

        val timePicker = TimePickerDialog(context, { _, v1, v2 ->

            val time = "$v1:$v2"

            try {
                val sdf = SimpleDateFormat("H:mm")
                val dateObj = sdf.parse(time)
                et.setText(SimpleDateFormat("K:mm a").format(dateObj))

            } catch (e: ParseException) {
                e.printStackTrace()
            }


        }, 0, 0, false)

        timePicker.show()

    }


    @OnClick(R.id.dialog_btnCancel)
    fun onCancelClick() {
        dialog.dismiss()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            listener = context as EditChecklistListener
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    public interface EditChecklistListener {
        fun editEvent(event: CheckListsEvents, pos: Int)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

}