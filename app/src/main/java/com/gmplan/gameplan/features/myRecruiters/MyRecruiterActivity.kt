package com.gmplan.gameplan.features.myRecruiters

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.BaseDrwrActivity
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.Recruiter
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.infrastructure.GmplanApplication
import com.gmplan.gameplan.util.AppConst
import com.gmplan.gameplan.util.Utilz
import javax.inject.Inject

/**
 * Created by Sammie on 5/4/2017.
 */

class MyRecruiterActivity : BaseDrwrActivity<MyRecruiterView, MyRecruiterPresenter>(), MyRecruiterView {

    @Inject
    lateinit var taskRepository: TaskRepository

    @Inject
    lateinit var shedulers: RxSchedulersImpl

    lateinit var dialog: ProgressDialog


    override fun gifPresenter(): MyRecruiterPresenter {
        (application as GmplanApplication).component?.Inject(this)
        return MyRecruiterPresenter(taskRepository, shedulers)
    }


    override fun finishCurrentActivity() {
        finish()
    }


    @BindView(R.id.recruiter_list)
    lateinit var recyclerView: RecyclerView

    @BindView(R.id.app_toolbar)
    lateinit var toolbar: Toolbar

    @BindView(R.id.notFound)
    lateinit var notFound: TextView

    @BindView(R.id.activity_myrecruiters_sr)
    lateinit var swipeRefresh: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_myrecruiters)
        ButterKnife.bind(this)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "My Recruiters"
        createDrawer(toolbar, taskRepository, true, 5)
        init()
    }

    fun init() {

        dialog = Utilz.dialog(this)

        recyclerView.layoutManager = LinearLayoutManager(this)
        refreshList()

        swipeRefresh.setOnRefreshListener { refreshList() }
    }


    fun refreshList() {

        dialog.show()
        presenter.getRecruiters(getUserData?.student?.id.toString())

    }

    override fun onNoRecruiterFound() {
        recyclerView.visibility = View.GONE
        notFound.visibility = View.VISIBLE
    }

    override fun onSuccess(recruiters: MutableList<Recruiter>) {
        recyclerView.adapter = RecruiterAdapter(recruiters)
        recyclerView.visibility = View.VISIBLE
        notFound.visibility = View.GONE
    }

    override fun onError() {
        recyclerView.visibility = View.GONE
        notFound.visibility = View.VISIBLE
        Utilz.tmsgError(this, "Error")
    }

    override fun dismissProgress() {
        dialog.dismiss()

        if (swipeRefresh.isRefreshing)
            swipeRefresh.isRefreshing = false
    }


}
