package com.gmplan.gameplan.features.agenda

import com.gmplan.gameplan.data.pojos.CheckListsEvents
import com.gmplan.gameplan.data.pojos.EventsNotification

/**
 * Created by Sammie on 10/4/2017.
 */

data class CreateAgendaStudent(var studentName: String, var isCheck: Boolean)

data class CreateAgendaJson(var eventName: String? = null, var eventStartDateTime: String? = null,
                            var eventEndDateTime: String? = null, var notificationList: List<EventsNotification>? = null,
                            var checklist: List<CheckListsEvents>? = null, var color: String? = null,
                            var student_id: String? = null, var studentsIdList: List<String>? = null)