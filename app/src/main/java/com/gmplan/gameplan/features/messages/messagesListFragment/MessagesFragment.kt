package com.gmplan.gameplan.features.messages.messagesListFragment

import android.app.Dialog
import android.app.ProgressDialog
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.ShortMessageDetail
import com.gmplan.gameplan.data.pojos.detailMessagePojos.Messages
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.messages.MessagesActivity
import com.gmplan.gameplan.util.Utilz
import com.hannesdorfmann.mosby.mvp.MvpFragment
import javax.inject.Inject

/**
 * Created by Sammie on 5/25/2017.
 */
class MessagesFragment : MvpFragment<MessagesView, MessagesPresenter>(), MessagesView, MessagesAdapter.Listener, MessageDetailDialog.DialogListener {

    @Inject
    lateinit var schedulers: RxSchedulersImpl

    @Inject
    lateinit var taskRepository: TaskRepository

    var unbinder: Unbinder? = null


    @BindView(R.id.frag_messages_sr)
    lateinit var swipeRefresh: SwipeRefreshLayout

    @BindView(R.id.frag_messages_recycler)
    lateinit var recyclerView: RecyclerView

    var pd: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.frag_message, container, false) as View
        ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

    private fun init() {

        recyclerView.layoutManager = LinearLayoutManager(context)
        pd = Utilz.dialog(context!!)

        pd?.show()
        presenter.getMessagesList((activity as MessagesActivity).getUserData?.student?.id.toString())


        swipeRefresh.setOnRefreshListener {

            pd?.show()
            presenter.getMessagesList((activity as MessagesActivity).getUserData?.student?.id.toString())

        }

    }


    override fun createPresenter(): MessagesPresenter {
        (activity as MessagesActivity).gifApplication().component?.Inject(this)
        return MessagesPresenter(taskRepository, schedulers)
    }

    override fun dismissDialog() {

        pd?.let {

            if (it.isShowing)
                it.dismiss()

        }

        if (swipeRefresh.isRefreshing)
            swipeRefresh.isRefreshing = false

    }

    override fun onFailedGettingList() {
        Utilz.tmsgError(context!!, "Error")
    }

    override fun onNoMessagesFound() {
        Utilz.tmsgError(context!!, "No Messages Found")
    }

    override fun onSuccessMessages(messages: MutableList<ShortMessageDetail>) {
        recyclerView.adapter = MessagesAdapter(messages, this)
    }

    override fun onItemClick(id: String) {
//        Utilz.tmsg(context, id)

        pd?.show()
        presenter.messageDetail(id)
    }

    override fun onFailedGettingDetail() {
        Utilz.tmsgError(context!!, "Error")
    }

    override fun onSuccessMessageDetail(messages: Messages) {

        val dialog = MessageDetailDialog(this, messages)
        dialog.show(fragmentManager, "tag")

    }

    override fun onReplyClick(dialog: Dialog, reply: String, message: Messages?) {
        dialog.dismiss()

        message?.let {

            pd?.show()
            presenter.replyMessage(
                    (activity as MessagesActivity).getUserData?.student?.id.toString(),
                    it.collegeMessage.messageId,
                    reply, reply, it.collegeMessage.collegeId.toString(),
                    (activity as MessagesActivity).getUserData?.student?.userId.toString(),
                    it.admissionStaff?.id.toString())
        }

    }

    override fun onSuccessReply() {
        Utilz.tmsgSuccess(context!!, "Success")
    }

    override fun onFailedReply() {
        Utilz.tmsgError(context!!, "Error")
    }


}