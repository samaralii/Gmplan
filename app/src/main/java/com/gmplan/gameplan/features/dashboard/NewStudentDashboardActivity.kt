package com.gmplan.gameplan.features.dashboard

import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v7.widget.Toolbar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.gmplan.gameplan.BaseDrwrActivity
import com.gmplan.gameplan.EmptyPresenter
import com.gmplan.gameplan.EmptyView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.userPojo.UserData
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.dashboard.items.DashboardFragment
import com.gmplan.gameplan.infrastructure.GmplanApplication
import com.gmplan.gameplan.util.Utilz
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*
import javax.inject.Inject

class NewStudentDashboardActivity : BaseDrwrActivity<EmptyView, EmptyPresenter>() {
    override fun gifPresenter() = EmptyPresenter()


    @BindView(R.id.toolbar)
    lateinit var mToolbar: Toolbar

    @BindView(R.id.activity_dashboard_ugpa)
    lateinit var tvUgpa: TextView

    @BindView(R.id.activity_dashboard_wgpa)
    lateinit var tvWgpa: TextView

    @BindView(R.id.activity_dashboard_grade)
    lateinit var tvGrade: TextView

    @BindView(R.id.activity_dashboard_sat)
    lateinit var tvSat: TextView

    @BindView(R.id.activity_dashboard_act)
    lateinit var tvAct: TextView

    @BindView(R.id.activity_dashboard_classRank)
    lateinit var tvClassRank: TextView

    @BindView(R.id.activity_dashboard_profilePic)
    lateinit var profilePicture: CircleImageView

    @BindView(R.id.collapsing_toolbar)
    lateinit var collapsingToolbarLayout: CollapsingToolbarLayout


    @Inject
    lateinit var repoo: TaskRepository

    override fun finishCurrentActivity() {
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_student_dashboard)
        ButterKnife.bind(this)
        (application as GmplanApplication).component?.Inject(this)
        init()
    }

    fun init() {

        setSupportActionBar(mToolbar)

        val itemTitle = ""

        collapsingToolbarLayout.title = itemTitle
        collapsingToolbarLayout.setExpandedTitleColor(resources.getColor(android.R.color.transparent))

        createDrawer(mToolbar, repoo, true, 1)
        Utilz.addFragmentToActivity(supportFragmentManager,
                DashboardFragment(), R.id.activity_dashboard_container)

        bindDataWithViews()

    }

    private fun bindDataWithViews() {

        tvGrade.text = getUserData?.student?.grade
        tvUgpa.text = getUserData?.student?.ugpa
        tvWgpa.text = getUserData?.student?.wgpa
        tvSat.text = getUserData?.student?.satScore
        tvAct.text = getUserData?.student?.actScore
        tvClassRank.text = getUserData?.student?.classRank


        try {

            val img_url: String = Utilz.getImgUrl(getUserData as UserData)

            Glide.with(this)
                    .load(img_url)
                    .placeholder(R.drawable.placeholder)
                    .dontAnimate()
                    .listener(object : RequestListener<String, GlideDrawable> {
                        override fun onException(e: java.lang.Exception?, model: String?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
                            android.util.Log.d("GLIDE", String.format(Locale.ROOT,
                                    "onException(%s, %s, %s, %s)", e, model, target, isFirstResource), e)
                            return false
                        }

                        override fun onResourceReady(resource: GlideDrawable?, model: String?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                            android.util.Log.d("GLIDE", String.format(Locale.ROOT,
                                    "onResourceReady(%s, %s, %s, %s, %s)", resource, model, target, isFromMemoryCache, isFirstResource));
                            return false
                        }

                    })
                    .into(profilePicture)


        } catch (e: Exception) {
            e.printStackTrace()
            Utilz.tmsgError(this, "Error loading profile Image")
        }


    }


}