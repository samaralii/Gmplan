package com.gmplan.gameplan.features.searchCareers.stemCareers

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos._Career

/**
 * Created by Sammie on 9/7/2017.
 */
class StemCareersAdapter(private val data: MutableList<_Career>, private val listener: ListListener) : RecyclerView.Adapter<StemCareersAdapter.StemCareersVH>() {

    override fun onBindViewHolder(holder: StemCareersVH, pos: Int) {
        holder.title.text = data[pos].title

    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): StemCareersVH {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.list_industry_careers, p0, false)
        return StemCareersVH(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }


    inner class StemCareersVH(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_careersTitle)
        lateinit var title: TextView


        init {
            ButterKnife.bind(this, v)
        }

        @OnClick(R.id.list_delete)
        fun onDeleteClick() {
            listener.onAddClick(data[layoutPosition])
        }
    }

    interface ListListener {
        fun onAddClick(career: _Career)
    }
}