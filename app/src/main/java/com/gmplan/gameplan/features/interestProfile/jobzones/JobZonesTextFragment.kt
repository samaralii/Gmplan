package com.gmplan.gameplan.features.interestProfile.jobzones

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gmplan.gameplan.R

/**
 * Created by Sammie on 9/11/2017.
 */
class JobZonesTextFragment : Fragment() {

    companion object {
        fun newInstance(): JobZonesTextFragment {
            return JobZonesTextFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.frag_text_jobzone, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val text1 = "To focus your search, think about the following question:\n" +
                "\n" +
                "How much education, training, and experience do I need to do the job?\n" +
                "\n" +
                "Each O*NET career is in one of five Job Zones, which are groups of careers that need the same level of experience, education, and training.\n" +
                "\n" +
                "Different careers need different amounts of preparation. You will be asked to pick a Job Zone. Using your Job Zone and your interests, the Interest Profiler will help you identify and explore careers that might be right for you."

        val text2 = "When picking your Job Zone, you can choose your:\n" +
                "\n" +
                "Current Job Zone — choose the Job Zone that matches the kind of experience, education, and training you have now.\n" +
                "\n" +
                "OR\n" +
                "\n" +
                "Future Job Zone — choose the Job Zone that matches the amount of experience, education, and training you plan to get in the future.\n" +
                "\n" +
                "In both cases, your Job Zone will include careers that you might like to do.\n" +
                "\n" +
                "Don't worry about making the wrong choice; you can explore a different Job Zone later."


        (view?.findViewById<TextView>(R.id.text1))?.text = text1
        (view?.findViewById<TextView>(R.id.text2))?.text = text2

    }

}