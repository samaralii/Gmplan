package com.gmplan.gameplan.features.searchCareers.industryCareers

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.industryCareersSearch.Career
import com.gmplan.gameplan.data.pojos.industryCareersSearch.IndustryCareers
import com.gmplan.gameplan.data.source.TaskDataSource
import com.google.gson.JsonElement
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver

interface IndustryCareersView : MvpView {
    fun dismissProgress()
    fun onErrorGettingList()
    fun onSuccessGettingList(careers: List<Career>)
    fun onErrorAdding()
    fun SuccessAddedCareer()
}


class IndustryCareersPresenter(private val repo: TaskDataSource, private val schedulers: RxSchedulers) :
        MvpBasePresenter<IndustryCareersView>() {


    private val disposable = CompositeDisposable()

    fun getList(careerId: String, studentId: String) {
        disposable.add(repo.findIndustryCareers(careerId, studentId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally { if (isViewAttached) view?.dismissProgress() }
                .subscribeWith(object : DisposableSingleObserver<IndustryCareers>() {
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        if (isViewAttached) view?.onErrorGettingList()
                    }

                    override fun onSuccess(t: IndustryCareers) {

                        if (t.careers != null) {
                            if (t.careers.isNotEmpty()) {

                                if (isViewAttached) view?.onSuccessGettingList(t.careers)

                            } else {
                                if (isViewAttached) view?.onErrorGettingList()
                            }
                        } else {
                            if (isViewAttached) view?.onErrorGettingList()
                        }
                    }
                }))
    }

    fun getListByKeyword(keyword: String, studentId: String) {
        disposable.add(repo.searchCareerByKeyword(keyword, studentId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally { if (isViewAttached) view?.dismissProgress() }
                .subscribeWith(object : DisposableSingleObserver<IndustryCareers>() {
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        if (isViewAttached) view?.onErrorGettingList()
                    }

                    override fun onSuccess(t: IndustryCareers) {

                        if (t.careers != null) {
                            if (t.careers.isNotEmpty()) {

                                if (isViewAttached) view?.onSuccessGettingList(t.careers)

                            } else {
                                if (isViewAttached) view?.onErrorGettingList()
                            }
                        } else {
                            if (isViewAttached) view?.onErrorGettingList()
                        }
                    }
                }))
    }

    fun addCareer(careerCode: String, studentId: String, careerTitle: String) {
        disposable.add(repo.addCareer(careerCode, studentId, careerTitle)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally { if (isViewAttached) view?.dismissProgress() }
                .subscribeWith(object : DisposableSingleObserver<JsonElement>() {
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        if (isViewAttached) view?.onErrorAdding()
                    }

                    override fun onSuccess(t: JsonElement) {

                        try {

                            val obj = t.asJsonObject

                            val response = obj.get("response").asString

                            if (response == "success") {
                                if (isViewAttached) view?.SuccessAddedCareer()
                            } else {
                                if (isViewAttached) view?.onErrorAdding()
                            }



                        } catch (e: Exception) {
                            if (isViewAttached) view?.onErrorAdding()
                        }
                    }
                }))
    }


    override fun detachView(retainInstance: Boolean) {
        super.detachView(retainInstance)
        if (!retainInstance) disposable.clear()
    }


}
