package com.gmplan.gameplan.features.myRecruiters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.Recruiter

/**
 * Created by Sammie on 5/4/2017.
 */
class RecruiterAdapter(val data: List<Recruiter>) : RecyclerView.Adapter<RecruiterAdapter.RecruiterViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecruiterViewHolder {
        val v: View = LayoutInflater.from(parent.context).inflate(R.layout.list_recruiter, parent, false)
        return RecruiterViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecruiterViewHolder, position: Int) {

        holder.name.text = data[position].recruiter

    }


    override fun getItemCount(): Int {
        return data.size
    }


    class RecruiterViewHolder(val v: View?) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_recruiter_name)
        lateinit var name: TextView


        init {
            ButterKnife.bind(this, v as View)
        }

    }
}