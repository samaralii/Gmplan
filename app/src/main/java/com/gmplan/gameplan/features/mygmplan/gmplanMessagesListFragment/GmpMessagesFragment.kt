package com.gmplan.gameplan.features.mygmplan.gmplanMessagesListFragment

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.Gmessage
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.mygmplan.MyGmplanActivity
import com.gmplan.gameplan.util.Utilz
import com.hannesdorfmann.mosby.mvp.MvpFragment
import javax.inject.Inject

/**
 * Created by Sammie on 5/26/2017.
 */
class GmpMessagesFragment : MvpFragment<GmplanMessagesView, GmplanMessagesPresenter>(), GmplanMessagesView, GmplanListAdapter.Listener {

    @Inject
    lateinit var schedulers: RxSchedulersImpl

    @Inject
    lateinit var taskRepository: TaskRepository

    @BindView(R.id.frag_gmplan_recyclerView)
    lateinit var recyclerView: RecyclerView

    @BindView(R.id.frag_gmplan_sr)
    lateinit var swipeRefresh: SwipeRefreshLayout

    var pd: ProgressDialog? = null

    override fun createPresenter(): GmplanMessagesPresenter {
        (activity as MyGmplanActivity).gifApplication().component?.Inject(this)
        return GmplanMessagesPresenter(taskRepository, schedulers)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pd = Utilz.dialog(context!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.frag_gmplan, container, false) as View
        ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        recyclerView.layoutManager = LinearLayoutManager(context)


        pd?.show()
        presenter.getGmpMessages((activity as MyGmplanActivity).getUserData?.student?.id.toString())


        swipeRefresh.setOnRefreshListener {


            pd?.show()
            presenter.getGmpMessages((activity as MyGmplanActivity).getUserData?.student?.id.toString())

        }



    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun dismissDialog() {

        pd?.let {
            if (it.isShowing)
                it.dismiss()
        }

        if (swipeRefresh.isRefreshing)
            swipeRefresh.isRefreshing = false
    }

    override fun onFailedGettingMessages() {
        Utilz.tmsgError(context, "Error")
    }

    override fun onNoMessagesFound() {
        Utilz.tmsgError(context, "No Messages Found")
    }

    override fun onSuccessfullyMessagesFound(gmessages: List<Gmessage>) {
        recyclerView.adapter = GmplanListAdapter(gmessages, this)

    }

    override fun onItemClick(gmessage: Gmessage) {
        val dialog = GmpMsgsDetailDialog(gmessage.body)
        dialog.show(fragmentManager, "tag")
    }



}