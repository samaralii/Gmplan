package com.gmplan.gameplan.features.myCareers.myCareerListFragment

import com.gmplan.gameplan.data.pojos.Career
import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Sammie on 5/18/2017.
 */
interface MyCareerView : MvpView {
    fun onFailed()
    fun onNoCareerFound()
    fun careerList(careers: List<Career>)
    fun dismissDialog()
    fun onFailedDeleteCareer()
    fun careerDeleted(pos: Int)
}