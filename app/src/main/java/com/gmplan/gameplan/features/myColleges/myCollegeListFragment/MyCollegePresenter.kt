package com.gmplan.gameplan.features.myColleges.myCollegeListFragment

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.UserColleges
import com.gmplan.gameplan.data.source.TaskDataSource
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sammie on 5/18/2017.
 */
class MyCollegePresenter(val repo: TaskDataSource, val schedulers: RxSchedulers): MvpBasePresenter<MyCollegeView>() {



    fun getUserColleges(studentId: String) {
        repo.getUserColleges(studentId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<UserColleges>() {
                    override fun onError(p0: Throwable) {


                        if (isViewAttached)
                            view?.onFailed()


                    }

                    override fun onSuccess(userCollege: UserColleges) {

                        if (userCollege.response == "success") {

                            if (userCollege.colleges == null || userCollege.colleges.isEmpty()) {


                                if (isViewAttached)
                                    view?.onNoCollegeFound()

                            } else {

                                if (isViewAttached)
                                    view?.collegeList(userCollege.colleges)

                            }

                        } else {

                            if (isViewAttached)
                                view?.onFailed()

                        }

                    }
                })
    }

}