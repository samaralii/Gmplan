package com.gmplan.gameplan.features.searchCareers

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Spinner
import butterknife.*
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos._Career
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.searchCareers.industryCareers.IndustryCareersFragment
import com.gmplan.gameplan.features.searchCareers.stemCareers.StemCareerFragment
import com.gmplan.gameplan.util.Utilz
import com.gmplan.gameplan.util.tmsg
import com.gmplan.gameplan.util.validate
import com.hannesdorfmann.mosby.mvp.MvpFragment
import javax.inject.Inject

/**
 * Created by Sammie on 9/6/2017.
 */
class SearchCareersFragment : MvpFragment<SearchCareersView, SearchCareersPresenter>(), SearchCareersView {


    var unbinder: Unbinder? = null

    @BindView(R.id.activity_careers_spnCareers)
    lateinit var spnCareers: Spinner

    @BindView(R.id.activity_careers_stemCareers)
    lateinit var spnStemCareers: Spinner

    @BindView(R.id.search_career_etKeyword)
    lateinit var etKeyword: EditText

    private var careersList: MutableList<_Career>? = null
    private var stemCareersList: MutableList<_Career>? = null

    @Inject
    lateinit var schedulers: RxSchedulersImpl

    @Inject
    lateinit var taskRepository: TaskRepository

    var progress: ProgressDialog? = null

    companion object {

        fun newInstance(): SearchCareersFragment {
            return SearchCareersFragment()
        }

    }

    override fun createPresenter(): SearchCareersPresenter {
        (activity as SearchCareersActivity).gifApplication().component?.Inject(this)
        return SearchCareersPresenter(taskRepository, schedulers)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        progress = Utilz.dialog(context!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.frag_search_careers, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    fun init() {
        progress?.show()
        presenter.getCareers()

    }

    var careerId: String? = null
    var stemCareerId: String? = null

    @OnItemSelected(R.id.activity_careers_spnCareers)
    fun onCareersSelected(pos: Int) {
        careerId = careersList?.get(pos)?.code

    }


    @OnItemSelected(R.id.activity_careers_stemCareers)
    fun onOccupationSelected(pos: Int) {
        stemCareerId = stemCareersList?.get(pos)?.code

    }

    override fun dismissProgress() {
        progress?.let { if (it.isShowing) it.dismiss() }
    }

    override fun onErrorGettingCareers() {
        context!!.tmsg("Error")
    }

    override fun industryCareerList(careers: MutableList<_Career>) {
        careersList = careers
        spnCareers.adapter = CareersSpinnerAdapter(context!!, careers)
    }

    override fun stemCareerList(stemCareers: MutableList<_Career>) {
        stemCareersList = stemCareers
        spnStemCareers.adapter = CareersSpinnerAdapter(context!!, stemCareers)
    }


    @OnClick(R.id.activity_careers_btnCareers)
    fun onCareersClick() {

        careerId?.let {
            Utilz.replaceFragmentToActivity(fragmentManager!!, IndustryCareersFragment.newInstance(it),
                    R.id.activity_search_careers_container, addToBackStack = true)
        }


    }

    @OnClick(R.id.activity_careers_btnStemCareers)
    fun onStemCareersClick() {

        stemCareerId?.let {
            Utilz.replaceFragmentToActivity(fragmentManager!!, StemCareerFragment.newInstance(it),
                    R.id.activity_search_careers_container, addToBackStack = true)
        }

    }

    @OnClick(R.id.search_career_btnKeywordSearch)
    fun onSearchKeywordClick() {


        if (!etKeyword.validate())
            return


        Utilz.replaceFragmentToActivity(fragmentManager!!,
                IndustryCareersFragment.newInstance(keyword = etKeyword.text.toString(), isSearchCareer = true),
                R.id.activity_search_careers_container, addToBackStack = true)

    }


    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

}