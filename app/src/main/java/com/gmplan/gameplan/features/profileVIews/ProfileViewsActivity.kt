package com.gmplan.gameplan.features.profileVIews

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.BaseDrwrActivity
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.P_View
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.infrastructure.GmplanApplication
import javax.inject.Inject

/**
 * Created by Sammie on 5/3/2017.
 */
class ProfileViewsActivity : BaseDrwrActivity<ProfileViewsView, ProfileViewPresenter>(), ProfileViewsView {

    override fun finishCurrentActivity() {
        finish()
    }


    @BindView(R.id.activity_profile_views_list)
    lateinit var recyclerView: RecyclerView

    @BindView(R.id.app_toolbar)
    lateinit var toolbar: Toolbar

    @BindView(R.id.activity_profile_views_notFound)
    lateinit var noProfileFound: TextView

    @BindView(R.id.activity_profile_views_sr)
    lateinit var swipeRefresh: SwipeRefreshLayout

    @Inject
    lateinit var taskRepository: TaskRepository

    @Inject
    lateinit var rxSchedulers: RxSchedulersImpl

    lateinit var dialog: ProgressDialog

    override fun gifPresenter(): ProfileViewPresenter {
        (application as GmplanApplication).component?.Inject(this)
        return ProfileViewPresenter(taskRepository, rxSchedulers)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_views)
        ButterKnife.bind(this)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Profile Views"
        createDrawer(toolbar, taskRepository, true, 6)
        init()
    }

    private fun init() {

        dialog = ProgressDialog(this)
        dialog.setMessage("Loading...")



        recyclerView.layoutManager = LinearLayoutManager(this)

        refreshList()

        swipeRefresh.setOnRefreshListener {

            refreshList()

        }

    }

    fun refreshList() {
        dialog.show()
        presenter?.getProfileViews(getUserData?.student?.id.toString())
    }


    override fun dismissProgress() {
        dialog.dismiss()

        if (swipeRefresh.isRefreshing)
            swipeRefresh.isRefreshing = false
    }

    override fun onSuccess(profileViews: List<P_View>) {
        recyclerView.visibility = View.VISIBLE
        noProfileFound.visibility = View.GONE
        recyclerView.adapter = ProfileViewAdapter(profileViews)
    }

    override fun onNoProfileViews() {
        recyclerView.visibility = View.GONE
        noProfileFound.visibility = View.VISIBLE
    }

    override fun onErrorProfileViews() {
        recyclerView.visibility = View.GONE
        noProfileFound.visibility = View.VISIBLE
    }


}