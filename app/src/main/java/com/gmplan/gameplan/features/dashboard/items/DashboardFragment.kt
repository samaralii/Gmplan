package com.gmplan.gameplan.features.dashboard.items

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.gmplan.gameplan.BaseDrwrActivity
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.DashboardItems
import com.gmplan.gameplan.features.agenda.AgendaActivity
import com.gmplan.gameplan.features.messages.MessagesActivity
import com.gmplan.gameplan.features.myCareers.MyCareersActivity
import com.gmplan.gameplan.features.myColleges.MyCollegesActivity
import com.gmplan.gameplan.features.myHighlights.MyHighlightsActivity
import com.gmplan.gameplan.features.myOffers.MyOffersActivity
import com.gmplan.gameplan.features.mySemesters.MySemestersActivity
import com.gmplan.gameplan.features.mygmplan.MyGmplanActivity
import com.gmplan.gameplan.features.staff.myMessenger.MyMessengerActivity
import com.gmplan.gameplan.features.staff.studentsProfiles.StudentProfileActivity
import com.gmplan.gameplan.features.staff.updateStatus.UpdateStatusActivity
import com.gmplan.gameplan.features.userProfile.UserProfile
import com.hannesdorfmann.mosby.mvp.MvpFragment

/**
 * Created by Sammie on 3/21/2017.
 */

class DashboardFragment : MvpFragment<DashboardItemsView, DashboardItemsPresenter>(), DashboardItemsView, PostsAdapter.Listener {


    @BindView(R.id.frag_dashboard_dashboardList)
    lateinit var recyclerView: RecyclerView

    lateinit var unbinder: Unbinder


    override fun onDestroy() {
        super.onDestroy()
    }

    override fun createPresenter(): DashboardItemsPresenter {
        return DashboardItemsPresenter()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.frag_dashboard, container, false) as View
        unbinder = ButterKnife.bind(this, view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        recyclerView.layoutManager = GridLayoutManager(context, 2)
        recyclerView.setHasFixedSize(true)
        presenter.getDashboardItems((activity as BaseDrwrActivity<*, *>).getUserData?.user?.type?.toInt())
    }

    override fun dashBoardItems(items: List<DashboardItems>) {
        val adapter = PostsAdapter(items, context!!, this)
        recyclerView.adapter = adapter
    }

    override fun onClick(id: Int) {
        presenter.onItemClick(id)
    }


    override fun openMyCareerActivity() {
        startActivity(Intent(context, MyCareersActivity::class.java))
        activity!!.finish()
    }

    override fun openMyCollegeActivity() {
        startActivity(Intent(context, MyCollegesActivity::class.java))
        activity!!.finish()
    }


    override fun openMySemestersActivity() {
        startActivity(Intent(context, MySemestersActivity::class.java))
        activity!!.finish()
    }

    override fun openHighlightActivity() {
        startActivity(Intent(context, MyHighlightsActivity::class.java))
        activity!!.finish()
    }

    override fun openMessagesActivity() {
        startActivity(Intent(context, MessagesActivity::class.java))
        activity!!.finish()
    }

    override fun openMyGmplanActivity() {
        startActivity(Intent(context, MyGmplanActivity::class.java))
        activity!!.finish()
    }

    override fun myOffersActivity() {
        startActivity(Intent(context, MyOffersActivity::class.java))
        activity!!.finish()
    }

    override fun openStudentProfileActivity() {
        startActivity(Intent(context, StudentProfileActivity::class.java))
        activity!!.finish()
    }

    override fun openUpdateStatusActivity() {
        startActivity(Intent(context, UpdateStatusActivity::class.java))
        activity!!.finish()
    }

    override fun openMyStudentsActivity() {
//        startActivity(Intent(context, UpdateStatusActivity::class.java))
//        activity.finish()
    }

    override fun openProfileActivity() {
        startActivity(Intent(context, UserProfile::class.java))
        activity!!.finish()
    }

    override fun openAgendaActivity() {
        startActivity(Intent(context, AgendaActivity::class.java))
        activity!!.finish()
    }

    override fun openMyMessageActivity() {
        startActivity(Intent(context, MyMessengerActivity::class.java))
        activity!!.finish()
    }



    override fun onDestroyView() {
        super.onDestroyView()
        unbinder.unbind()
    }
}
