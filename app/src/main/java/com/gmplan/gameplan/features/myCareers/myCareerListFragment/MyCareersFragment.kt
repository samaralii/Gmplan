package com.gmplan.gameplan.features.myCareers.myCareerListFragment

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.Career
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.CustomWebViewActivity
import com.gmplan.gameplan.features.myCareers.MyCareersActivity
import com.gmplan.gameplan.features.myCareers.careersJobs.CareersJobs
import com.gmplan.gameplan.util.Utilz
import com.hannesdorfmann.mosby.mvp.MvpFragment
import javax.inject.Inject


class MyCareersFragment : MvpFragment<MyCareerView, MyCareerPresenter>(), MyCareerView, CareerAdapter.Listener {

    @Inject
    lateinit var schedulers: RxSchedulersImpl

    @Inject
    lateinit var taskRepository: TaskRepository

    var unbinder: Unbinder? = null


    lateinit var pd: ProgressDialog

    var careerAdapter: CareerAdapter? = null


    @BindView(R.id.frag_mycarrer_careerList)
    lateinit var recyclerView: RecyclerView

    @BindView(R.id.frag_mycareer_sr)
    lateinit var swipeRefresh: SwipeRefreshLayout


    override fun createPresenter(): MyCareerPresenter {
        (activity as MyCareersActivity).gifApplication().component?.Inject(this)
        return MyCareerPresenter(taskRepository, schedulers)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pd = Utilz.dialog(context!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.frag_mycareers, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        recyclerView.layoutManager = LinearLayoutManager(context)
        refreshList()

        swipeRefresh.setOnRefreshListener { refreshList() }

    }

    fun refreshList() {
        pd.show()
        presenter.getCareersList((activity as MyCareersActivity).getUserData?.student?.id.toString())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }


    override fun onFailed() {
        Utilz.tmsgError(context!!, "Error")
    }

    override fun onNoCareerFound() {
        Utilz.tmsgInfo(context!!, "No Career Found")
    }

    override fun careerList(careers: List<Career>) {
        careerAdapter = CareerAdapter(careers as MutableList<Career>, this, context!!)
        recyclerView.adapter = careerAdapter
    }

    override fun dismissDialog() {
        if (pd.isShowing)
            pd.dismiss()

        if (swipeRefresh.isRefreshing)
            swipeRefresh.isRefreshing = false
    }


    override fun onDeleteClick(career_id: String, pos: Int) {
        pd.show()
        presenter.deleteCareer(career_id, pos)
    }

    override fun onJobClick(title: String) {
        val fragment = CareersJobs.newInstance((activity as MyCareersActivity).getUserData?.student?.schoolId.toString(), title)
        val ft = fragmentManager!!.beginTransaction()
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
        ft.replace(R.id.activity_mycareers_container, fragment)
        ft.addToBackStack(null)
        ft.commit()
    }

    override fun onItemClick(code: String) {
        //https://www.gmplan.com/careers/api_career_detail/41-2021.00
        val i = Intent(context, CustomWebViewActivity::class.java)
        i.putExtra("url", "https://www.gmplan.com/careers/api_career_detail/$code")
        i.putExtra("job", "Detail")
        startActivity(i)

    }


    override fun onFailedDeleteCareer() {
        Utilz.tmsgError(context!!, "Error")
    }

    override fun careerDeleted(pos: Int) {
        Utilz.tmsgSuccess(context!!, "Deleted")
        careerAdapter?.removeAt(pos)

    }


}