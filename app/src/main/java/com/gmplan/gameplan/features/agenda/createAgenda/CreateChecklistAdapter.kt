package com.gmplan.gameplan.features.agenda.createAgenda

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.CheckListsEvents

class CreateChecklistAdapter(var data: MutableList<CheckListsEvents>,
                             val onEditClick: (CheckListsEvents, Int) -> Unit)
    : RecyclerView.Adapter<CreateChecklistVH>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): CreateChecklistVH {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.list_createchecklist, p0, false)
        return CreateChecklistVH(v)
    }

    override fun onBindViewHolder(holder: CreateChecklistVH, pos: Int) {

        holder.edit?.setOnClickListener {
            onEditClick.invoke(data[holder.layoutPosition], holder.layoutPosition)
        }

        val startDateTime = data[pos].startDateTime
        val endDateTime = data[pos].endDateTime

        holder.startEndDateTime?.text = "$startDateTime - $endDateTime"

        holder.stepText?.text = "Step ${holder.layoutPosition}"
        holder.title?.text = data[pos].event


    }

    override fun getItemCount() = data.size

    fun addItem(checkListsEvents: CheckListsEvents) {
        data.add(checkListsEvents)
        notifyDataSetChanged()
    }

    fun editItem(event: CheckListsEvents, pos: Int) {
        data.removeAt(pos)
        data.add(pos, event)
        notifyDataSetChanged()
    }

    fun getList(): List<CheckListsEvents> {
        return data
    }

}

class CreateChecklistVH(v: View) : RecyclerView.ViewHolder(v) {
    val title by lazy { v.findViewById<TextView>(R.id.list_checklist_tvTitle) }
    val startEndDateTime by lazy { v.findViewById<TextView>(R.id.list_checklist_tvStartEndDateTime) }
    val stepText by lazy { v.findViewById<TextView>(R.id.list_checklist_tvStepText) }
    val edit by lazy { v.findViewById<TextView>(R.id.list_checklist_tvEdit) }
}
