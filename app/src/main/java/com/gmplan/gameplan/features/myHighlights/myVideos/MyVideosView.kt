package com.gmplan.gameplan.features.myHighlights.myVideos

import com.gmplan.gameplan.data.pojos.Video
import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Sammie on 5/22/2017.
 */
interface MyVideosView  : MvpView {
    fun dismissDialog()
    fun onFailedGettingUserVideos()
    fun onNoVideoFound()
    fun onSuccessVideoFound(videos: MutableList<Video>)
    fun onFailedPostingVideo()
    fun onSuccessVideoPosted()
    fun onFailedEditingVideo()
    fun onSuccessVideoEdited()
    fun onSuccessVideoDeleted(pos: Int)
    fun onFailedDeletingVideo()
}