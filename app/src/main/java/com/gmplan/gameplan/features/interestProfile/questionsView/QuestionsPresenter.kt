package com.gmplan.gameplan.features.interestProfile.questionsView

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.QuestionsResult
import com.gmplan.gameplan.data.pojos.interestProfileQuestions.Question
import com.gmplan.gameplan.data.pojos.interestProfileQuestions.QuestionsPojo
import com.gmplan.gameplan.data.source.TaskDataSource
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sammie on 8/28/2017.
 */
class QuestionsPresenter(val repo: TaskDataSource, val schedulers: RxSchedulers) : MvpBasePresenter<QuestionsView>() {


    private val list = mutableListOf<ArrayList<Question>>()
    private var index = 0

    private val disposable = CompositeDisposable()

    fun getQuestions(studentId: String) {
        disposable.add(repo.getInterestProfileQuestions(studentId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {
                    if (isViewAttached) view?.dismissProgress()
                }
                .subscribeWith(object : DisposableSingleObserver<QuestionsPojo>() {
                    override fun onSuccess(response: QuestionsPojo) {

                        try {

                            if (response.questions.isNotEmpty()) {

                                createQuestionsList(response.questions)

                            } else {

                                if (isViewAttached) view?.onErrorGettingQuestion()

                            }


                        } catch (ex: Exception) {
                            ex.printStackTrace()
                            if (isViewAttached) view?.onErrorGettingQuestion()
                        }

                    }

                    override fun onError(error: Throwable) {

                        error.printStackTrace()

                        if (isViewAttached) view?.onErrorGettingQuestion()
                    }

                }))
    }


    private fun createQuestionsList(questions: List<Question>) {

        var count = 0

        for (i in 0..14) {

            val l = ArrayList<Question>()
            (count until (count + 4)).mapTo(l) { questions[it] }
            list.add(l)
            count += 4

        }

        if (isViewAttached) view?.showInitialQuestions(list[index], index)

    }

    fun onNextClick() {

        index++

        if (index == 14)
            if (isViewAttached) view?.enableProceedBtn()

        if (index <= 14) {
            if (isViewAttached) view?.showNextQuestions(list[index], index)
        } else {
            index = 14
        }

    }

    fun onPreviousClick() {

        index--

        if (index < 14)
            if (isViewAttached) view?.disableProceedBtn()

        if (index >= 0) {
            if (isViewAttached) view?.showPreviousQuestions(list[index], index)
        } else {
            index = 0
        }

    }

    fun onUpdateList(i: Int, arr: ArrayList<Question>) {
        list.removeAt(i)
        list.add(i, arr)
    }


    fun onSaveAnswer(studentId: String, newAnswer: String = "153453245543224333523534323351555544223252343544234325154442") {

        val answers = getAnswers()

        repo.getResults(studentId, answers)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally { if (isViewAttached) view?.dismissProgress() }
                .subscribeWith(object : DisposableSingleObserver<QuestionsResult>() {
                    override fun onSuccess(t: QuestionsResult) {

                        if (t.result.isNotEmpty()) {
                            if (isViewAttached) view?.onSuccessResults(t.result)
                        } else {
                            if (isViewAttached) view?.onErrorGettingResult()
                        }

                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        if (isViewAttached) view?.onErrorGettingResult()
                    }

                })
    }


    private fun getAnswers(): String {

        var answers = ""

        for (i in 0 until list.size) {
            for (j in 0 until list[i].size) {
                answers += list[i][j].rate
            }
        }

        return answers
    }


    override fun detachView(retainInstance: Boolean) {
        super.detachView(retainInstance)
        if (!retainInstance) disposable.clear()
    }

}