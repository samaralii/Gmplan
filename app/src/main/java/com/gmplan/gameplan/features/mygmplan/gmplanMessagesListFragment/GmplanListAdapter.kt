package com.gmplan.gameplan.features.mygmplan.gmplanMessagesListFragment

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.Gmessage

/**
 * Created by Sammie on 5/26/2017.
 */
class GmplanListAdapter(val data: List<Gmessage>, val listener: Listener) : RecyclerView.Adapter<GmplanListAdapter.MyGmpVH>() {

    override fun onBindViewHolder(holder: MyGmpVH, position: Int) {

        holder.title.text = data[position].title
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyGmpVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_gmplist, parent, false)
        return MyGmpVH(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }


    inner class MyGmpVH(v: View) : RecyclerView.ViewHolder(v) {

        @BindView(R.id.list_gmplist_title)
        lateinit var title: TextView


        init {
            ButterKnife.bind(this, v)

            v.setOnClickListener {
                listener.onItemClick(data[layoutPosition])

            }
        }
    }

    interface Listener {
        fun onItemClick(gmessage: Gmessage)
    }
}