package com.gmplan.gameplan.features.myHighlights.myVideos

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.UserVideos
import com.gmplan.gameplan.data.source.TaskDataSource
import com.google.gson.JsonElement
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sammie on 5/22/2017.
 */
class MyVideosPresenter(val repo: TaskDataSource, val schedulers: RxSchedulers) : MvpBasePresenter<MyVideosView>() {


    fun getUserVideos(studentId: String) {
        repo.getUserVideos(studentId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<UserVideos>() {
                    override fun onSuccess(p0: UserVideos) {

                        if (p0 != null && p0.response == "success") {

                            if (p0.videos.size > 0) {

                                if (isViewAttached)
                                    view?.onSuccessVideoFound(p0.videos)

                            } else {

                                if (isViewAttached)
                                    view?.onNoVideoFound()

                            }

                        } else {

                            if (isViewAttached)
                                view?.onFailedGettingUserVideos()

                        }
                    }

                    override fun onError(p0: Throwable) {

                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onFailedGettingUserVideos()

                    }
                })
    }

    fun postVideo(studentId: String, url: String, name: String) {
        repo.postVideo(studentId, url, name)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<JsonElement>() {
                    override fun onSuccess(p0: JsonElement) {

                        val obj = p0.asJsonObject

                        val status = obj?.get("status")?.asString

                        if (status == "success") {

                            if (isViewAttached)
                                view?.onSuccessVideoPosted()

                        } else {

                            if (isViewAttached)
                                view?.onFailedPostingVideo()

                        }

                    }

                    override fun onError(p0: Throwable) {

                        p0.printStackTrace()


                        if (isViewAttached)
                            view?.onFailedPostingVideo()

                    }

                })
    }

    fun editVideo(videoId: String, url: String, name: String) {
        repo.editVideo(videoId, url, name)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<JsonElement>() {
                    override fun onError(p0: Throwable) {

                        p0.printStackTrace()


                        if (isViewAttached)
                            view?.onFailedEditingVideo()

                    }

                    override fun onSuccess(p0: JsonElement) {

                        val obj = p0.asJsonObject

                        val status = obj?.get("status")?.asString

                        if (status == "success") {

                            if (isViewAttached)
                                view?.onSuccessVideoEdited()

                        } else {

                            if (isViewAttached)
                                view?.onFailedEditingVideo()

                        }


                    }

                })
    }

    fun deleteVideo(videoId: String, pos: Int) {
        repo.deleteVideo(videoId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<JsonElement>() {

                    override fun onSuccess(p0: JsonElement) {

                        val obj = p0.asJsonObject

                        val status = obj?.get("response")?.asString

                        if (status == "success") {

                            if (isViewAttached)
                                view?.onSuccessVideoDeleted(pos)

                        } else {

                            if (isViewAttached)
                                view?.onFailedDeletingVideo()

                        }

                    }

                    override fun onError(p0: Throwable) {

                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onFailedDeletingVideo()

                    }

                })
    }

}