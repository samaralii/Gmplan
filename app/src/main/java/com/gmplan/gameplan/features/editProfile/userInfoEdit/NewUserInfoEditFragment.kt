package com.gmplan.gameplan.features.editProfile.userInfoEdit

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.bumptech.glide.Glide
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.userPojo.UserData
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.editProfile.EditProfileActivity
import com.gmplan.gameplan.util.Utilz
import com.hannesdorfmann.mosby.mvp.MvpFragment
import com.tbruyelle.rxpermissions2.RxPermissions
import de.hdodenhof.circleimageview.CircleImageView
import mabbas007.tagsedittext.TagsEditText
import pl.aprilapps.easyphotopicker.EasyImage
import java.io.File
import javax.inject.Inject

class NewUserInfoEditFragment : MvpFragment<UserInfoEditView, UserInfoEditPresenter>(), UserInfoEditView {


    var userData: UserData? = null

    var unbinder: Unbinder? = null

    @BindView(R.id.frag_edit_btnProceed)
    lateinit var btnProceed: Button


    @BindView(R.id.frag_edit_profileImage)
    lateinit var profileImage: CircleImageView

    @BindView(R.id.frag_edit_info_etSchoolName)
    lateinit var schoolName: TextView

    @BindView(R.id.frag_edit_etLikes)
    lateinit var etLikes: TagsEditText

    @BindView(R.id.frag_edit_etDislikes)
    lateinit var etDislikes: TagsEditText

    @BindView(R.id.frag_edit_etfavMusic)
    lateinit var etMusic: TagsEditText

    @BindView(R.id.frag_edit_etFavTeam)
    lateinit var etFavTeam: TagsEditText

    @BindView(R.id.frag_edit_etFavCollege)
    lateinit var etCollege: TagsEditText

    lateinit var progress: ProgressDialog

    @Inject
    lateinit var rxSchedulers: RxSchedulersImpl
    @Inject
    lateinit var taskRepository: TaskRepository


    var imageFile: File? = null

    override fun createPresenter(): UserInfoEditPresenter {
        (activity as EditProfileActivity).gifApplication().component?.Inject(this)
        return UserInfoEditPresenter(taskRepository, rxSchedulers)
    }

    companion object {

        fun newInstance(): NewUserInfoEditFragment {
            return NewUserInfoEditFragment()
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userData = (activity as EditProfileActivity).getUserData
        progress = Utilz.dialog(context!!)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.frag_edit_userinfo, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {



        userData?.let { presenter.onCreateView(it) }
        bindDataWithViews()
    }

    private fun bindDataWithViews() {
        try {

            val img_url: String = Utilz.getImgUrl(userData as UserData)

            Glide.with(context)
                    .load(img_url)
                    .dontAnimate()
                    .placeholder(R.drawable.placeholder)
                    .into(profileImage)

        } catch (e: Exception) {
            e.printStackTrace()
        }


        schoolName.text = userData?.student?.mySchool

    }


    @OnClick(R.id.frag_edit_btnProceed)
    fun onProceedClick() {
        progress.show()

        presenter.editUserProfile(userData?.student?.id as String, etLikes.text.toString(), etDislikes.text.toString(),
                etMusic.text.toString(), etFavTeam.text.toString(), etCollege.text.toString(), imageFile)


    }


    @OnClick(R.id.frag_edit_userInfo_uploadImage)
    fun onUpdateImageClick() {

        val rxPermission = RxPermissions(activity!!)

        rxPermission
                .request(android.Manifest.permission.CAMERA)
                .subscribe({ granted ->

                    if (granted) {
                        EasyImage.openChooserWithGallery(this, "Choose", 1)
                    }

                })

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        EasyImage.handleActivityResult(requestCode, resultCode, data, activity, object : EasyImage.Callbacks {
            override fun onImagePicked(p0: File?, p1: EasyImage.ImageSource?, p2: Int) {

                imageFile = p0

                Glide.with(context)
                        .load(p0)
                        .dontAnimate()
                        .placeholder(R.drawable.placeholder)
                        .into(profileImage)


            }

            override fun onImagePickerError(p0: java.lang.Exception?, p1: EasyImage.ImageSource?, p2: Int) {

                p0?.printStackTrace()

            }

            override fun onCanceled(p0: EasyImage.ImageSource?, p1: Int) {


            }
        })

    }

    override fun dismissProgress() {
        progress.dismiss()
    }

    override fun onFailedEditProfile() {
        Utilz.tmsg(context!!, "Error")
    }

    override fun onSuccessEditProfile() {
        Utilz.tmsg(context!!, "Success")
    }

    override fun setTags(myLike: Array<String?>?, myDislike: Array<String?>?, myMusic: Array<String?>?,
                         myTeam: Array<String?>?, myCollege: Array<String?>?) {

        myLike?.let { etLikes.setTags(it) }
        myDislike?.let { etDislikes.setTags(it) }
        myMusic?.let { etMusic.setTags(it) }
        myTeam?.let { etFavTeam.setTags(it) }
        myCollege?.let { etCollege.setTags(it) }

    }

}


