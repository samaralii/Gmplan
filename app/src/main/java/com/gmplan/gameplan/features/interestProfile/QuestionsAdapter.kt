package com.gmplan.gameplan.features.interestProfile

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.BindViews
import butterknife.ButterKnife
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.interestProfileQuestions.Question

class QuestionsAdapter(var data: ArrayList<Question>, var listener: Listener) : RecyclerView.Adapter<QuestionsAdapter.QuestionsViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuestionsViewHolder {
        return QuestionsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.questions_list, parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: QuestionsViewHolder, position: Int) {
        holder.questionTxt.text = "${data[position].qIndex} ${data[position].qText}"


        if (data[position].rate.toInt() != 0)
            holder.rd[(data[position].rate.toInt()) - 1].isChecked = !data[position].rate.isEmpty()


    }


    inner class QuestionsViewHolder(val view: View?) : RecyclerView.ViewHolder(view) {

        @BindView(R.id.questions_list_text)
        lateinit var questionTxt: TextView

        @BindViews(R.id.questions_list_dislike, R.id.questions_list_unlike,
                R.id.questions_list_notSure, R.id.questions_list_like,
                R.id.questions_list_stronglike)
        lateinit var rd: List<@JvmSuppressWildcards RadioButton>


        @BindView(R.id.questions_list_rdGroup)
        lateinit var rdGroup: RadioGroup


        init {
            ButterKnife.bind(this, view as View)

            rdGroup.setOnCheckedChangeListener { _, i ->

                val q = data[layoutPosition]

                (0 until rd.size)
                        .filter { rd[it].id == i }
                        .forEach { q.rate = (it + 1).toString() }

                data[layoutPosition] = q

                listener.onClick(data)

            }
        }

    }

    interface Listener {
        fun onClick(data: ArrayList<Question>)
    }
}