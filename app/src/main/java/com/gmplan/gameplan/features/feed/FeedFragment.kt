package com.gmplan.gameplan.features.feed

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.staffFeed.StaffNewssFeed
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.dashboard.FeedAdapter
import com.gmplan.gameplan.util.Utilz
import com.hannesdorfmann.mosby.mvp.MvpFragment
import javax.inject.Inject

/**
 * Created by Sammie on 6/6/2017.
 */
class FeedFragment : MvpFragment<FeedView, FeedPresenter>(), FeedView {


    @BindView(R.id.frag_feed_list)
    lateinit var recyclerView: RecyclerView


    @BindView(R.id.frag_feed_sr)
    lateinit var sr: SwipeRefreshLayout

    var unbinder: Unbinder? = null

    @Inject
    lateinit var taskRepository: TaskRepository

    @Inject
    lateinit var schedulers: RxSchedulersImpl

    var pd: ProgressDialog? = null

    override fun createPresenter(): FeedPresenter {
        (activity as FeedActivity).gifApplication().component?.Inject(this)
        return FeedPresenter(taskRepository, schedulers)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        pd = Utilz.dialog(context!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.frag_feed, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.hasFixedSize()

        refreshList()


        sr.setOnRefreshListener {

            refreshList()

        }


    }

    private fun refreshList() {

        sr.isRefreshing = true
        presenter.getStaffFeed()

    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }

    override fun dismissDialog() {

        if (sr.isRefreshing)
            sr.isRefreshing = false

    }

    override fun onErrorGettingFeed() {
    }

    override fun onSuccessFeed(p0: List<StaffNewssFeed>) {

        recyclerView.adapter = FeedAdapter(p0 as MutableList<StaffNewssFeed>,
                (activity as FeedActivity).gifApplication())

    }

    override fun onNoFeedFound() {
        Utilz.tmsgInfo(context!!, "No Feed")
    }


}