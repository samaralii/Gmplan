package com.gmplan.gameplan.features.Login;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.gmplan.gameplan.features.feed.FeedActivity;

@RequiresApi(api = Build.VERSION_CODES.M)
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {
    private CancellationSignal cancellationSignal;
    private Context context;
    private FingerprintHandlerListener listener;

    public FingerprintHandler(Context context, FingerprintHandlerListener listener) {
        this.context = context;
        this.listener = listener;
    }

    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
        cancellationSignal = new CancellationSignal();

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
//        Toast.makeText(context, "Authentication error\n" + errString, Toast.LENGTH_SHORT).show();
        listener.onAuthenticationError(errMsgId, errString);
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
//        Toast.makeText(context, "Authentication help\n" + helpString, Toast.LENGTH_SHORT).show();
        listener.onAuthenticationHelp(helpMsgId, helpString);
    }

    @Override
    public void onAuthenticationFailed() {
//        Toast.makeText(context, "Authentication failed.", Toast.LENGTH_SHORT).show();
        listener.onAuthenticationFailed();
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        listener.onAuthenticationSucceeded();
//        Toast.makeText(context, "Authentication succeeded.", Toast.LENGTH_SHORT).show();
//        context.startActivity(new Intent(context, FeedActivity.class));
//        context.finish();

    }

    public interface FingerprintHandlerListener {
        void onAuthenticationError(int errMsgId, CharSequence errString);

        void onAuthenticationHelp(int helpMsgId, CharSequence helpString);

        void onAuthenticationFailed();

        void onAuthenticationSucceeded();
    }
}