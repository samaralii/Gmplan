package com.gmplan.gameplan.features.mySemesters.semestersListFragment

import android.app.Dialog
import android.app.ProgressDialog
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import butterknife.BindView
import com.gmplan.gamep.MySemestersPresenter
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.Semestergoal
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.mySemesters.MySemestersActivity
import com.gmplan.gameplan.util.Utilz
import com.hannesdorfmann.mosby.mvp.MvpFragment
import javax.inject.Inject

/**
 * Created by Sammie on 5/19/2017.
 */
class MySemestersFragment :
        MvpFragment<MySemestersView, MySemestersPresenter>(),
        MySemestersView,
        MySemestersAdapter.Listeners,
        AddSemesterDialog.DialogListener {


    @Inject
    lateinit var schedulers: RxSchedulersImpl

    @Inject
    lateinit var taskRepository: TaskRepository

    var unbinder: butterknife.Unbinder? = null

    @butterknife.BindView(com.gmplan.gameplan.R.id.frag_mycolleges_list)
    lateinit var recyclerView: RecyclerView

    @BindView(R.id.frag_mycolleges_sr)
    lateinit var swipeRefresh: SwipeRefreshLayout

    lateinit var pd: ProgressDialog


    var adapter: MySemestersAdapter? = null

    override fun createPresenter(): MySemestersPresenter {
        (activity as MySemestersActivity).gifApplication().component?.Inject(this)
        return MySemestersPresenter(taskRepository, schedulers)
    }

    override fun onCreate(savedInstanceState: android.os.Bundle?) {
        super.onCreate(savedInstanceState)
        pd = Utilz.dialog(context!!)
    }

    override fun onCreateView(inflater: android.view.LayoutInflater, container: android.view.ViewGroup?, savedInstanceState: android.os.Bundle?): android.view.View? {
        val v = inflater?.inflate(R.layout.frag_mycolleges, container, false) as android.view.View
        unbinder = butterknife.ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: android.view.View, savedInstanceState: android.os.Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        recyclerView.layoutManager = LinearLayoutManager(context)

        refreshList()


        swipeRefresh.setOnRefreshListener {

            refreshList()


        }


    }

    fun refreshList() {
        pd.show()
        presenter.getUserSemesters((activity as MySemestersActivity).getUserData?.student?.id.toString())
    }

    @butterknife.OnClick(com.gmplan.gameplan.R.id.frag_mycolleges_btnAdd)
    fun onBtnAdd() {
        val dialog = AddSemesterDialog()
        dialog.show(fragmentManager, "dialog")
    }

    override fun onAddClick(title: String, detail: String, grade: String, semester: String, dialog: Dialog) {
        dialog.dismiss()
        pd.show()
        presenter.addSemester((activity as MySemestersActivity).getUserData?.student?.id.toString(),
                title, grade, semester, detail
        )

    }


    override fun onDestroyView() {
        super.onDestroyView()
        unbinder?.unbind()
    }


    override fun onFailedGettingSemesters() {
        Utilz.tmsgError(context, "Error")
    }

    override fun onNoSemestersGoalFound() {
        Utilz.tmsgError(context, "No Semesters Found")
    }

    override fun onSuccessSemestersList(semestergoals: List<Semestergoal>) {
        adapter = MySemestersAdapter(semestergoals as MutableList<Semestergoal>, this, context!!)
        recyclerView.adapter = adapter
    }


    override fun dismissDialog() {
        if (pd.isShowing)
            pd.dismiss()


        if (swipeRefresh.isRefreshing)
            swipeRefresh.isRefreshing = false
    }

    override fun onDeleteClick(semesterId: String, pos: Int) {
        pd.show()
        presenter.deleteSemester(semesterId, pos)
    }

    override fun deleteSuccessfully(pos: Int) {
        Utilz.tmsgSuccess(context, "Success")
        adapter?.removeAt(pos)

    }

    override fun onFailedDelete() {
        Utilz.tmsgError(context, "Error")
    }


    override fun onSuccessfullySemesterAdded() {
        Utilz.tmsgSuccess(context, "Added")
        refreshList()
    }

    override fun onFailedAddSemester() {
        Utilz.tmsgError(context, "Error")
    }


}