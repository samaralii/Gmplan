package com.gmplan.gameplan.features.splashScreen

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.Login.LoginActivity
import com.gmplan.gameplan.features.introView.Intro
import com.gmplan.gameplan.infrastructure.GmplanApplication
import com.gmplan.gameplan.util.AppConst
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SplashScreenActivity : AppCompatActivity() {


    @Inject
    lateinit var repo: TaskRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        (application as GmplanApplication).component?.Inject(this)


        Single.just(1)
                .delay(1000, TimeUnit.MILLISECONDS)
                .subscribe { _ ->

                    val isFirstTime = repo.getBooleanByKey(AppConst.IS_FIRST_TIME)

                    if (isFirstTime) {
                        startActivity(Intent(this, Intro::class.java))
                        repo.addValueInPref(AppConst.IS_FIRST_TIME, false)
                    } else {
                        startActivity(Intent(this, LoginActivity::class.java))
                    }

                    finish()

                }
    }

}
