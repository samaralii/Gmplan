package com.gmplan.gameplan.features.myCareers

import android.os.Bundle
import android.support.v7.widget.Toolbar
import com.gmplan.gameplan.BaseDrwrActivity
import com.gmplan.gameplan.EmptyPresenter
import com.gmplan.gameplan.EmptyView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.myCareers.myCareerListFragment.MyCareersFragment
import com.gmplan.gameplan.util.Utilz
import com.gmplan.gameplan.util.hideKeyboard
import javax.inject.Inject

class MyCareersActivity : BaseDrwrActivity<EmptyView, EmptyPresenter>() {

    override fun gifPresenter() = EmptyPresenter()

    override fun finishCurrentActivity() {
        finish()
    }

    @Inject
    lateinit var repository: TaskRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mycareers)
        gifApplication().component?.Inject(this)
        val toolbar = findViewById<Toolbar>(R.id.app_toolbar)
        setSupportActionBar(toolbar)
        title = "My Careers"
        createDrawer(toolbar, repository, true)
        Utilz.addFragmentToActivity(supportFragmentManager, MyCareersFragment(), R.id.activity_mycareers_container)

        hideKeyboard()
    }

    override fun onBackPressed() {
        val c = supportFragmentManager.backStackEntryCount
        if (c > 0)
            supportFragmentManager.popBackStack()
        else
            super.onBackPressed()
    }
}