package com.gmplan.gameplan.features.Login

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.widget.SwitchCompat
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ProgressBar
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.gmplan.gameplan.Injection.modules.RxSchedulersImpl
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.userPojo.UserPojo
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.feed.FeedActivity
import com.gmplan.gameplan.infrastructure.GmplanApplication
import com.gmplan.gameplan.util.*
import com.hannesdorfmann.mosby.mvp.MvpActivity
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject


class LoginActivity : MvpActivity<LoginView, LoginPresenter>(), LoginView, FingerprintDialog.FingerprintListener {


    override fun onTestApiReturn() {
        Utilz.tmsgInfo(this, "error in test api")
    }

    override fun onReturnApi(result: Any) {
        if (result is Int) {
            Utilz.tmsgInfo(this, result.toString())
        }
    }

//    @OnClick(R.id.frag_login_btnFbLogin)
//    fun testApiCall() {
//        presenter.testApi()
//    }


    @Inject
    lateinit var repository: TaskRepository

    @Inject
    lateinit var schedulers: RxSchedulersImpl

    @BindView(R.id.activity_login_etEmail)
    lateinit var etEmail: EditText

    @BindView(R.id.activity_login_etPassword)
    lateinit var etPassword: EditText

    @BindView(R.id.activity_login_fieldsLayout)
    lateinit var fieldsLayout: LinearLayout

    @BindView(R.id.activity_login_progress)
    lateinit var progressBar: ProgressBar

    @BindView(R.id.frag_login_btnLogin)
    lateinit var btnLogin: Button

    @BindView(R.id.login_button)
    lateinit var btnFbLogin: LoginButton

    @BindView(R.id.activity_login_llFingerprintLogin)
    lateinit var llFingerprintLogin: View

    @BindView(R.id.activity_login_swEnable)
    lateinit var swEnable: SwitchCompat


    private val callBackManager by lazy { CallbackManager.Factory.create() }


    override fun createPresenter(): LoginPresenter {
        (application as GmplanApplication).component?.Inject(this)

        return LoginPresenter(repository, schedulers)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        ButterKnife.bind(this)
        presenter.checkForPreviousLogin()
        init()
    }

    private fun init() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            swEnable.setOnCheckedChangeListener { compoundButton, isCheck ->

                if (isCheck) {

                    checkFingerPrint(this,
                            securityNotEnabled = {
                                tmsg("Lock screen security not enabled in Settings")
                                compoundButton.isChecked = false
                                return@setOnCheckedChangeListener

                            },
                            permissionRequired = {
                                tmsg("Fingerprint authentication permission not enabled")
                                compoundButton.isChecked = false
                                return@setOnCheckedChangeListener

                            },
                            noEnrolledFingerFound = {
                                tmsg("Register at least one fingerprint in Settings")
                                compoundButton.isChecked = false
                                return@setOnCheckedChangeListener
                            })

                    //enable fingerprint
                    presenter.enableFingerprintLogin()

                } else {

                    presenter.disableFingerprintLogin()

                }

            }
        } else {

            //Android version is lower than M
            llFingerprintLogin.visibility = View.GONE

        }




        LoginManager.getInstance().logOut()

        btnFbLogin.setReadPermissions("email")


        btnFbLogin.registerCallback(callBackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult) {
                Log.d("FB_USER_REGISTER", "${result.accessToken}")
            }

            override fun onError(error: FacebookException?) {
                Log.e("FB_ERROR", "error", error)
            }

            override fun onCancel() {
            }
        })


        LoginManager.getInstance().registerCallback(callBackManager, object : FacebookCallback<LoginResult> {
            override fun onError(error: FacebookException?) {
                Log.e("FB_ERROR", "error", error)
            }

            override fun onCancel() {
            }

            override fun onSuccess(result: LoginResult?) {
                Log.d("FB_USER_LOGIN", "${result?.accessToken}")


                val request = GraphRequest.newMeRequest(result?.accessToken) { jsonObj, response ->
                    tmsg(jsonObj.getString("email"))
                }

                val parameters = Bundle()
                parameters.putString("fields", "id,name,email,gender,birthday")
                request.parameters = parameters
                request.executeAsync()

            }

        })


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        callBackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun showProgress() {
        btnLogin.text = ""
        progressBar.visibility = View.VISIBLE
        btnLogin.isEnabled = false

        etEmail.isEnabled = false
        etPassword.isEnabled = false
    }

    private fun hideProgress() {
        btnLogin.text = "Login"
        progressBar.visibility = View.INVISIBLE
        btnLogin.isEnabled = true

        etEmail.isEnabled = true
        etPassword.isEnabled = true
    }

    @OnClick(R.id.frag_login_btnLogin)
    fun onLoginClick() {

        if (etEmail.text.toString().isEmpty()) {
            etEmail.error = AppConst.REQUIRED_FIELD
            return
        } else {
            etEmail.error = null
        }

        if (etPassword.text.toString().isEmpty()) {
            etPassword.error = AppConst.REQUIRED_FIELD
            return
        } else {
            etPassword.error = null
        }


        showProgress()
        presenter.getOneSignalId(etEmail.text.toString(), etPassword.text.toString())

    }


    override fun onErrorLogin() {
        Utilz.tmsgError(this, "Error")
        hideProgress()
        YoYo.with(Techniques.Shake)
                .playOn(fieldsLayout)
    }

    override fun onSuccessLogin(userPojo: UserPojo?) {

        Utilz.tmsg(this, "Success")
        Log.d("USER_STUDENT_ID", " ========> " + userPojo?.userData?.student?.id)

        startActivity(Intent(this, FeedActivity::class.java))
        finish()

    }

    override fun onWrongCredentials() {
        Utilz.tmsgError(this, "Wrong Email or Password")
    }

    override fun onFinally() {

        hideProgress()

    }

    override fun openFeedActivity() {
        startActivity(Intent(this, FeedActivity::class.java))
        finish()
    }

    override fun enableFingerprintViews() {

        activity_login_llFingerprint.showView()
        activity_login_llFingerprintLogin.hideView()

        activity_login_llFingerprint.setOnClickListener {
            val dialog = FingerprintDialog()
            dialog.show(supportFragmentManager, "fingerprint_dialog")
        }


    }


    override fun onSuccess() {
        startActivity(Intent(this, FeedActivity::class.java))
        finish()
    }

    override fun onFailed() {
//        YoYo.with(Techniques.Shake).playOn(view)
        Utilz.tmsg(this, "Error")
    }

}
