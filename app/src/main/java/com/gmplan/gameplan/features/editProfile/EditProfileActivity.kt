package com.gmplan.gameplan.features.editProfile

import android.os.Bundle
import android.support.annotation.IdRes
import android.support.v7.widget.Toolbar
import butterknife.BindView
import butterknife.ButterKnife
import com.gmplan.gameplan.BaseDrwrActivity
import com.gmplan.gameplan.EmptyPresenter
import com.gmplan.gameplan.EmptyView
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.features.editProfile.userInfoEdit.NewUserInfoEditFragment
import com.gmplan.gameplan.util.Utilz
import com.gmplan.gameplan.util.hideKeyboard
import javax.inject.Inject

/**
 * Created by Sammie on 5/1/2017.
 */
class EditProfileActivity : BaseDrwrActivity<EmptyView, EmptyPresenter>() {

    override fun gifPresenter() = EmptyPresenter()

    override fun finishCurrentActivity() {
        finish()
    }

    @BindView(R.id.app_toolbar)
    lateinit var toolbar: Toolbar


    @IdRes
    val containerId: Int = R.id.activity_edit_profile_container

    @Inject
    lateinit var repository: TaskRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        gifApplication().component?.Inject(this)
        ButterKnife.bind(this)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Edit Profile"

        createDrawer(toolbar, repository, true, 2)

        var fragment = supportFragmentManager.findFragmentById(containerId) as NewUserInfoEditFragment?

        if (fragment == null)
            fragment = NewUserInfoEditFragment.newInstance()

        Utilz.addFragmentToActivity(supportFragmentManager, fragment, containerId)

        hideKeyboard()


    }


}