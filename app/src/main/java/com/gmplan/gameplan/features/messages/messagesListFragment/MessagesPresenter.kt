package com.gmplan.gameplan.features.messages.messagesListFragment

import com.gmplan.gameplan.Injection.modules.RxSchedulers
import com.gmplan.gameplan.data.pojos.UserMessages
import com.gmplan.gameplan.data.pojos.detailMessagePojos.MessageDetail
import com.gmplan.gameplan.data.source.TaskDataSource
import com.google.gson.JsonElement
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sammie on 5/25/2017.
 */
class MessagesPresenter(private val repo: TaskDataSource, private val schedulers: RxSchedulers) : MvpBasePresenter<MessagesView>() {


    fun getMessagesList(studentId: String) {
        repo.getUserMessages(studentId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<UserMessages>() {
                    override fun onError(p0: Throwable) {
                        p0.printStackTrace()


                        if (isViewAttached)
                            view?.onFailedGettingList()
                    }

                    override fun onSuccess(p0: UserMessages) {

                        if (p0.response == "success") {


                            if (p0.messages.size > 0) {

                                if (isViewAttached)
                                    view?.onSuccessMessages(p0.messages)

                            } else {

                                if (isViewAttached)
                                    view?.onNoMessagesFound()

                            }


                        } else {

                            if (isViewAttached)
                                view?.onFailedGettingList()

                        }


                    }
                })
    }

    fun messageDetail(messageId: String) {
        repo.getMessageDetail(messageId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<MessageDetail>() {
                    override fun onError(p0: Throwable) {

                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onFailedGettingDetail()

                    }

                    override fun onSuccess(p0: MessageDetail) {

                        if (isViewAttached)
                            view?.onSuccessMessageDetail(p0.messages)

                    }

                })
    }


    fun replyMessage(studentId: String, collegeMessageId: String, body: String,
                     subject: String, collegeId: String, userId: String, admStaffId: String) {
        repo.replyMessage(studentId, collegeMessageId, body, subject, collegeId, userId, admStaffId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {

                    if (isViewAttached)
                        view?.dismissDialog()

                }
                .subscribeWith(object : DisposableSingleObserver<JsonElement>() {
                    override fun onError(p0: Throwable) {
                        p0.printStackTrace()

                        if (isViewAttached)
                            view?.onFailedReply()
                    }

                    override fun onSuccess(p0: JsonElement) {

                        val obj = p0.asJsonObject

                        val response = obj.get("response").asString

                        if (response == "success") {

                            if (isViewAttached)
                                view?.onSuccessReply()

                        } else {

                            if (isViewAttached)
                                view?.onFailedReply()

                        }


                    }

                })
    }

}