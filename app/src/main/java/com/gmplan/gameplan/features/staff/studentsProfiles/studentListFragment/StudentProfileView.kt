package com.gmplan.gameplan.features.staff.studentsProfiles.studentListFragment

import android.app.Dialog
import com.gmplan.gameplan.data.pojos.StudentsCareers
import com.gmplan.gameplan.data.pojos.staffStudent.StaffStudent
import com.gmplan.gameplan.data.pojos.studentProfileDetails.StudentProfileDetails
import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Sammie on 6/6/2017.
 */
interface StudentProfileView : MvpView {

    fun dismissDialog()
    fun onErrorGettingStudents()
    fun onNoStudentFound()
    fun onStudentsFound(p0: List<StaffStudent>)
    fun onErrorGettingStudentDetail()
    fun onSuccessStudentDetail(p0: StudentProfileDetails)
    fun onErrorGettingFieldsValue()
    fun onSuccessGettingFieldsValue(schoolList: MutableList<String>, citiesList: MutableList<String>, countriesList: MutableList<String>, statesList: MutableList<String>)
    fun onErrorSearchStudent()
    fun onErrorGettingCareerList()
    fun onSuccessCareerList(careerList: List<StudentsCareers>)
    fun onErrorSearchingByCareer()

}