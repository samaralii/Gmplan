package com.gmplan.gameplan.service

import android.annotation.TargetApi
import android.app.IntentService
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.util.Log
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.source.TaskRepository
import com.gmplan.gameplan.infrastructure.GmplanApplication
import com.gmplan.gameplan.util.AppConst
import com.google.gson.JsonElement
import io.reactivex.observers.DisposableSingleObserver
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import javax.inject.Inject

class UpdateStatus : IntentService("UpdateStatusService") {

    private val TAG = UpdateStatus::class.java.name.toUpperCase()

    private val notification_id = "update_status_1"
    private val notification_title = "Updating Status"
    private val notification_description = "Uploading..."
    private val importance by lazy { NotificationManagerCompat.IMPORTANCE_LOW }


    private val mNotificationManager by lazy { getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager }
    private val notification by lazy { NotificationCompat.Builder(this, notification_id) }

    @Inject
    lateinit var repo: TaskRepository

    override fun onHandleIntent(intent: Intent?) {
        (application as GmplanApplication).component?.Inject(this)

        setupNotification()

        val imageFilePath = intent?.getStringExtra(AppConst.IMAGE_PATH)
        val collegeId = intent?.getStringExtra(AppConst.COLLEGE_ID)
        val userId = intent?.getStringExtra(AppConst.USER_ID)
        val status = intent?.getStringExtra(AppConst.STATUS)

        updateStatus(imageFilePath.toString(),
                collegeId.toString(),
                userId.toString(),
                status.toString())
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun setupNotification() {

        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.logo)

        notification.apply {
            setSmallIcon(R.drawable.ic_file_uploading)
            setLargeIcon(bitmap)
            setContentTitle(notification_title)
            setContentText(notification_description)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(notification_id, notification_title, importance)
            mNotificationManager.createNotificationChannel(mChannel)
        }

        mNotificationManager.notify(1, notification.build())
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun successNotification() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(notification_id, notification_title, importance)
            mNotificationManager.createNotificationChannel(mChannel)
        }

        notification.apply {
            setSmallIcon(R.drawable.ic_done)
            setContentText("Update Successfully")
        }

        mNotificationManager.notify(1, notification.build())
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun errorNotification() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(notification_id, notification_title, importance)
            mNotificationManager.createNotificationChannel(mChannel)
        }

        notification.apply {
            setSmallIcon(R.drawable.ic_file_uploading)
            setContentText("Error, Please try again.")
        }

        mNotificationManager.notify(1, notification.build())
    }

    private fun updateStatus(imageFilePath: String,
                             collegeId: String,
                             userId: String,
                             status: String) {

        val imageFile = File(imageFilePath)

        val _collegeId = RequestBody.create(
                MediaType.parse("multipart/form-data"), collegeId)

        val _userId = RequestBody.create(
                MediaType.parse("multipart/form-data"), userId)

        val _status = RequestBody.create(
                MediaType.parse("multipart/form-data"), status)

        Log.d(TAG, imageFile.absolutePath)

        var photoFile: MultipartBody.Part? = null

        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile)

        photoFile = MultipartBody.Part.createFormData("image", imageFile.name, requestFile)

        repo.updateStatus(_collegeId, _userId, _status, photoFile)
                .subscribeWith(object : DisposableSingleObserver<JsonElement>() {
                    override fun onError(p0: Throwable) {
                        p0.printStackTrace()
                        errorNotification()
                    }

                    override fun onSuccess(p0: JsonElement) {

                        try {

                            val obj = p0.asJsonObject
                            val response = obj.get("response").asString

                            if (response == "success") {
                                Log.d(TAG, "Success")
                                successNotification()
                            } else {
                                Log.d(TAG, "Error")
                                errorNotification()
                            }

                        } catch (e: Exception) {

                            e.printStackTrace()
                            errorNotification()

                        }

                    }
                })
    }

}