package com.gmplan.gameplan.viewholders

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import com.gmplan.gameplan.R

class JobZonesCareerVH(view: View): RecyclerView.ViewHolder(view) {
    val check by lazy { view.findViewById<CheckBox>(R.id.list_jobzones_check) }
    val jobs by lazy { view.findViewById<View>(R.id.list_jobzones_jobs) }
    val detail by lazy { view.findViewById<TextView>(R.id.list_jobzones_careersTitle) }
}