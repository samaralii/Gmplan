package com.gmplan.gameplan

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby.mvp.MvpView

interface EmptyView : MvpView
class EmptyPresenter : MvpBasePresenter<EmptyView>()