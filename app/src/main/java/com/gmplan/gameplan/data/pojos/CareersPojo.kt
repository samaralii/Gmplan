package com.gmplan.gameplan.data.pojos

import android.os.Parcel
import android.os.Parcelable
import com.gmplan.gameplan.data.pojos.industryCareersSearch.Career
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Sammie on 9/6/2017.
 */


data class _Career(val code: String, val title: String)

data class GetCareersAndStems(val careers: MutableList<_Career>? = null, val stemCareers: MutableList<_Career>? = null)

data class StemCareers(val stemCareer: MutableList<_Career>? = null)

data class Result(val area: String, val score: String, val description: String) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(area)
        writeString(score)
        writeString(description)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Result> = object : Parcelable.Creator<Result> {
            override fun createFromParcel(source: Parcel): Result = Result(source)
            override fun newArray(size: Int): Array<Result?> = arrayOfNulls(size)
        }
    }
}

data class QuestionsResult(val result: ArrayList<Result>) : Parcelable {
    constructor(source: Parcel) : this(
            source.createTypedArrayList(Result.CREATOR)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeTypedList(result)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<QuestionsResult> = object : Parcelable.Creator<QuestionsResult> {
            override fun createFromParcel(source: Parcel): QuestionsResult = QuestionsResult(source)
            override fun newArray(size: Int): Array<QuestionsResult?> = arrayOfNulls(size)
        }
    }
}

class Zones {

    @SerializedName("@value")
    @Expose
    var value: String? = null

    //    @SerializedName("@value")
//    @Expose
    var title: String? = null

    //    @SerializedName("@value")
//    @Expose
    var experience: String? = null

    //    @SerializedName("@value")
//    @Expose
    var education: String? = null

    //    @SerializedName("@value")
//    @Expose
    var job_training: String? = null

    //    @SerializedName("@value")
//    @Expose
    var examples: String? = null

    var svp_range: String? = null

}

data class JobZones(val zones: Zones, val average_salary: Any, val careers: ArrayList<Career> = arrayListOf())