package com.gmplan.gameplan.data.pojos

/**
 * Created by Sammie on 10/10/2017.
 */
data class SearchStudents(
        val staffid: String = "",
        val college_id: String = "",
        val name: String = "",
        val ugpa_from: String = "",
        val ugpa_to: String = "",
        val wgpa_from: String = "",
        val wgpa_to: String = "",
        val class_rank_from: String = "",
        val class_rank_to: String = "",
        val sat_score_from: String = "",
        val sat_score_to: String = "",
        val act_score_from: String = "",
        val act_score_to: String = "",
        val school: String = "",
        val city: String = ""
)