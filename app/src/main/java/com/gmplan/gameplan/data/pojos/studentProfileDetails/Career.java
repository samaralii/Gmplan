
package com.gmplan.gameplan.data.pojos.studentProfileDetails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Career implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("career_title")
    @Expose
    private String careerTitle;
    @SerializedName("career_code")
    @Expose
    private String careerCode;
    @SerializedName("job_zone")
    @Expose
    private String jobZone;

    protected Career(Parcel in) {
        id = in.readString();
        studentId = in.readString();
        careerTitle = in.readString();
        careerCode = in.readString();
        jobZone = in.readString();
    }

    public static final Creator<Career> CREATOR = new Creator<Career>() {
        @Override
        public Career createFromParcel(Parcel in) {
            return new Career(in);
        }

        @Override
        public Career[] newArray(int size) {
            return new Career[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getCareerTitle() {
        return careerTitle;
    }

    public void setCareerTitle(String careerTitle) {
        this.careerTitle = careerTitle;
    }

    public String getCareerCode() {
        return careerCode;
    }

    public void setCareerCode(String careerCode) {
        this.careerCode = careerCode;
    }

    public String getJobZone() {
        return jobZone;
    }

    public void setJobZone(String jobZone) {
        this.jobZone = jobZone;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(studentId);
        dest.writeString(careerTitle);
        dest.writeString(careerCode);
        dest.writeString(jobZone);
    }
}
