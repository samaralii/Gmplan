package com.gmplan.gameplan.data.pojos

import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import com.gmplan.gameplan.R


data class DashboardItems(val id: Int, @DrawableRes val imageId: Int,
                          @StringRes val nameId: Int, val color: String)