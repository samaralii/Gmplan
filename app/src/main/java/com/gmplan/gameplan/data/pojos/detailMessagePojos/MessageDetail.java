
package com.gmplan.gameplan.data.pojos.detailMessagePojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageDetail {

    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("messages")
    @Expose
    private Messages messages;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Messages getMessages() {
        return messages;
    }

    public void setMessages(Messages messages) {
        this.messages = messages;
    }

}
