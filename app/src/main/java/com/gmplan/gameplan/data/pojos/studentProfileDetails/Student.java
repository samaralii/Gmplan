
package com.gmplan.gameplan.data.pojos.studentProfileDetails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Student implements Parcelable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("middle_name")
    @Expose
    private String middleName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("grade")
    @Expose
    private String grade;
    @SerializedName("ugpa")
    @Expose
    private String ugpa;
    @SerializedName("wgpa")
    @Expose
    private String wgpa;
    @SerializedName("class_rank")
    @Expose
    private String classRank;
    @SerializedName("views")
    @Expose
    private String views;
    @SerializedName("reco")
    @Expose
    private String reco;
    @SerializedName("sat_score")
    @Expose
    private String satScore;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("cons_id")
    @Expose
    private String consId;
    @SerializedName("info")
    @Expose
    private String info;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("photo_dir")
    @Expose
    private String photoDir;
    @SerializedName("my_like")
    @Expose
    private String myLike;
    @SerializedName("my_dislike")
    @Expose
    private String myDislike;
    @SerializedName("my_music")
    @Expose
    private String myMusic;
    @SerializedName("my_team")
    @Expose
    private String myTeam;
    @SerializedName("my_college")
    @Expose
    private String myCollege;
    @SerializedName("my_school")
    @Expose
    private String mySchool;
    @SerializedName("school_id")
    @Expose
    private String schoolId;
    @SerializedName("step")
    @Expose
    private String step;
    @SerializedName("act_score")
    @Expose
    private String actScore;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("county_id")
    @Expose
    private String countyId;
    @SerializedName("state_id")
    @Expose
    private String stateId;
    @SerializedName("organization")
    @Expose
    private String organization;
    @SerializedName("fullname")
    @Expose
    private String fullname;

    protected Student(Parcel in) {
        id = in.readString();
        firstName = in.readString();
        middleName = in.readString();
        lastName = in.readString();
        grade = in.readString();
        ugpa = in.readString();
        wgpa = in.readString();
        classRank = in.readString();
        views = in.readString();
        reco = in.readString();
        satScore = in.readString();
        userId = in.readString();
        consId = in.readString();
        info = in.readString();
        photo = in.readString();
        photoDir = in.readString();
        myLike = in.readString();
        myDislike = in.readString();
        myMusic = in.readString();
        myTeam = in.readString();
        myCollege = in.readString();
        mySchool = in.readString();
        schoolId = in.readString();
        step = in.readString();
        actScore = in.readString();
        cityId = in.readString();
        countyId = in.readString();
        stateId = in.readString();
        organization = in.readString();
        fullname = in.readString();
    }

    public static final Creator<Student> CREATOR = new Creator<Student>() {
        @Override
        public Student createFromParcel(Parcel in) {
            return new Student(in);
        }

        @Override
        public Student[] newArray(int size) {
            return new Student[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getUgpa() {
        return ugpa;
    }

    public void setUgpa(String ugpa) {
        this.ugpa = ugpa;
    }

    public String getWgpa() {
        return wgpa;
    }

    public void setWgpa(String wgpa) {
        this.wgpa = wgpa;
    }

    public String getClassRank() {
        return classRank;
    }

    public void setClassRank(String classRank) {
        this.classRank = classRank;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getReco() {
        return reco;
    }

    public void setReco(String reco) {
        this.reco = reco;
    }

    public String getSatScore() {
        return satScore;
    }

    public void setSatScore(String satScore) {
        this.satScore = satScore;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getConsId() {
        return consId;
    }

    public void setConsId(String consId) {
        this.consId = consId;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhotoDir() {
        return photoDir;
    }

    public void setPhotoDir(String photoDir) {
        this.photoDir = photoDir;
    }

    public String getMyLike() {
        return myLike;
    }

    public void setMyLike(String myLike) {
        this.myLike = myLike;
    }

    public String getMyDislike() {
        return myDislike;
    }

    public void setMyDislike(String myDislike) {
        this.myDislike = myDislike;
    }

    public String getMyMusic() {
        return myMusic;
    }

    public void setMyMusic(String myMusic) {
        this.myMusic = myMusic;
    }

    public String getMyTeam() {
        return myTeam;
    }

    public void setMyTeam(String myTeam) {
        this.myTeam = myTeam;
    }

    public String getMyCollege() {
        return myCollege;
    }

    public void setMyCollege(String myCollege) {
        this.myCollege = myCollege;
    }

    public String getMySchool() {
        return mySchool;
    }

    public void setMySchool(String mySchool) {
        this.mySchool = mySchool;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getActScore() {
        return actScore;
    }

    public void setActScore(String actScore) {
        this.actScore = actScore;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCountyId() {
        return countyId;
    }

    public void setCountyId(String countyId) {
        this.countyId = countyId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(firstName);
        dest.writeString(middleName);
        dest.writeString(lastName);
        dest.writeString(grade);
        dest.writeString(ugpa);
        dest.writeString(wgpa);
        dest.writeString(classRank);
        dest.writeString(views);
        dest.writeString(reco);
        dest.writeString(satScore);
        dest.writeString(userId);
        dest.writeString(consId);
        dest.writeString(info);
        dest.writeString(photo);
        dest.writeString(photoDir);
        dest.writeString(myLike);
        dest.writeString(myDislike);
        dest.writeString(myMusic);
        dest.writeString(myTeam);
        dest.writeString(myCollege);
        dest.writeString(mySchool);
        dest.writeString(schoolId);
        dest.writeString(step);
        dest.writeString(actScore);
        dest.writeString(cityId);
        dest.writeString(countyId);
        dest.writeString(stateId);
        dest.writeString(organization);
        dest.writeString(fullname);
    }
}
