
package com.gmplan.gameplan.data.pojos.studentProfileDetails;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Studentinfo  implements Parcelable {

    @SerializedName("Student")
    @Expose
    private Student student;
    @SerializedName("User")
    @Expose
    private User user;
    @SerializedName("School")
    @Expose
    private School school;
    @SerializedName("City")
    @Expose
    private City city;
    @SerializedName("County")
    @Expose
    private County county;
    @SerializedName("State")
    @Expose
    private State state;
    @SerializedName("Answer")
    @Expose
    private Answer answer;
    @SerializedName("Career")
    @Expose
    private List<Career> career = null;
    @SerializedName("RequestS")
    @Expose
    private List<Object> requestS = null;
    @SerializedName("RequestC")
    @Expose
    private List<Object> requestC = null;
    @SerializedName("Friend")
    @Expose
    private List<Object> friend = null;
    @SerializedName("StudentMessage")
    @Expose
    private List<StudentMessage> studentMessage = null;
    @SerializedName("Video")
    @Expose
    private List<Video> video = null;
    @SerializedName("Achievement")
    @Expose
    private List<Achievement> achievement = null;
    @SerializedName("CollegeMessage")
    @Expose
    private List<CollegeMessage> collegeMessage = null;
    @SerializedName("SemesterGoal")
    @Expose
    private List<SemesterGoal> semesterGoal = null;
    @SerializedName("Recruiter")
    @Expose
    private List<Object> recruiter = null;
    @SerializedName("Offer")
    @Expose
    private List<Offer> offer = null;
    @SerializedName("ProfileView")
    @Expose
    private List<ProfileView> profileView = null;

    protected Studentinfo(Parcel in) {
        student = in.readParcelable(Student.class.getClassLoader());
        user = in.readParcelable(User.class.getClassLoader());
        school = in.readParcelable(School.class.getClassLoader());
        city = in.readParcelable(City.class.getClassLoader());
        county = in.readParcelable(County.class.getClassLoader());
        state = in.readParcelable(State.class.getClassLoader());
        answer = in.readParcelable(Answer.class.getClassLoader());
        career = in.createTypedArrayList(Career.CREATOR);
        studentMessage = in.createTypedArrayList(StudentMessage.CREATOR);
        video = in.createTypedArrayList(Video.CREATOR);
        achievement = in.createTypedArrayList(Achievement.CREATOR);
        collegeMessage = in.createTypedArrayList(CollegeMessage.CREATOR);
        semesterGoal = in.createTypedArrayList(SemesterGoal.CREATOR);
        offer = in.createTypedArrayList(Offer.CREATOR);
        profileView = in.createTypedArrayList(ProfileView.CREATOR);
    }

    public static final Creator<Studentinfo> CREATOR = new Creator<Studentinfo>() {
        @Override
        public Studentinfo createFromParcel(Parcel in) {
            return new Studentinfo(in);
        }

        @Override
        public Studentinfo[] newArray(int size) {
            return new Studentinfo[size];
        }
    };

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public County getCounty() {
        return county;
    }

    public void setCounty(County county) {
        this.county = county;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public List<Career> getCareer() {
        return career;
    }

    public void setCareer(List<Career> career) {
        this.career = career;
    }

    public List<Object> getRequestS() {
        return requestS;
    }

    public void setRequestS(List<Object> requestS) {
        this.requestS = requestS;
    }

    public List<Object> getRequestC() {
        return requestC;
    }

    public void setRequestC(List<Object> requestC) {
        this.requestC = requestC;
    }

    public List<Object> getFriend() {
        return friend;
    }

    public void setFriend(List<Object> friend) {
        this.friend = friend;
    }

    public List<StudentMessage> getStudentMessage() {
        return studentMessage;
    }

    public void setStudentMessage(List<StudentMessage> studentMessage) {
        this.studentMessage = studentMessage;
    }

    public List<Video> getVideo() {
        return video;
    }

    public void setVideo(List<Video> video) {
        this.video = video;
    }

    public List<Achievement> getAchievement() {
        return achievement;
    }

    public void setAchievement(List<Achievement> achievement) {
        this.achievement = achievement;
    }

    public List<CollegeMessage> getCollegeMessage() {
        return collegeMessage;
    }

    public void setCollegeMessage(List<CollegeMessage> collegeMessage) {
        this.collegeMessage = collegeMessage;
    }

    public List<SemesterGoal> getSemesterGoal() {
        return semesterGoal;
    }

    public void setSemesterGoal(List<SemesterGoal> semesterGoal) {
        this.semesterGoal = semesterGoal;
    }

    public List<Object> getRecruiter() {
        return recruiter;
    }

    public void setRecruiter(List<Object> recruiter) {
        this.recruiter = recruiter;
    }

    public List<Offer> getOffer() {
        return offer;
    }

    public void setOffer(List<Offer> offer) {
        this.offer = offer;
    }

    public List<ProfileView> getProfileView() {
        return profileView;
    }

    public void setProfileView(List<ProfileView> profileView) {
        this.profileView = profileView;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(student, flags);
        dest.writeParcelable(user, flags);
        dest.writeParcelable(school, flags);
        dest.writeParcelable(city, flags);
        dest.writeParcelable(county, flags);
        dest.writeParcelable(state, flags);
        dest.writeParcelable(answer, flags);
        dest.writeTypedList(career);
        dest.writeTypedList(studentMessage);
        dest.writeTypedList(video);
        dest.writeTypedList(achievement);
        dest.writeTypedList(collegeMessage);
        dest.writeTypedList(semesterGoal);
        dest.writeTypedList(offer);
        dest.writeTypedList(profileView);
    }
}
