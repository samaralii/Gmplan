
package com.gmplan.gameplan.data.pojos.studentProfileDetails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CollegeMessage implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("college_id")
    @Expose
    private String collegeId;
    @SerializedName("message_id")
    @Expose
    private String messageId;
    @SerializedName("admission_staff_id")
    @Expose
    private String admissionStaffId;
    @SerializedName("is_read")
    @Expose
    private boolean isRead;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("college_message_id")
    @Expose
    private String collegeMessageId;

    protected CollegeMessage(Parcel in) {
        id = in.readString();
        studentId = in.readString();
        collegeId = in.readString();
        messageId = in.readString();
        admissionStaffId = in.readString();
        isRead = in.readByte() != 0;
        userId = in.readString();
        collegeMessageId = in.readString();
    }

    public static final Creator<CollegeMessage> CREATOR = new Creator<CollegeMessage>() {
        @Override
        public CollegeMessage createFromParcel(Parcel in) {
            return new CollegeMessage(in);
        }

        @Override
        public CollegeMessage[] newArray(int size) {
            return new CollegeMessage[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getCollegeId() {
        return collegeId;
    }

    public void setCollegeId(String collegeId) {
        this.collegeId = collegeId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getAdmissionStaffId() {
        return admissionStaffId;
    }

    public void setAdmissionStaffId(String admissionStaffId) {
        this.admissionStaffId = admissionStaffId;
    }

    public boolean isIsRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCollegeMessageId() {
        return collegeMessageId;
    }

    public void setCollegeMessageId(String collegeMessageId) {
        this.collegeMessageId = collegeMessageId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(studentId);
        dest.writeString(collegeId);
        dest.writeString(messageId);
        dest.writeString(admissionStaffId);
        dest.writeByte((byte) (isRead ? 1 : 0));
        dest.writeString(userId);
        dest.writeString(collegeMessageId);
    }
}
