
package com.gmplan.gameplan.data.pojos.studentProfileDetails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Studentcollege  implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("college_name")
    @Expose
    private String collegeName;
    @SerializedName("college_id")
    @Expose
    private String collegeId;
    @SerializedName("student_id")
    @Expose
    private String studentId;

    protected Studentcollege(Parcel in) {
        id = in.readString();
        collegeName = in.readString();
        collegeId = in.readString();
        studentId = in.readString();
    }

    public static final Creator<Studentcollege> CREATOR = new Creator<Studentcollege>() {
        @Override
        public Studentcollege createFromParcel(Parcel in) {
            return new Studentcollege(in);
        }

        @Override
        public Studentcollege[] newArray(int size) {
            return new Studentcollege[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public String getCollegeId() {
        return collegeId;
    }

    public void setCollegeId(String collegeId) {
        this.collegeId = collegeId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(collegeName);
        dest.writeString(collegeId);
        dest.writeString(studentId);
    }
}
