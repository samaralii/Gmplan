
package com.gmplan.gameplan.data.pojos;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GmpMessages {

    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("gmessages")
    @Expose
    private List<Gmessage> gmessages = null;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<Gmessage> getGmessages() {
        return gmessages;
    }

    public void setGmessages(List<Gmessage> gmessages) {
        this.gmessages = gmessages;
    }

}
