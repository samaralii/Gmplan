
package com.gmplan.gameplan.data.pojos.industryCareersSearch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tags {

    @SerializedName("@bright_outlook")
    @Expose
    private String brightOutlook;
    @SerializedName("@green")
    @Expose
    private String green;
    @SerializedName("@apprenticeship")
    @Expose
    private String apprenticeship;

    public String getBrightOutlook() {
        return brightOutlook;
    }

    public void setBrightOutlook(String brightOutlook) {
        this.brightOutlook = brightOutlook;
    }

    public String getGreen() {
        return green;
    }

    public void setGreen(String green) {
        this.green = green;
    }

    public String getApprenticeship() {
        return apprenticeship;
    }

    public void setApprenticeship(String apprenticeship) {
        this.apprenticeship = apprenticeship;
    }

}
