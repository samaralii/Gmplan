
package com.gmplan.gameplan.data.pojos.userPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SecretQuestion1 {

    @SerializedName("imageId")
    @Expose
    private String id;
    @SerializedName("nameId")
    @Expose
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
