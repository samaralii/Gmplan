
package com.gmplan.gameplan.data.pojos.studentProfileDetails;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StudentProfileDetails  implements Parcelable {

    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("studentinfo")
    @Expose
    private Studentinfo studentinfo;
    @SerializedName("studentcolleges")
    @Expose
    private List<Studentcollege> studentcolleges = null;
    @SerializedName("rating")
    @Expose
    private List<Rating> rating = null;

    protected StudentProfileDetails(Parcel in) {
        response = in.readString();
        studentinfo = in.readParcelable(Studentinfo.class.getClassLoader());
        studentcolleges = in.createTypedArrayList(Studentcollege.CREATOR);
        rating = in.createTypedArrayList(Rating.CREATOR);
    }

    public static final Creator<StudentProfileDetails> CREATOR = new Creator<StudentProfileDetails>() {
        @Override
        public StudentProfileDetails createFromParcel(Parcel in) {
            return new StudentProfileDetails(in);
        }

        @Override
        public StudentProfileDetails[] newArray(int size) {
            return new StudentProfileDetails[size];
        }
    };

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Studentinfo getStudentinfo() {
        return studentinfo;
    }

    public void setStudentinfo(Studentinfo studentinfo) {
        this.studentinfo = studentinfo;
    }

    public List<Studentcollege> getStudentcolleges() {
        return studentcolleges;
    }

    public void setStudentcolleges(List<Studentcollege> studentcolleges) {
        this.studentcolleges = studentcolleges;
    }

    public List<Rating> getRating() {
        return rating;
    }

    public void setRating(List<Rating> rating) {
        this.rating = rating;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(response);
        dest.writeParcelable(studentinfo, flags);
        dest.writeTypedList(studentcolleges);
        dest.writeTypedList(rating);
    }
}
