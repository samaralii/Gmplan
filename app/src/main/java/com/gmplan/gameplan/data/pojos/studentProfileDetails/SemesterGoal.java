
package com.gmplan.gameplan.data.pojos.studentProfileDetails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SemesterGoal implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("detail")
    @Expose
    private String detail;
    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("semester")
    @Expose
    private String semester;
    @SerializedName("grade")
    @Expose
    private String grade;

    protected SemesterGoal(Parcel in) {
        id = in.readString();
        detail = in.readString();
        studentId = in.readString();
        title = in.readString();
        semester = in.readString();
        grade = in.readString();
    }

    public static final Creator<SemesterGoal> CREATOR = new Creator<SemesterGoal>() {
        @Override
        public SemesterGoal createFromParcel(Parcel in) {
            return new SemesterGoal(in);
        }

        @Override
        public SemesterGoal[] newArray(int size) {
            return new SemesterGoal[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(detail);
        dest.writeString(studentId);
        dest.writeString(title);
        dest.writeString(semester);
        dest.writeString(grade);
    }
}
