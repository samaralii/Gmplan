package com.gmplan.gameplan.data.pojos

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Sammie on 9/29/2017.
 */
class EventsNotification(val text: String, val duration: String) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(text)
        writeString(duration)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<EventsNotification> = object : Parcelable.Creator<EventsNotification> {
            override fun createFromParcel(source: Parcel): EventsNotification = EventsNotification(source)
            override fun newArray(size: Int): Array<EventsNotification?> = arrayOfNulls(size)
        }
    }
}