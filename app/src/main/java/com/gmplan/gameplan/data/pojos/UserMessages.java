
package com.gmplan.gameplan.data.pojos;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserMessages {

    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("messages")
    @Expose
    private List<ShortMessageDetail> messages = null;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<ShortMessageDetail> getMessages() {
        return messages;
    }

    public void setMessages(List<ShortMessageDetail> messages) {
        this.messages = messages;
    }

}
