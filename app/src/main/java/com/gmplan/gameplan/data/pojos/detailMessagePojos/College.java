
package com.gmplan.gameplan.data.pojos.detailMessagePojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class College {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("state_id")
    @Expose
    private String stateId;
    @SerializedName("county_id")
    @Expose
    private String countyId;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("permission_profile_id")
    @Expose
    private String permissionProfileId;
    @SerializedName("type")
    @Expose
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getCountyId() {
        return countyId;
    }

    public void setCountyId(String countyId) {
        this.countyId = countyId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getPermissionProfileId() {
        return permissionProfileId;
    }

    public void setPermissionProfileId(String permissionProfileId) {
        this.permissionProfileId = permissionProfileId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
