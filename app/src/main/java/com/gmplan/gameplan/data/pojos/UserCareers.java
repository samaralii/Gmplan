
package com.gmplan.gameplan.data.pojos;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserCareers {

    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("careers")
    @Expose
    private List<Career> careers = null;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<Career> getCareers() {
        return careers;
    }

    public void setCareers(List<Career> careers) {
        this.careers = careers;
    }

}
