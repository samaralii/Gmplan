
package com.gmplan.gameplan.data.pojos.studentProfileDetails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileView  implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("college_id")
    @Expose
    private String collegeId;
    @SerializedName("views")
    @Expose
    private String views;
    @SerializedName("admission_staff_id")
    @Expose
    private String admissionStaffId;

    protected ProfileView(Parcel in) {
        id = in.readString();
        studentId = in.readString();
        collegeId = in.readString();
        views = in.readString();
        admissionStaffId = in.readString();
    }

    public static final Creator<ProfileView> CREATOR = new Creator<ProfileView>() {
        @Override
        public ProfileView createFromParcel(Parcel in) {
            return new ProfileView(in);
        }

        @Override
        public ProfileView[] newArray(int size) {
            return new ProfileView[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getCollegeId() {
        return collegeId;
    }

    public void setCollegeId(String collegeId) {
        this.collegeId = collegeId;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getAdmissionStaffId() {
        return admissionStaffId;
    }

    public void setAdmissionStaffId(String admissionStaffId) {
        this.admissionStaffId = admissionStaffId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(studentId);
        dest.writeString(collegeId);
        dest.writeString(views);
        dest.writeString(admissionStaffId);
    }
}
