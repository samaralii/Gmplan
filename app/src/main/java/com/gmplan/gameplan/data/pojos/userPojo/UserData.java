
package com.gmplan.gameplan.data.pojos.userPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserData {

    @SerializedName("User")
    @Expose
    private User user;
    @SerializedName("Group")
    @Expose
    private Group group;
    @SerializedName("SecretQuestion1")
    @Expose
    private SecretQuestion1 secretQuestion1;
    @SerializedName("SecretQuestion2")
    @Expose
    private SecretQuestion2 secretQuestion2;
    @SerializedName("Student")
    @Expose
    private Student student;
    @SerializedName("Institution")
    @Expose
    private Institution institution;
    @SerializedName("AdmissionStaff")
    @Expose
    private AdmissionStaff admissionStaff;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public SecretQuestion1 getSecretQuestion1() {
        return secretQuestion1;
    }

    public void setSecretQuestion1(SecretQuestion1 secretQuestion1) {
        this.secretQuestion1 = secretQuestion1;
    }

    public SecretQuestion2 getSecretQuestion2() {
        return secretQuestion2;
    }

    public void setSecretQuestion2(SecretQuestion2 secretQuestion2) {
        this.secretQuestion2 = secretQuestion2;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public AdmissionStaff getAdmissionStaff() {
        return admissionStaff;
    }

    public void setAdmissionStaff(AdmissionStaff admissionStaff) {
        this.admissionStaff = admissionStaff;
    }

}
