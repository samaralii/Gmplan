package com.gmplan.gameplan.data.pojos

data class Questions(val question: String, val answer: String)