
package com.gmplan.gameplan.data.pojos.staffStudent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Answer {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("ip_answer")
    @Expose
    private String ipAnswer;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getIpAnswer() {
        return ipAnswer;
    }

    public void setIpAnswer(String ipAnswer) {
        this.ipAnswer = ipAnswer;
    }

}
