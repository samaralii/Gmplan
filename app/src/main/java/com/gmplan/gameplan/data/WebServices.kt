package com.gmplan.gameplan.data

import com.gmplan.gameplan.data.pojos.*
import com.gmplan.gameplan.data.pojos.careersJobs.CareersJobsPojo
import com.gmplan.gameplan.data.pojos.detailMessagePojos.MessageDetail
import com.gmplan.gameplan.data.pojos.fieldsValue.FieldsValue
import com.gmplan.gameplan.data.pojos.industryCareersSearch.IndustryCareers
import com.gmplan.gameplan.data.pojos.interestProfileQuestions.QuestionsPojo
import com.gmplan.gameplan.data.pojos.staffFeed.StaffNewssFeed
import com.gmplan.gameplan.data.pojos.staffStudent.StaffStudent
import com.gmplan.gameplan.data.pojos.studentProfileDetails.StudentProfileDetails
import com.gmplan.gameplan.data.pojos.userPojo.UserPojo
import com.google.gson.JsonElement

import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

/**
 * Created by Sammie on 3/8/2017.
 */

interface WebServices {

    @GET
    fun testApi(@Url url: String): Single<Response<String>>

    @FormUrlEncoded
    @POST("users/api_loginservice")
    fun LoginUser(@Field("email") email: String,
                  @Field("password") password: String,
                  @Field("playerid") playerId: String
    ): Single<Response<UserPojo>>


    @FormUrlEncoded
    @POST("students/api_pro_views")
    fun userProfileViews(@Field("student_id") studentId: String): Single<Response<ProfileViewsPojo>>

    @FormUrlEncoded
    @POST("students/api_my_recruiters")
    fun userRecruiters(@Field("student_id") studentId: String): Single<Response<MyRecruitersPojo>>

    @Multipart
    @POST("students/api_edituser")
    fun editUserProfile(
            @Part("student_id") id: RequestBody,
            @Part("my_likes") myLikes: RequestBody,
            @Part("my_dislikes") myDislikes: RequestBody,
            @Part("my_music") myMusic: RequestBody,
            @Part("my_team") myTeam: RequestBody,
            @Part("mycollege") myCollege: RequestBody,
            @Part image: MultipartBody.Part?
    ): Single<Response<JsonElement>>


    @FormUrlEncoded
    @POST("careers/api_mycareers")
    fun getUserCareers(@Field("student_id") studentId: String): Single<Response<UserCareers>>

    @FormUrlEncoded
    @POST("careers/api_mycareer_details")
    fun getCareersDetail(@Field("code") code: String): Single<Response<JsonElement>>

    @FormUrlEncoded
    @POST("careers/api_mycareer_delete")
    fun deleteCareer(@Field("career_id") careerId: String): Single<Response<JsonElement>>


    @FormUrlEncoded
    @POST("mycolleges/api_my_college")
    fun getUserColleges(@Field("student_id") studentId: String): Single<Response<UserColleges>>

    @FormUrlEncoded
    @POST("SemesterGoals/api_addsemester_goal")
    fun addSemester(@Field("student_id") studentId: String,
                    @Field("title") title: String,
                    @Field("grade") grade: String,
                    @Field("semester") semester: String,
                    @Field("detail") detail: String): Single<Response<JsonElement>>

    @FormUrlEncoded
    @POST("SemesterGoals/api_mysemester_goals")
    fun getUserSemestersGoals(@Field("student_id") studentId: String): Single<Response<UsersSemestersGoals>>


    @FormUrlEncoded
    @POST("SemesterGoals/api_delete_mysemester")
    fun deleteSemester(@Field("goal_id") goalId: String): Single<Response<JsonElement>>


    @FormUrlEncoded
    @POST("Highlights/api_videos")
    fun userVideos(@Field("student_id") studentId: String): Single<Response<UserVideos>>

    @FormUrlEncoded
    @POST("Highlights/api_edit_video")
    fun editVideo(@Field("video_id") videoId: String, @Field("url") url: String,
                  @Field("name") name: String): Single<Response<JsonElement>>

    @FormUrlEncoded
    @POST("Highlights/api_delete_video")
    fun deleteVideo(@Field("video_id") videoId: String): Single<Response<JsonElement>>

    @FormUrlEncoded
    @POST("Highlights/api_post_video")
    fun addVideo(@Field("student_id") studentId: String,
                 @Field("url") url: String,
                 @Field("name") name: String): Single<Response<JsonElement>>


    @FormUrlEncoded
    @POST("Highlights/api_achievement")
    fun userAchievements(@Field("student_id") studentId: String): Single<Response<UserAchievements>>

    @FormUrlEncoded
    @POST("Highlights/api_post_achievement")
    fun postAchievement(@Field("student_id") studentId: String,
                        @Field("title") title: String, @Field("detail") detail: String): Single<Response<JsonElement>>


    @FormUrlEncoded
    @POST("Messages/api_messages")
    fun userMessages(@Field("student_id") studentId: String): Single<Response<UserMessages>>

    @FormUrlEncoded
    @POST("Messages/api_view_message")
    fun messageDetail(@Field("message_id") messageId: String): Single<Response<MessageDetail>>


    @FormUrlEncoded
    @POST("messages/api_reply")
    fun replyMessage(
            @Field("student_id") studentId: String,
            @Field("college_message_id") collegeMessageId: String,
            @Field("body") body: String,
            @Field("subject") subject: String,
            @Field("college_id") collegeId: String,
            @Field("user_id") userId: String,
            @Field("admission_staff_id") admStaffId: String
    ): Single<Response<JsonElement>>


    @FormUrlEncoded
    @POST("Messages/api_gpmessages")
    fun userGmpMessages(@Field("student_id") studentId: String): Single<Response<GmpMessages>>


    @FormUrlEncoded
    @POST("students/staff_college_student")
    fun getStaffStudents(@Field("staffid") staffId: String,
                         @Field("college_id") collegeId: String): Single<Response<List<StaffStudent>>>

    @FormUrlEncoded
    @POST("students/staff_student_view")
    fun getUserProfileDetail(@Field("staffid") staffId: String,
                             @Field("college_id") collegeId: String,
                             @Field("student_id") studentId: String): Single<Response<StudentProfileDetails>>

    @FormUrlEncoded
    @POST("students/staff_permission_profile")
    fun getStudentFieldValues(@Field("staffid") staffid: String): Single<Response<FieldsValue>>

    @FormUrlEncoded
    @POST("students/staff_search_student")
    fun staffSearchStudent(@Field("staffid") StaffId: String, @Field("collegeid") collegeId: String,
                           @Field("name") name: String): Single<Response<List<StaffStudent>>>

    @FormUrlEncoded
    @POST("students/staff_list_careers")
    fun getStudentsSearchCareers(@Field("key") key: String = "yes"): Single<Response<List<StudentsCareers>>>

    @FormUrlEncoded
    @POST("students/staff_serach_by_career")
    fun staffSearchStudentByCareer(@Field("staffid") staffId: String,
                                   @Field("career_code") careerCode: String): Single<Response<List<StaffStudent>>>

    @FormUrlEncoded
    @POST("Messages/staff_send_to_student")
    fun staffSendMessage(@Field("college_id") collegeId: String, @Field("staffid") staffId: String, @Field("student_id") studentId: String,
                         @Field("staff_name") staffName: String, @Field("staff_title") staffTitle: String,
                         @Field("staff_college") staffCollege: String, @Field("userid") userId: String,
                         @Field("subject") subject: String, @Field("message") message: String): Single<Response<JsonElement>>


    @Multipart
    @POST("NewsFeeds/api_staff_update_status")
    fun updateStatus(
            @Part("collegeid") collegeId: RequestBody,
            @Part("userid") userId: RequestBody,
            @Part("status") status: RequestBody,
            @Part image: MultipartBody.Part
    ): Single<Response<JsonElement>>

    @FormUrlEncoded
    @POST("AdmissionStaffs/staff_newsfeeds")
    fun staffFeeds(@Field("key") key: String = "yes"): Single<Response<List<StaffNewssFeed>>>

    @FormUrlEncoded
    @POST("careers/api_jobs")
    fun getCareersJobs(@Field("studentSchoolId") schoolId: String,
                       @Field("career_title") careerTitle: String): Single<Response<CareersJobsPojo>>

    @FormUrlEncoded
    @POST("students/api_interest_profile")
    fun getInterestProfileQuestions(@Field("studentId") studentId: String): Single<Response<QuestionsPojo>>

    @FormUrlEncoded
    @POST("careers/api_get_careers")
    fun getCareersAndStemsList(@Field("key") key: String = "allow"): Single<Response<GetCareersAndStems>>

    @FormUrlEncoded
    @POST("careers/api_industry_careers")
    fun findIndustryCareers(@Field("careerId") careerId: String, @Field("studentId") studentId: String): Single<Response<IndustryCareers>>

    @FormUrlEncoded
    @POST("careers/api_keyword_careers")
    fun searchCareerByKeyword(@Field("keyword") keyword: String, @Field("studentId") studentId: String): Single<Response<IndustryCareers>>


    @FormUrlEncoded
    @POST("careers/api_stem_careers")
    fun getStemCareers(@Field("stemCareer") stemId: String): Single<Response<StemCareers>>

    @FormUrlEncoded
    @POST("careers/api_addcareer")
    fun addCareers(@Field("studentId") studentId: String, @Field("careerTitle") careerTitle: String,
                   @Field("careerCode") careerCode: String): Single<Response<JsonElement>>


    @FormUrlEncoded
    @POST("Students/api_save_answers")
    fun getResults(@Field("studentId") studentId: String, @Field("new_answers") newAnswer: String,
                   @Field("save_answers") saveAnswer: String = "1"): Single<Response<QuestionsResult>>


    @FormUrlEncoded
    @POST("Students/api_job_zones")
    fun getJobZones(@Field("studentid") studentId: String, @Field("jobzone") jobZone: String): Single<Response<JobZones>>

    @FormUrlEncoded
    @POST("events/api_staff_agenda")
    fun getStaffEvents(@Field("staff_id") staffId: String): Single<Response<EventPojo>>

    @FormUrlEncoded
    @POST("events/api_student_agenda")
    fun getStudentEvents(@Field("student_id") studentId: String): Single<Response<EventPojo>>

    @FormUrlEncoded
    @POST("Careers/api_add_careers")
    fun addJobZoneCareers(@Field("careerjson") json: String, @Field("student_id") studentId: String,
                          @Field("job_zone") jobZone: String): Single<Response<JsonElement>>


    @FormUrlEncoded
    @POST("events/api_get_checklist")
    fun getCheckList(@Field("event_id") eventId: String): Single<Response<CheckLists>>

    @FormUrlEncoded
    @POST("events/api_get_events_bydate")
    fun getEventByDate(@Field("student_id") studentId: String, @Field("date") date: String,
                       @Field("usertype") usertype: String = "student"): Single<Response<EventByDate>>

    @FormUrlEncoded
    @POST("events/api_get_events_bydate")
    fun getStaffEventByDate(@Field("staff_id") studentId: String,
                            @Field("date") date: String, @Field("usertype") usertype: String = "staff"): Single<Response<EventByDate>>

    @FormUrlEncoded
    @POST("events/api_student_event")
    fun createAgendaEvent(@Field("eventJson") json: String): Single<Response<JsonElement>>

    @FormUrlEncoded
    @POST("events/api_add_staff_event")
    fun createStaffAgendaEvent(@Field("eventJson") json: String, @Field("staff_id") staffId: String): Single<Response<JsonElement>>

    @FormUrlEncoded
    @POST("events/staff_api_advancesearch")
    fun searchStudents(@FieldMap students: Map<String, String>): Single<Response<List<StaffStudent>>>

    @FormUrlEncoded
    @POST("students/staff_get_schools")
    fun getSchools(@Field("staffid") staffId: String,
                   @Field("collegeid") collegeId: String): Single<Response<List<AllSchool>>>

    @FormUrlEncoded
    @POST("students/staff_inbox")
    fun getInboxList(@Field("collegeId") collegeId: String): Single<Response<List<MessagesObject>>>

    @FormUrlEncoded
    @POST("students/staff_outgoing")
    fun getOutboxList(@Field("staffId") staffId: String): Single<Response<List<MessagesObject>>>


}
