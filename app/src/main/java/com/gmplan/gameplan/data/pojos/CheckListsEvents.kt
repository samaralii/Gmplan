package com.gmplan.gameplan.data.pojos

import android.os.Parcel
import android.os.Parcelable

data class CheckListsEvents(val event: String, val startDateTime: String, val endDateTime: String) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(event)
        writeString(startDateTime)
        writeString(endDateTime)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CheckListsEvents> = object : Parcelable.Creator<CheckListsEvents> {
            override fun createFromParcel(source: Parcel): CheckListsEvents = CheckListsEvents(source)
            override fun newArray(size: Int): Array<CheckListsEvents?> = arrayOfNulls(size)
        }
    }
}