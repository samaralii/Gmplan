package com.gmplan.gameplan.data.pojos

data class MessagesObject(val message_id: String?, val subject: String?,
                          val photo: String?, val college_message_id: String?)