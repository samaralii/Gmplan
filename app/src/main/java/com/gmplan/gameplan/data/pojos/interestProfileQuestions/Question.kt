package com.gmplan.gameplan.data.pojos.interestProfileQuestions

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Question : Parcelable {
    @SerializedName("index")
    @Expose
    var qIndex: String? = null

    @SerializedName("area")
    @Expose
    var area: String? = null

    @SerializedName("text")
    @Expose
    var qText: String? = null

    @SerializedName("answer")
    @Expose
    var rate: String = "1"

    constructor(source: Parcel) : this(
    )

    constructor()

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {}

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Question> = object : Parcelable.Creator<Question> {
            override fun createFromParcel(source: Parcel): Question = Question(source)
            override fun newArray(size: Int): Array<Question?> = arrayOfNulls(size)
        }
    }
}
