
package com.gmplan.gameplan.data.pojos.userPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserPojo {

    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("UserObject")
    @Expose
    private UserData userData;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }
}
