package com.gmplan.gameplan.data.pojos

import android.os.Parcel
import android.os.Parcelable


data class CheckList(val id: String, val title: String, val startdate: String, val enddate: String) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeString(title)
        writeString(startdate)
        writeString(enddate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CheckList> = object : Parcelable.Creator<CheckList> {
            override fun createFromParcel(source: Parcel): CheckList = CheckList(source)
            override fun newArray(size: Int): Array<CheckList?> = arrayOfNulls(size)
        }
    }
}

data class CheckLists(val response: String, val checklist: ArrayList<CheckList>)
