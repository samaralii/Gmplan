
package com.gmplan.gameplan.data.pojos;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EventPojo {

    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("events")
    @Expose
    private List<Event> events = null;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

}
