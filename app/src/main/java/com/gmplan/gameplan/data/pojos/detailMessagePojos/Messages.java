
package com.gmplan.gameplan.data.pojos.detailMessagePojos;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Messages {

    @SerializedName("CollegeMessage")
    @Expose
    private CollegeMessage collegeMessage;
    @SerializedName("Message")
    @Expose
    private Message message;
    @SerializedName("College")
    @Expose
    private College college;
    @SerializedName("AdmissionStaff")
    @Expose
    private AdmissionStaff admissionStaff;
    @SerializedName("Replies")
    @Expose
    private List<Object> replies = null;

    public CollegeMessage getCollegeMessage() {
        return collegeMessage;
    }

    public void setCollegeMessage(CollegeMessage collegeMessage) {
        this.collegeMessage = collegeMessage;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public College getCollege() {
        return college;
    }

    public void setCollege(College college) {
        this.college = college;
    }

    public AdmissionStaff getAdmissionStaff() {
        return admissionStaff;
    }

    public void setAdmissionStaff(AdmissionStaff admissionStaff) {
        this.admissionStaff = admissionStaff;
    }

    public List<Object> getReplies() {
        return replies;
    }

    public void setReplies(List<Object> replies) {
        this.replies = replies;
    }

}
