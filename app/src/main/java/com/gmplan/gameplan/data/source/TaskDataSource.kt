package com.gmplan.gameplan.data.source

import com.gmplan.gameplan.data.pojos.*
import com.gmplan.gameplan.data.pojos.careersJobs.CareersJobsPojo
import com.gmplan.gameplan.data.pojos.detailMessagePojos.MessageDetail
import com.gmplan.gameplan.data.pojos.fieldsValue.FieldsValue
import com.gmplan.gameplan.data.pojos.industryCareersSearch.IndustryCareers
import com.gmplan.gameplan.data.pojos.interestProfileQuestions.QuestionsPojo
import com.gmplan.gameplan.data.pojos.staffFeed.StaffNewssFeed
import com.gmplan.gameplan.data.pojos.staffStudent.StaffStudent
import com.gmplan.gameplan.data.pojos.studentProfileDetails.StudentProfileDetails
import com.gmplan.gameplan.data.pojos.userPojo.UserPojo
import com.google.gson.JsonElement
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.Field

interface TaskDataSource {

    fun clearUserData()

    fun getStringByKey(key: String): String?
    fun getBooleanByKey(key: String): Boolean
    fun getUserData(key: String): UserPojo

    fun addValueInPref(key: String, data: Any)

    fun deleteKey(key: String)

    fun testApi(): Single<Int>

    fun LoginUser(email: String, password: String, userId: String): Single<UserPojo>

    fun userProfileViews(studentId: String): Single<ProfileViewsPojo>

    fun userRecruiters(studentId: String): Single<MyRecruitersPojo>

    fun editProfile(id: RequestBody, myLikes: RequestBody, myDislikes: RequestBody,
                    myMusic: RequestBody, myTeam: RequestBody,
                    myCollege: RequestBody, image: MultipartBody.Part?): Single<JsonElement>

    fun getUserCareers(studentId: String): Single<UserCareers>

    fun getCareersDetails(code: String): Single<JsonElement>

    fun getUserColleges(studentId: String): Single<UserColleges>

    fun deleteCareer(careerId: String): Single<JsonElement>

    fun getUserSemesters(studentId: String): Single<UsersSemestersGoals>

    fun deleteSemester(goalId: String): Single<JsonElement>

    fun addSemesters(studentId: String,
                     title: String,
                     grade: String,
                     semester: String,
                     detail: String
    ): Single<JsonElement>

    fun getUserVideos(studentId: String): Single<UserVideos>

    fun editVideo(videoId: String, url: String, name: String): Single<JsonElement>

    fun deleteVideo(videoId: String): Single<JsonElement>

    fun postVideo(studentId: String, url: String, name: String): Single<JsonElement>

    fun getUserAchievements(studentId: String): Single<UserAchievements>

    fun postAchievement(studentId: String, title: String, detail: String): Single<JsonElement>

    fun getUserMessages(studentId: String): Single<UserMessages>

    fun getMessageDetail(messageId: String): Single<MessageDetail>

    fun replyMessage(studentId: String, collegeMessageId: String,
                     body: String, subject: String, collegeId: String,
                     userId: String, admStaffId: String): Single<JsonElement>


    fun getGmpMessages(studentId: String): Single<GmpMessages>

    fun getStaffStudent(staffid: String, collegeId: String): Single<List<StaffStudent>>

    fun getStudentDetail(staffId: String, collegeId: String, studentId: String): Single<StudentProfileDetails>

    fun getStudentFieldsValues(staffId: String): Single<FieldsValue>

    fun searchStaffStudent(staffid: String, collegeId: String, name: String): Single<List<StaffStudent>>

    fun getStudentsCareersSearchList(): Single<List<StudentsCareers>>

    fun searchStaffStudentByCareer(staffid: String, careerCode: String): Single<List<StaffStudent>>

    fun staffSendMessage(collegeId: String, staffid: String, studentId: String,
                         staffName: String, staffTitle: String, staffCollege: String,
                         userId: String, subject: String, message: String): Single<JsonElement>


    fun updateStatus(collegeId: RequestBody, userId: RequestBody, status: RequestBody,
                     image: MultipartBody.Part): Single<JsonElement>

    fun getStaffNewsFeed(): Single<List<StaffNewssFeed>>

    fun getCareersJobs(schoolId: String, careerTitle: String): Single<CareersJobsPojo>

    fun getInterestProfileQuestions(studentId: String): Single<QuestionsPojo>

    fun getCareersAndStems(): Single<GetCareersAndStems>

    fun findIndustryCareers(careerId: String, studentId: String): Single<IndustryCareers>

    fun searchCareerByKeyword(keyword: String, studentId: String): Single<IndustryCareers>

    fun findStemCareers(careerId: String): Single<StemCareers>

    fun addCareer(careerCode: String, studentId: String, careerTitle: String): Single<JsonElement>

    fun getResults(studentId: String, newAnswer: String): Single<QuestionsResult>

    fun getJobZones(studentId: String, jobZones: String): Single<JobZones>

    fun getStaffEvents(staffid: String): Single<EventPojo>
    fun getStudentEvents(studentId: String): Single<EventPojo>

    fun getCheckList(eventId: String): Single<CheckLists>

    fun getEventByDate(studentId: String, date: String): Single<EventByDate>

    fun getStaffEventByDate(staffId: String, date: String): Single<EventByDate>

    fun createAgendaEvent(json: String): Single<JsonElement>
    fun createStaffAgendaEvent(json: String, staffid: String): Single<JsonElement>

    fun searchStudents(students: HashMap<String, String>): Single<List<StaffStudent>>

    fun getSchools(staffId: String, collegeId: String): Single<List<AllSchool>>

    fun getInboxList(collegeId: String): Single<List<MessagesObject>>
    fun getOutboxList(staffId: String): Single<List<MessagesObject>>

}
