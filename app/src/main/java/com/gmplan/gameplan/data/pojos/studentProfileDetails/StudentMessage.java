
package com.gmplan.gameplan.data.pojos.studentProfileDetails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StudentMessage  implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("message_id")
    @Expose
    private String messageId;
    @SerializedName("is_read")
    @Expose
    private boolean isRead;

    protected StudentMessage(Parcel in) {
        id = in.readString();
        studentId = in.readString();
        messageId = in.readString();
        isRead = in.readByte() != 0;
    }

    public static final Creator<StudentMessage> CREATOR = new Creator<StudentMessage>() {
        @Override
        public StudentMessage createFromParcel(Parcel in) {
            return new StudentMessage(in);
        }

        @Override
        public StudentMessage[] newArray(int size) {
            return new StudentMessage[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public boolean isIsRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(studentId);
        dest.writeString(messageId);
        dest.writeByte((byte) (isRead ? 1 : 0));
    }
}
