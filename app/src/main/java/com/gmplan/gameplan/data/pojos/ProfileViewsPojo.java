
package com.gmplan.gameplan.data.pojos;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileViewsPojo {

    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("views")
    @Expose
    private List<P_View> profileViews = null;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<P_View> getProfileViews() {
        return profileViews;
    }

    public void setProfileViews(List<P_View> profileViews) {
        this.profileViews = profileViews;
    }

}
