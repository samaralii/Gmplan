
package com.gmplan.gameplan.data.pojos.staffStudent;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StaffStudent implements Parcelable {

    @SerializedName("Student")
    @Expose
    private Student student;
    @SerializedName("User")
    @Expose
    private User user;
    @SerializedName("School")
    @Expose
    private School school;
    @SerializedName("City")
    @Expose
    private City city;
    @SerializedName("County")
    @Expose
    private County county;
    @SerializedName("State")
    @Expose
    private State state;
    @SerializedName("Answer")
    @Expose
    private Answer answer;

    private boolean isSelected;

    public StaffStudent() {}

    protected StaffStudent(Parcel in) {
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<StaffStudent> CREATOR = new Creator<StaffStudent>() {
        @Override
        public StaffStudent createFromParcel(Parcel in) {
            return new StaffStudent(in);
        }

        @Override
        public StaffStudent[] newArray(int size) {
            return new StaffStudent[size];
        }
    };

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public County getCounty() {
        return county;
    }

    public void setCounty(County county) {
        this.county = county;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
