
package com.gmplan.gameplan.data.pojos.fieldsValue;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FieldsValue {

    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("schools")
    @Expose
    private List<School> schools = null;
    @SerializedName("cities")
    @Expose
    private List<City> cities = null;
    @SerializedName("counties")
    @Expose
    private List<County> counties = null;
    @SerializedName("states")
    @Expose
    private List<State> states = null;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<School> getSchools() {
        return schools;
    }

    public void setSchools(List<School> schools) {
        this.schools = schools;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public List<County> getCounties() {
        return counties;
    }

    public void setCounties(List<County> counties) {
        this.counties = counties;
    }

    public List<State> getStates() {
        return states;
    }

    public void setStates(List<State> states) {
        this.states = states;
    }

}
