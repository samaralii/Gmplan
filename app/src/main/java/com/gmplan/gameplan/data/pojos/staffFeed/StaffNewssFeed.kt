package com.gmplan.gameplan.data.pojos.staffFeed

/**
 * Created by Sammie on 8/8/2017.
 */
data class StaffNewssFeed(val id: String?, val detail: String?,
                          val college_id: String?,
                          val user_id: String?, val image: String?,
                          val created: String?,
                          val updated: String?,
                          val collegename: String?, val user: String?)