
package com.gmplan.gameplan.data.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class P_View {

    @SerializedName("imageId")
    @Expose
    private String id;
    @SerializedName("collegename")
    @Expose
    private String collegename;
    @SerializedName("staffname")
    @Expose
    private String staffname;
    @SerializedName("jobtitle")
    @Expose
    private String jobtitle;
    @SerializedName("profile_views")
    @Expose
    private String profileViews;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCollegename() {
        return collegename;
    }

    public void setCollegename(String collegename) {
        this.collegename = collegename;
    }

    public String getStaffname() {
        return staffname;
    }

    public void setStaffname(String staffname) {
        this.staffname = staffname;
    }

    public String getJobtitle() {
        return jobtitle;
    }

    public void setJobtitle(String jobtitle) {
        this.jobtitle = jobtitle;
    }

    public String getProfileViews() {
        return profileViews;
    }

    public void setProfileViews(String profileViews) {
        this.profileViews = profileViews;
    }

}
