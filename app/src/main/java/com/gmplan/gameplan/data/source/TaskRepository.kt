package com.gmplan.gameplan.data.source

import android.content.SharedPreferences
import com.gmplan.gameplan.data.WebServices
import com.gmplan.gameplan.data.pojos.*
import com.gmplan.gameplan.data.pojos.careersJobs.CareersJobsPojo
import com.gmplan.gameplan.data.pojos.detailMessagePojos.MessageDetail
import com.gmplan.gameplan.data.pojos.fieldsValue.FieldsValue
import com.gmplan.gameplan.data.pojos.industryCareersSearch.IndustryCareers
import com.gmplan.gameplan.data.pojos.interestProfileQuestions.QuestionsPojo
import com.gmplan.gameplan.data.pojos.staffFeed.StaffNewssFeed
import com.gmplan.gameplan.data.pojos.staffStudent.StaffStudent
import com.gmplan.gameplan.data.pojos.studentProfileDetails.StudentProfileDetails
import com.gmplan.gameplan.data.pojos.userPojo.UserPojo
import com.google.gson.Gson
import com.google.gson.JsonElement
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Retrofit


class TaskRepository(retrofit: Retrofit, private val sharedPreferences: SharedPreferences): TaskDataSource {


    private val api: WebServices = retrofit.create(WebServices::class.java)

    override fun clearUserData() {
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    override fun getStringByKey(key: String): String? {
        return sharedPreferences.getString(key, null)
    }

    override fun getBooleanByKey(key: String): Boolean {
        return sharedPreferences.getBoolean(key, true)
    }

    override fun getUserData(key: String): UserPojo {
        val gson = Gson()
        val json = getStringByKey(key)
        return gson.fromJson(json, UserPojo::class.java)
    }

    override fun deleteKey(key: String) {
        val editor = sharedPreferences.edit()
        editor.remove(key)
        editor.commit()
    }


    override fun addValueInPref(key: String, data: Any) {
        val editor = sharedPreferences.edit()

        when (data) {
            is String -> editor.putString(key, data)
            is Boolean -> editor.putBoolean(key, data)
            is UserPojo -> editor.putString(key, addUserData(data))
        }

        editor.commit()
    }

    private fun addUserData(data: UserPojo): String {
        val gson = Gson()
        return gson.toJson(data)
    }


    override fun LoginUser(email: String, password: String, userId: String): Single<UserPojo> {
        return api.LoginUser(email, password, userId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun testApi(): Single<Int> {
        return api.testApi("http://demo9251804.mockable.io/rxtest").map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()?.toInt()
        }
    }


    override fun userProfileViews(studentId: String): Single<ProfileViewsPojo> {
        return api.userProfileViews(studentId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }


    override fun userRecruiters(studentId: String): Single<MyRecruitersPojo> {
        return api.userRecruiters(studentId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun editProfile(id: RequestBody, myLikes: RequestBody, myDislikes: RequestBody, myMusic: RequestBody,
                             myTeam: RequestBody, myCollege: RequestBody, image: MultipartBody.Part?): Single<JsonElement> {
        return api.editUserProfile(id, myLikes, myDislikes, myMusic,
                myTeam, myCollege, image).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }


    override fun getUserCareers(studentId: String): Single<UserCareers> {
        return api.getUserCareers(studentId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun getCareersDetails(code: String): Single<JsonElement> {
        return api.getCareersDetail(code).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }


    override fun getUserColleges(studentId: String): Single<UserColleges> {
        return api.getUserColleges(studentId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun deleteCareer(careerId: String): Single<JsonElement> {
        return api.deleteCareer(careerId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }


    override fun getUserSemesters(studentId: String): Single<UsersSemestersGoals> {
        return api.getUserSemestersGoals(studentId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun deleteSemester(goalId: String): Single<JsonElement> {
        return api.deleteSemester(goalId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun addSemesters(studentId: String, title: String, grade: String, semester: String, detail: String): Single<JsonElement> {
        return api.addSemester(studentId, title, grade, semester, detail).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }


    override fun getUserVideos(studentId: String): Single<UserVideos> {
        return api.userVideos(studentId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun editVideo(videoId: String, url: String, name: String): Single<JsonElement> {
        return api.editVideo(videoId, url, name).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun deleteVideo(videoId: String): Single<JsonElement> {
        return api.deleteVideo(videoId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun postVideo(studentId: String, url: String, name: String): Single<JsonElement> {
        return api.addVideo(studentId, url, name).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }


            response.body()
        }
    }


    override fun getUserAchievements(studentId: String): Single<UserAchievements> {
        return api.userAchievements(studentId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }


            response.body()
        }
    }

    override fun postAchievement(studentId: String, title: String, detail: String): Single<JsonElement> {
        return api.postAchievement(studentId, title, detail).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }


            response.body()
        }
    }


    override fun getUserMessages(studentId: String): Single<UserMessages> {
        return api.userMessages(studentId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun getMessageDetail(messageId: String): Single<MessageDetail> {
        return api.messageDetail(messageId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }


    override fun replyMessage(studentId: String, collegeMessageId: String,
                              body: String, subject: String, collegeId: String, userId: String, admStaffId: String): Single<JsonElement> {
        return api.replyMessage(studentId, collegeMessageId, body, subject, collegeId, userId, admStaffId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun getGmpMessages(studentId: String): Single<GmpMessages> {
        return api.userGmpMessages(studentId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun getStaffStudent(staffid: String, collegeId: String): Single<List<StaffStudent>> {
        return api.getStaffStudents(staffid, collegeId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun getStudentDetail(staffId: String, collegeId: String, studentId: String): Single<StudentProfileDetails> {
        return api.getUserProfileDetail(staffId, collegeId, studentId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun getStudentFieldsValues(staffId: String): Single<FieldsValue> {
        return api.getStudentFieldValues(staffId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }


    override fun searchStaffStudent(staffid: String, collegeId: String, name: String): Single<List<StaffStudent>> {
        return api.staffSearchStudent(staffid, collegeId, name).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }


    override fun getStudentsCareersSearchList(): Single<List<StudentsCareers>> {
        return api.getStudentsSearchCareers().map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun searchStaffStudentByCareer(staffid: String, careerCode: String): Single<List<StaffStudent>> {
        return api.staffSearchStudentByCareer(staffid, careerCode).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun staffSendMessage(collegeId: String, staffid: String, studentId: String,
                                  staffName: String, staffTitle: String, staffCollege: String,
                                  userId: String, subject: String, message: String): Single<JsonElement> {
        return api.staffSendMessage(collegeId, staffid, studentId, staffName, staffTitle, staffCollege,
                userId, subject, message).map { response ->


            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()


        }
    }

    override fun updateStatus(collegeId: RequestBody, userId: RequestBody,
                              status: RequestBody, image: MultipartBody.Part): Single<JsonElement> {
        return api.updateStatus(collegeId, userId, status, image).map { response ->


            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun getStaffNewsFeed(): Single<List<StaffNewssFeed>> {
        return api.staffFeeds().map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun getCareersJobs(schoolId: String, careerTitle: String): Single<CareersJobsPojo> {
        return api.getCareersJobs(schoolId, careerTitle).map { response ->
            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun getInterestProfileQuestions(studentId: String): Single<QuestionsPojo> {
        return api.getInterestProfileQuestions(studentId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun getCareersAndStems(): Single<GetCareersAndStems> {
        return api.getCareersAndStemsList().map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun findIndustryCareers(careerId: String, studentId: String): Single<IndustryCareers> {
        return api.findIndustryCareers(careerId, studentId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun addCareer(careerCode: String, studentId: String, careerTitle: String): Single<JsonElement> {
        return api.addCareers(studentId, careerTitle, careerCode).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun findStemCareers(careerId: String): Single<StemCareers> {
        return api.getStemCareers(careerId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun searchCareerByKeyword(keyword: String, studentId: String): Single<IndustryCareers> {
        return api.searchCareerByKeyword(keyword, studentId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }


    override fun getResults(studentId: String, newAnswer: String): Single<QuestionsResult> {
        return api.getResults(studentId, newAnswer).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun getJobZones(studentId: String, jobZones: String): Single<JobZones> {
        return api.getJobZones(studentId, jobZones).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    fun addJobZonesCareer(json: String, id: String, jobZones: String): Single<JsonElement> {
        return api.addJobZoneCareers(json, id, jobZones).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun getStaffEvents(staffid: String): Single<EventPojo> {
        return api.getStaffEvents(staffid).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun getStudentEvents(studentId: String): Single<EventPojo> {
        return api.getStudentEvents(studentId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun getCheckList(eventId: String): Single<CheckLists> {
        return api.getCheckList(eventId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun getEventByDate(studentId: String, date: String): Single<EventByDate> {
        return api.getEventByDate(studentId, date).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun getStaffEventByDate(staffId: String, date: String): Single<EventByDate> {
        return api.getStaffEventByDate(staffId, date).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun createAgendaEvent(json: String): Single<JsonElement> {
        return api.createAgendaEvent(json).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }


            response.body()
        }
    }

    override fun searchStudents(students: HashMap<String, String>): Single<List<StaffStudent>> {
        return api.searchStudents(students).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()
        }
    }

    override fun createStaffAgendaEvent(json: String, staffid: String): Single<JsonElement> {
        return api.createStaffAgendaEvent(json, staffid).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }


            response.body()
        }
    }

    override fun getSchools(staffId: String, collegeId: String): Single<List<AllSchool>> {
        return api.getSchools(staffId, collegeId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()

        }
    }

    override fun getInboxList(collegeId: String): Single<List<MessagesObject>> {
        return api.getInboxList(collegeId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()

        }
    }

    override fun getOutboxList(staffId: String): Single<List<MessagesObject>> {
        return api.getOutboxList(staffId).map { response ->

            if (!response.isSuccessful) {
                throw Exception("Error In Response")
            }

            response.body()

        }
    }

}



