
package com.gmplan.gameplan.data.pojos.detailMessagePojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CollegeMessage {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("college_id")
    @Expose
    private String collegeId;
    @SerializedName("message_id")
    @Expose
    private String messageId;
    @SerializedName("admission_staff_id")
    @Expose
    private String admissionStaffId;
    @SerializedName("is_read")
    @Expose
    private boolean isRead;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("college_message_id")
    @Expose
    private String collegeMessageId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getCollegeId() {
        return collegeId;
    }

    public void setCollegeId(String collegeId) {
        this.collegeId = collegeId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getAdmissionStaffId() {
        return admissionStaffId;
    }

    public void setAdmissionStaffId(String admissionStaffId) {
        this.admissionStaffId = admissionStaffId;
    }

    public boolean isIsRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCollegeMessageId() {
        return collegeMessageId;
    }

    public void setCollegeMessageId(String collegeMessageId) {
        this.collegeMessageId = collegeMessageId;
    }

}
