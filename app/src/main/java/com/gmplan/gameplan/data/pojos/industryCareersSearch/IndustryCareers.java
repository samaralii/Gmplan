
package com.gmplan.gameplan.data.pojos.industryCareersSearch;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IndustryCareers {

    @SerializedName("careers")
    @Expose
    private List<Career> careers = null;

    public List<Career> getCareers() {
        return careers;
    }

    public void setCareers(List<Career> careers) {
        this.careers = careers;
    }

}
