
package com.gmplan.gameplan.data.pojos;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserVideos {

    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("videos")
    @Expose
    private List<Video> videos = null;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<Video> getVideos() {
        return videos;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }

}
