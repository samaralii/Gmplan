
package com.gmplan.gameplan.data.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Event {

    @SerializedName("Event")
    @Expose
    private Event_ event;

    public Event_ getEvent() {
        return event;
    }

    public void setEvent(Event_ event) {
        this.event = event;
    }

}
