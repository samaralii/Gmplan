
package com.gmplan.gameplan.data.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Career {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("career_title")
    @Expose
    private String careerTitle;
    @SerializedName("career_code")
    @Expose
    private String careerCode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCareerTitle() {
        return careerTitle;
    }

    public void setCareerTitle(String careerTitle) {
        this.careerTitle = careerTitle;
    }

    public String getCareerCode() {
        return careerCode;
    }

    public void setCareerCode(String careerCode) {
        this.careerCode = careerCode;
    }

}
