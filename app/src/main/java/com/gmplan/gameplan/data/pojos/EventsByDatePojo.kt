package com.gmplan.gameplan.data.pojos

import android.os.Parcel
import android.os.Parcelable


data class EventsDes(val id: String, val event_name: String, val event_description: String, val event_end_date: String,
                     val event_start_date: String, val color: String) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeString(event_name)
        writeString(event_description)
        writeString(event_end_date)
        writeString(event_start_date)
        writeString(color)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<EventsDes> = object : Parcelable.Creator<EventsDes> {
            override fun createFromParcel(source: Parcel): EventsDes = EventsDes(source)
            override fun newArray(size: Int): Array<EventsDes?> = arrayOfNulls(size)
        }
    }
}

data class EventByDate(val response: String, val events: ArrayList<EventsDes>)