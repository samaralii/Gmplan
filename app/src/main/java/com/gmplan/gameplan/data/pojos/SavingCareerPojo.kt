package com.gmplan.gameplan.data.pojos

data class _Careerslist(val career_title: String, val career_code: String,
                        val job_zone: String, val student_id: String, val career_check: String)

data class SavingCareer(val Careerslist: List<_Careerslist>)