package com.gmplan.gameplan.data.pojos

import android.os.Parcel
import android.os.Parcelable

class AllSchool(val School: School_)


class School_(val id: String, val name: String, val Student: List<Student_>? = null) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.createTypedArrayList(Student_.CREATOR)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeString(name)
        writeTypedList(Student)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<School_> = object : Parcelable.Creator<School_> {
            override fun createFromParcel(source: Parcel): School_ = School_(source)
            override fun newArray(size: Int): Array<School_?> = arrayOfNulls(size)
        }
    }
}


class Student_(val id: String, val name: String) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeString(name)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Student_> = object : Parcelable.Creator<Student_> {
            override fun createFromParcel(source: Parcel): Student_ = Student_(source)
            override fun newArray(size: Int): Array<Student_?> = arrayOfNulls(size)
        }
    }
}