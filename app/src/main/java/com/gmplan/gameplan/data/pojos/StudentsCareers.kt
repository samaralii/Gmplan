package com.gmplan.gameplan.data.pojos

data class StudentsCareers(var career_code: String, var career_title: String)