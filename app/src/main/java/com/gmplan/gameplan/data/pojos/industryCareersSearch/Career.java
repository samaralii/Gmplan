
package com.gmplan.gameplan.data.pojos.industryCareersSearch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Career {

    @SerializedName("@href")
    @Expose
    private String href;
    @SerializedName("@percent_employed")
    @Expose
    private String percentEmployed;
    @SerializedName("@category")
    @Expose
    private String category;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("tags")
    @Expose
    private Tags tags;

    @SerializedName("salary")
    @Expose
    private String salary;

    @SerializedName("checked")
    @Expose
    private boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getPercentEmployed() {
        return percentEmployed;
    }

    public void setPercentEmployed(String percentEmployed) {
        this.percentEmployed = percentEmployed;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Tags getTags() {
        return tags;
    }

    public void setTags(Tags tags) {
        this.tags = tags;
    }

}
