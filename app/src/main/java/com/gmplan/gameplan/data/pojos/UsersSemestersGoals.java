
package com.gmplan.gameplan.data.pojos;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UsersSemestersGoals {

    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("semestergoals")
    @Expose
    private List<Semestergoal> semestergoals = null;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<Semestergoal> getSemestergoals() {
        return semestergoals;
    }

    public void setSemestergoals(List<Semestergoal> semestergoals) {
        this.semestergoals = semestergoals;
    }

}
