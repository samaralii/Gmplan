package com.gmplan.gameplan.util


object AppConst {
    val REQUIRED_FIELD = "Required Field"
    val BASE_URL_PROFILE_IMG = "https://www.gmplan.com/app/webroot"
    val BASE_URL_FEED_IMG = "https://www.gmplan.com/img/feedsphotos/"
    val NEW_BASEURL = "https://gmplan.com/"
    val DATA = "data"
    val DATA_1 = "data_1"

    val IMAGE_PATH = "image_path"
    val COLLEGE_ID = "college_id"
    val USER_ID = "user_id"
    val STATUS = "status"

    val TYPE_STUDENT = 4
    val TYPE_STAFF = 5
    val IS_FIRST_TIME = "isfirsttime"
    val IS_FIRST_TIME_LOGIN = "isfirsttimelogin"
    val IS_FINGERPRINT_ENABLED = "isfingerprintenabled"
    val USER_DATA = "userdata"

}
