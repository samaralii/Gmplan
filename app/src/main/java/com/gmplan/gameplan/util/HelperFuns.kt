package com.gmplan.gameplan.util

import android.Manifest
import android.app.KeyguardManager
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.support.annotation.IdRes
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import android.widget.Toast
import com.prolificinteractive.materialcalendarview.CalendarDay
import es.dmoral.toasty.Toasty

fun String.splitTags(): Array<String?>? {

    if (this.isNotEmpty()) {

        if (this.contains(",")) {

            val tags = this.split(",")

            val arr = arrayOfNulls<String>(tags.size)

            (0 until tags.size)
                    .filter { tags.isNotEmpty() }
                    .forEach { arr[it] = tags[it] }

            return arr

        } else {

            val arr = arrayOfNulls<String>(1)
            arr[0] = this

            return arr
        }

    } else {
        return null
    }
}

fun EditText.validate(): Boolean {

    val check: Boolean

    if (this.text.isEmpty()) {
        this.error = "Required Field"
        check = false
    } else {
        this.error = null
        check = true
    }

    return check
}

fun Context.tmsg(msg: String) {
    Toasty.normal(this, msg, Toast.LENGTH_SHORT).show()
}

fun Context.tmsgError(msg: String) {
    Toasty.error(this, msg, Toast.LENGTH_SHORT).show()
}

fun Context.tmsgSuccess(msg: String) {
    Toasty.success(this, msg, Toast.LENGTH_SHORT).show()
}

fun Context.tmsgInfo(msg: String) {
    Toasty.info(this, msg, Toast.LENGTH_SHORT).show()
}

fun Context.tmsgWarning(msg: String) {
    Toasty.warning(this, msg, Toast.LENGTH_SHORT).show()
}

fun SwipeRefreshLayout.show() {
    this.isRefreshing = true
}

fun SwipeRefreshLayout.hide() {
    this.isRefreshing = false
}

fun AppCompatActivity.showView(@IdRes id: Int) {
    (this.findViewById<View>(id)).visibility = View.VISIBLE
}

fun AppCompatActivity.hideView(@IdRes id: Int) {
    (this.findViewById<View>(id)).visibility = View.GONE
}

fun View.showView() {
    this.visibility = View.VISIBLE
}

fun View.hideView() {
    this.visibility = View.GONE
}

//fun CalendarDay.toStrng() = this.toString().replace("CalendarDay{", "").replace("}", "")


fun CalendarDay.toStrng(): String {
    val d = this.toString()
            .replace("CalendarDay{", "").replace("}", "")
            .split("-")

    return "${d[0]}-${d[1].toInt() + 1}-${d[2]}"


}

fun String.isValidEmail(): Boolean {

    if (this.isEmpty())
        return false

    return android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun AppCompatActivity.hideKeyboard() {
    this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
    this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
}


@RequiresApi(Build.VERSION_CODES.M)
inline fun checkFingerPrint(context: Context,
                            securityNotEnabled: () -> Unit,
                            permissionRequired: () -> Unit,
                            noEnrolledFingerFound: () -> Unit) {

    val keyguardManager = context.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
    val fingerprintManager = context.getSystemService(Context.FINGERPRINT_SERVICE) as FingerprintManager

    if (!keyguardManager.isKeyguardSecure) {
//        Toast.makeText(this, "Lock screen security not enabled in Settings", Toast.LENGTH_LONG).show()
        securityNotEnabled()
    }

    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
//        Toast.makeText(this, "Fingerprint authentication permission not enabled", Toast.LENGTH_LONG).show()
        permissionRequired()
    }

    if (!fingerprintManager.hasEnrolledFingerprints()) {
//        Toast.makeText(this, "Register at least one fingerprint in Settings", Toast.LENGTH_LONG).show()
        noEnrolledFingerFound()
    }

}

