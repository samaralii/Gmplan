package com.gmplan.gameplan.util

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.support.annotation.ColorRes
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.text.InputType
import android.util.Log
import android.widget.NumberPicker
import android.widget.Toast
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.userPojo.UserData
import es.dmoral.toasty.Toasty
import android.widget.EditText


object Utilz {

    fun tmsg(c: Context?, m: String) {
        Toasty.normal(c!!, m, Toast.LENGTH_SHORT).show()
    }

    fun tmsgError(c: Context?, m: String) {
        Toasty.error(c!!, m, Toast.LENGTH_SHORT).show()
    }

    fun tmsgWarning(c: Context?, m: String) {
        Toasty.warning(c!!, m, Toast.LENGTH_SHORT).show()
    }

    fun tmsgInfo(c: Context?, m: String) {
        Toasty.info(c!!, m, Toast.LENGTH_SHORT).show()
    }

    fun tmsgSuccess(c: Context?, m: String) {
        Toasty.success(c!!, m, Toast.LENGTH_SHORT).show()
    }

    fun showAlert(c: Context, t: String, m: String) {

        val dialog = AlertDialog.Builder(c)
                .setTitle(t)
                .setMessage(m)
                .setPositiveButton("Ok") { d, _ -> d?.dismiss() }
                .create()

        dialog.show()

    }

    inline fun showAlertWithJustOkButton(c: Context, t: String, m: String,
                                         crossinline action: () -> Unit) {

        val dialog = AlertDialog.Builder(c)
                .setTitle(t)
                .setMessage(m)
                .setPositiveButton("Ok") { d, _ ->
                    d.dismiss()
                    action()
                }
                .create()

        dialog.show()
    }

    inline fun showAlertWithListeners(c: Context, t: String, m: String,
                                      crossinline btnOk: () -> Unit,
                                      crossinline btnCancel: () -> Unit) {

        val dialog = AlertDialog.Builder(c)
                .setTitle(t)
                .setMessage(m)
                .setPositiveButton("Ok") { d, _ ->
                    btnOk()
                    d.dismiss()
                }
                .setNegativeButton("Cancel") { d, _ ->
                    btnCancel()
                    d.dismiss()
                }
                .create()

        dialog.show()

    }

    fun addFragmentToActivity(fm: FragmentManager, f: Fragment, @IdRes id: Int,
                              b: Bundle = Bundle(), tag: String = "tag", addToBackStack: Boolean = false) {

        checkNotNull(fm)
        checkNotNull(f)

        val ft: FragmentTransaction = fm.beginTransaction()
        f.arguments = b
        ft.add(id, f, tag)

        if (addToBackStack)
            ft.addToBackStack(null)

        ft.commit()

    }

    fun addFragmentToActivity(fm: FragmentManager, f: Fragment, @IdRes id: Int, addToBackStack: Boolean = false) {

        checkNotNull(fm)
        checkNotNull(f)

        val ft: FragmentTransaction = fm.beginTransaction()
        ft.add(id, f)

        if (addToBackStack)
            ft.addToBackStack(null)

        ft.commit()

    }

    fun replaceFragmentToActivity(fm: FragmentManager, f: Fragment, @IdRes id: Int, addToBackStack: Boolean = false) {

        checkNotNull(fm)
        checkNotNull(f)

        val ft: FragmentTransaction = fm.beginTransaction()
        ft.replace(id, f)

        if (addToBackStack)
            ft.addToBackStack(null)

        ft.commit()

    }

    fun removeFragment(fm: FragmentManager, f: Fragment) {

        checkNotNull(fm)
        checkNotNull(f)

        val ft: FragmentTransaction = fm.beginTransaction()
        ft.remove(f)
        ft.commit()

    }

    fun replaceFragmentToActivity(fm: FragmentManager, f: Fragment, @IdRes id: Int,
                                  b: Bundle = Bundle(), tag: String = "tag", addToBackStack: Boolean = false) {

        checkNotNull(fm)
        checkNotNull(f)

        val ft: FragmentTransaction = fm.beginTransaction()
        ft.replace(id, f, tag)

        if (addToBackStack)
            ft.addToBackStack(null)

        ft.commit()

    }

    fun getColor(c: Context, @ColorRes res: Int): Int {
        return ContextCompat.getColor(c, res)
    }

    fun getImgUrl(userData: UserData?): String {
//        val url = AppConst.BASE_URL_PROFILE_IMG + userData?.student?.photoDir +
//                "/" + userData?.student?.photo
        val url = "${AppConst.BASE_URL_PROFILE_IMG}${userData?.student?.photoDir}/${userData?.student?.photo}"
        Log.d("IMG_URL", url)
        return url
    }

    fun getImgUrl(photoDir: String?, photo: String?): String {
        val url = "${AppConst.BASE_URL_PROFILE_IMG}$photoDir/$photo"
        Log.d("IMG_URL", url)
        return url
    }

    fun dialog(c: Context): ProgressDialog {
        val dialog = ProgressDialog(c)
        dialog.setTitle("Please Wait")
        dialog.setMessage("Loading...")
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }


    fun extractImgUrl(url: String): String {
        return if (url.contains("youtube", true)) {
            if (url.contains("v=")) {
                val s = url.split("=")
                val id = s[1]
                "http://img.youtube.com/vi/$id/mqdefault.jpg"
            } else
                "http://demo.masscode.ru/masspaging/pic/1.png"
        } else {
            "http://demo.masscode.ru/masspaging/pic/1.png"
        }
    }


    inline fun getPickerDialog(activity: Activity, title: String, values: List<String>,
                               crossinline btnDone: (value: String) -> Unit): AlertDialog {

        var selectedValue: String = ""
        val dialog = AlertDialog.Builder(activity)
        val layoutInflater = activity.layoutInflater
        val v = layoutInflater.inflate(R.layout.dialog_picker, null)
        dialog.setTitle(title)
        dialog.setView(v)
        val numberPicker = v.findViewById<NumberPicker>(R.id.dialog_picker_np) as NumberPicker
        numberPicker.maxValue = values.size - 1
        numberPicker.minValue = 0
        numberPicker.wrapSelectorWheel = false
        numberPicker.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        val numberPickerChild = numberPicker.getChildAt(0) as EditText
        numberPickerChild.isFocusable = false
        numberPickerChild.inputType = InputType. TYPE_NULL
//        numberPicker.displayedValues = values
//
        numberPicker.setFormatter { value ->
            values[value]
        }

        numberPicker.setOnValueChangedListener { _, _, newVal ->
            selectedValue = values[newVal]
        }

        dialog.setPositiveButton("Done") { dialog, _ ->
            dialog.dismiss()
            btnDone(selectedValue)
        }


        dialog.setNegativeButton("Cancel") { dialog, _ ->
            dialog.dismiss()
        }

        return dialog.create()

    }
}



