package com.gmplan.gameplan.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gmplan.gameplan.R
import com.gmplan.gameplan.data.pojos.industryCareersSearch.Career
import com.gmplan.gameplan.viewholders.JobZonesCareerVH

class JobZonesAdapter(var data: ArrayList<Career>,
                      val onJobsClick: (code: String) -> Unit,
                      val onUpdateList: (newList: ArrayList<Career>) -> Unit) : RecyclerView.Adapter<JobZonesCareerVH>() {

    override fun onBindViewHolder(holder: JobZonesCareerVH, pos: Int) {

        holder.detail?.text = data[pos].title
        holder.check?.isChecked = data[pos].isChecked


        holder.check?.setOnCheckedChangeListener { _, check ->
            data[pos].isChecked = check
            onUpdateList.invoke(data)
        }

        holder.jobs?.setOnClickListener { onJobsClick.invoke(data[holder.layoutPosition].code) }


    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): JobZonesCareerVH {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.list_jobzones, p0, false) as View
        return JobZonesCareerVH(v)
    }

    override fun getItemCount(): Int = data.size
}