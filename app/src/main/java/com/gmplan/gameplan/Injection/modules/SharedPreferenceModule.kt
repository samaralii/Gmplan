package com.gmplan.gameplan.Injection.modules

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides


@Module
class SharedPreferenceModule {

    @Provides
    fun provideSharedPreference(context: Context): SharedPreferences {
        return context.getSharedPreferences("com.gmplan.gameplan", Context.MODE_PRIVATE)
    }

}