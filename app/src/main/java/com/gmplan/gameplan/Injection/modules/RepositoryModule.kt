package com.gmplan.gameplan.Injection.modules

import android.content.SharedPreferences
import com.gmplan.gameplan.data.source.TaskRepository

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class RepositoryModule {


    @Singleton
    @Provides
    fun provideReposiory(retrofit: Retrofit, sharedPreferences: SharedPreferences): TaskRepository {
        return TaskRepository(retrofit, sharedPreferences)
    }
}
