package com.gmplan.gameplan.Injection.modules

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Sammie on 3/29/2017.
 */

class RxSchedulersImpl : RxSchedulers {
    override fun computation() = Schedulers.computation()
    override fun ui() = AndroidSchedulers.mainThread()
    override fun io() = Schedulers.io()
}
