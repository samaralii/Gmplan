package com.gmplan.gameplan.Injection.components

import com.gmplan.gameplan.Injection.modules.*
import com.gmplan.gameplan.features.Login.FingerprintDialog
import com.gmplan.gameplan.features.Login.LoginActivity
import com.gmplan.gameplan.features.agenda.AgendaActivity
import com.gmplan.gameplan.features.agenda.agendaFragment.AgendaFragment
import com.gmplan.gameplan.features.agenda.createAgenda.CreateAgendaActivity
import com.gmplan.gameplan.features.agenda.eventsFragment.EventsListFragment
import com.gmplan.gameplan.features.dashboard.NewStudentDashboardActivity
import com.gmplan.gameplan.features.dashboard.StaffDashboardActivity
import com.gmplan.gameplan.features.editProfile.EditProfileActivity
import com.gmplan.gameplan.features.editProfile.userInfoEdit.NewUserInfoEditFragment
import com.gmplan.gameplan.features.feed.FeedActivity
import com.gmplan.gameplan.features.feed.FeedFragment
import com.gmplan.gameplan.features.interestProfile.jobzones.JobZonesActivity
import com.gmplan.gameplan.features.interestProfile.jobzones.jobzonesfragment.JobZonesFragment
import com.gmplan.gameplan.features.interestProfile.questionsView.QuestionsActivity
import com.gmplan.gameplan.features.messages.MessagesActivity
import com.gmplan.gameplan.features.messages.messagesListFragment.MessagesFragment
import com.gmplan.gameplan.features.myCareers.MyCareersActivity
import com.gmplan.gameplan.features.myCareers.careersJobs.CareersJobs
import com.gmplan.gameplan.features.myCareers.myCareerListFragment.MyCareersFragment
import com.gmplan.gameplan.features.myColleges.MyCollegesActivity
import com.gmplan.gameplan.features.myColleges.myCollegeListFragment.MyCollegeFragment
import com.gmplan.gameplan.features.myHighlights.MyHighlightsActivity
import com.gmplan.gameplan.features.myHighlights.myVideos.MyVideosFragment
import com.gmplan.gameplan.features.myHighlights.myachievements.MyAchievFragment
import com.gmplan.gameplan.features.myOffers.MyOffersActivity
import com.gmplan.gameplan.features.myRecruiters.MyRecruiterActivity
import com.gmplan.gameplan.features.mySemesters.MySemestersActivity
import com.gmplan.gameplan.features.mySemesters.semestersListFragment.MySemestersFragment
import com.gmplan.gameplan.features.mygmplan.MyGmplanActivity
import com.gmplan.gameplan.features.mygmplan.gmplanMessagesListFragment.GmpMessagesFragment
import com.gmplan.gameplan.features.profileVIews.ProfileViewsActivity
import com.gmplan.gameplan.features.searchCareers.SearchCareersActivity
import com.gmplan.gameplan.features.searchCareers.SearchCareersFragment
import com.gmplan.gameplan.features.searchCareers.industryCareers.IndustryCareersFragment
import com.gmplan.gameplan.features.searchCareers.stemCareers.StemCareerFragment
import com.gmplan.gameplan.features.splashScreen.SplashScreenActivity
import com.gmplan.gameplan.features.staff.myMessenger.inbox.InboxFragment
import com.gmplan.gameplan.features.staff.myMessenger.MyMessengerActivity
import com.gmplan.gameplan.features.staff.myMessenger.outbox.OutboxFragment
import com.gmplan.gameplan.features.staff.myMessenger.searchStudent.SearchStudentFragment
import com.gmplan.gameplan.features.staff.studentsProfiles.schoolListFragment.SchoolListFragment
import com.gmplan.gameplan.features.staff.studentsProfiles.StudentProfileActivity
import com.gmplan.gameplan.features.staff.studentsProfiles.detailFragment.DetailFragment
import com.gmplan.gameplan.features.staff.studentsProfiles.profileDetails.ProfileDetailFragment
import com.gmplan.gameplan.features.staff.studentsProfiles.studentListFragment.StudentProfileFragment
import com.gmplan.gameplan.features.staff.updateStatus.UpdateStatusActivity
import com.gmplan.gameplan.features.userProfile.UserProfile
import com.gmplan.gameplan.infrastructure.GmplanApplication
import com.gmplan.gameplan.service.UpdateStatus
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Sammie on 3/29/2017.
 */


@Singleton
@Component(modules = [(NetModule::class), (AppModule::class), (SchedulersModule::class), (RepositoryModule::class), (SharedPreferenceModule::class)])

interface AppComponent {

    fun Inject(application: GmplanApplication)
    fun Inject(activity: LoginActivity)
    fun Inject(profileViewsActivity: ProfileViewsActivity)
    fun Inject(myRecruiterActivity: MyRecruiterActivity)
    fun Inject(myCareersFragment: MyCareersFragment)
    fun Inject(myCollegeFragment: MyCollegeFragment)
    fun Inject(mySemestersFragment: MySemestersFragment)
    fun Inject(myAchievFragment: MyAchievFragment)
    fun Inject(myVideosFragment: MyVideosFragment)
    fun Inject(messagesFragment: MessagesFragment)
    fun Inject(gmpMessagesFragment: GmpMessagesFragment)
    fun Inject(feedFragment: FeedFragment)
    fun Inject(studentProfileFragment: StudentProfileFragment)
    fun Inject(profileDetailFragment: ProfileDetailFragment)
    fun Inject(detailFragment: DetailFragment)
    fun Inject(searchStudentFragment: SearchStudentFragment)
    fun Inject(updateStatusActivity: UpdateStatusActivity)
    fun Inject(newUserInfoEditFragment: NewUserInfoEditFragment)
    fun Inject(newStudentDashboardActivity: NewStudentDashboardActivity)
    fun Inject(careersJobs: CareersJobs)
    fun Inject(questionsActivity: QuestionsActivity)
    fun Inject(searchCareersFragment: SearchCareersFragment)
    fun Inject(industryCareersFragment: IndustryCareersFragment)
    fun Inject(stemCareerFragment: StemCareerFragment)
    fun Inject(jobZonesFragment: JobZonesFragment)
    fun Inject(jobZonesActivity: JobZonesActivity)
    fun Inject(agendaFragment: AgendaFragment)
    fun Inject(eventsListFragment: EventsListFragment)
    fun Inject(createAgendaActivity: CreateAgendaActivity)
    fun Inject(splashScreenActivity: SplashScreenActivity)
    fun Inject(staffDashboardActivity: StaffDashboardActivity)
    fun Inject(feedActivity: FeedActivity)
    fun Inject(editProfileActivity: EditProfileActivity)
    fun Inject(searchCareersActivity: SearchCareersActivity)
    fun Inject(agendaActivity: AgendaActivity)
    fun Inject(myCareersActivity: MyCareersActivity)
    fun Inject(myCollegesActivity: MyCollegesActivity)
    fun Inject(myGmplanActivity: MyGmplanActivity)
    fun Inject(messagesActivity: MessagesActivity)
    fun Inject(myHighlightsActivity: MyHighlightsActivity)
    fun Inject(mySemestersActivity: MySemestersActivity)
    fun Inject(studentProfileActivity: StudentProfileActivity)
    fun Inject(myMessengerActivity: MyMessengerActivity)
    fun Inject(myOffersActivity: MyOffersActivity)
    fun Inject(userProfile: UserProfile)
    fun Inject(updateStatus: UpdateStatus)
    fun Inject(ac: FingerprintDialog)
    fun Inject(schoolListFragment: SchoolListFragment)
    fun Inject(inboxFragment: InboxFragment)
    fun Inject(outboxFragment: OutboxFragment)

}
