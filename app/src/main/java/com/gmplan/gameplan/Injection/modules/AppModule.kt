package com.gmplan.gameplan.Injection.modules

import android.content.Context

import dagger.Module
import dagger.Provides

/**
 * Created by Sammie on 3/29/2017.
 */

@Module
class AppModule(private val context: Context) {

    @Provides
    fun provideContext(): Context {
        return context
    }
}
