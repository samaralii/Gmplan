package com.gmplan.gameplan.Injection.modules


import dagger.Module
import dagger.Provides

/**
 * Created by Sammie on 3/29/2017.
 */

@Module
class SchedulersModule {

    @Provides
    fun provideSchedulers(): RxSchedulersImpl {
        return RxSchedulersImpl()
    }
}
