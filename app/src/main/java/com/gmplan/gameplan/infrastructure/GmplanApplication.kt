package com.gmplan.gameplan.infrastructure

import android.app.Application
import com.gmplan.gameplan.Injection.components.AppComponent
import com.gmplan.gameplan.Injection.components.DaggerAppComponent
import com.gmplan.gameplan.Injection.modules.*
import com.gmplan.gameplan.util.AppConst
import com.onesignal.OneSignal



/**
 * Created by Sammie on 3/8/2017.
 */

class GmplanApplication : Application() {


    var component: AppComponent? = null
        private set

    override fun onCreate() {
        super.onCreate()

//
//        if (LeakCanary.isInAnalyzerProcess(this)) {
////            //            // This process is dedicated to LeakCanary for heap analysis.
////            //            // You should not init your app in this process.
//            return
//        }
//        LeakCanary.install(this)
//        // Normal app init code...

//        FacebookSdk.sdkInitialize(applicationContext);
//        AppEventsLogger.activateApp(this);


        OneSignal.startInit(this).init()
        initDagger()

    }

    private fun initDagger() {

        component = DaggerAppComponent.builder()
                .netModule(NetModule(AppConst.NEW_BASEURL))
                .appModule(AppModule(this))
                .schedulersModule(SchedulersModule())
                .repositoryModule(RepositoryModule())
                .sharedPreferenceModule(SharedPreferenceModule())
                .build()

        component?.Inject(this)

    }

}
